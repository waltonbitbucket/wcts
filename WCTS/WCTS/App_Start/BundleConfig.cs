﻿using System.Web;
using System.Web.Optimization;

namespace WCTS
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/perfect-scrollbar.jquery.min.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/popper.min.js",
                      "~/Scripts/bootstrap-notify.js",
                      "~/Scripts/paper-dashboard.min.js",
                      "~/Scripts/demo.js",
                      "~/Scripts/fontawesome.min.js",
                      "~/Scripts/Barcode.js",
                      "~/Scripts/iziToast.min.js"
                     
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/paper-dashboard.css",
                      "~/Content/demo.css",
                      "~/Content/fontawesome-all.min.css",
                      "~/Content/iziToast.min.css"
                    
                      ));
        }
    }
}
