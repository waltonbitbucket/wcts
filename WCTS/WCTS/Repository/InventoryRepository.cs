﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Transactions;
using System.Web;
using BR_BLL;
using WCMS_MAIN.HelperClass;
using WCTS.Helper_Model;
using WCTS.Models;
using WCTS.Models.EntityModel;
using WebGrease.Css.Ast.Selectors;

namespace WCTS.Repository
{
    public class InventoryRepository
    {
        readonly WCTSEntities _entities = new WCTSEntities();
        internal List<ProductionModel> GetMasterBarcodeData(string masterBarCode)
        {
            var modelList = new List<ProductionModel>();
            var list = _entities.ProductionMaster.Where(a => a.MasterBarcode == masterBarCode).ToList();
            foreach (var data in list)
            {
                var model = new ProductionModel();
                model.MasterBarCode = data.MasterBarcode;
                model.Model = data.Model;
                model.LotNo = data.LotBarcode;
                model.BarCode = data.Barcode;
                if (data.AddedDate != null) model.Date = (DateTime)data.AddedDate;
                model.DateTime = model.Date.ToString("yyyy-MM-dd");
                modelList.Add(model);
            }
            return modelList;
        }

        internal List<Inventory> GetInventoryInfo(Inventory inventory)
        {

            List<Inventory> inventoryList;
            try
            {

                inventoryList = _entities.Inventory
                    .WhereIf(inventory.MasterBarCode != null, x => x.MasterBarCode == inventory.MasterBarCode)
                    
                .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return inventoryList;
        }
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        internal bool InventoryReceived(List<ProductionModel> inventoryList,long userId)
        {

            try
            {

                var consString = ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString;
                var con = new SqlConnection(consString);

                var result = new Result();

                con.Open();
                var dto = new DataTable();

                dto.Columns.AddRange(new DataColumn[5]
                    {
                new DataColumn("MasterBarCode", typeof (string)),
                new DataColumn("BarCode", typeof (string)),
                new DataColumn("LotNumber", typeof (string)),
                new DataColumn("ReceivedBy", typeof (long)),
                new DataColumn("ReceivedDate", typeof (DateTime))
               
                
            });

                foreach (var itm in inventoryList)
                {
                    var row = dto.NewRow();
                    row["MasterBarCode"] = itm.MasterBarCode;
                    row["BarCode"] = itm.BarCode;
                    row["LotNumber"] = itm.LotNo;
                    row["ReceivedBy"] = userId;
                    row["ReceivedDate"] = DateTime.Now;
                    dto.Rows.Add(row);

                }
                using (var sqlTransaction = con.BeginTransaction())
                {
                    using (var sqlBulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default,sqlTransaction))
                    {
                        //Set the database table name  tblShadowIMEIRecord
                        sqlBulkCopy.DestinationTableName = "dbo.Inventory"; //"dbo.tblIMEIRecord";

                        //[OPTIONAL]: Map the DataTable columns with that of the database table
                        sqlBulkCopy.ColumnMappings.Add("MasterBarCode", "MasterBarCode");
                        sqlBulkCopy.ColumnMappings.Add("BarCode", "BarCode");
                        sqlBulkCopy.ColumnMappings.Add("LotNumber", "LotNumber");
                        sqlBulkCopy.ColumnMappings.Add("ReceivedBy", "ReceivedBy");
                        sqlBulkCopy.ColumnMappings.Add("ReceivedDate", "ReceivedDate");
                        try
                        {
                            sqlBulkCopy.WriteToServer(dto);
                            sqlTransaction.Commit();
                            con.Close();
                            return true;
                            //var nwcomm = new SqlCommand("Update ProductionMaster SET IsPassed=1 WHERE  LotBarcode='" + lotBarcode + "'", con);
                            //var kk = nwcomm.ExecuteNonQuery();
                            //if (kk <= 0) return false;
                            //con.Close();

                        }
                        catch (Exception exception)
                        {
                            sqlTransaction.Rollback();
                            con.Close();
                            return false;
                        }
                    }
                }

            }
            catch (Exception)
            {
                
                throw;
            }
            
        }

        internal List<ProductionMaster> GetPassedItemToPrepareForInventory()
        {
            List<ProductionMaster> dataList;
            try
            {

                dataList = _entities.ProductionMaster.Where(a=>a.IsPassed==true&&(a.IsMoved==null ||a.IsMoved==false)).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return dataList;
        }

        internal bool IsExistsInPassedInventory(string code)
        {
            try
            {

                var data = _entities.PassedInventory.Any(a => a.Barcode == code);
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
          
        }

        internal bool InventoryPassedItemsInsert(List<PassedInventory> list)
        {
            try
            {

                _entities.PassedInventory.AddRange(list);
                _entities.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        internal bool UpdateProductionMasterMovedItems(List<PassedInventory> list)
        {
            try
            {
                foreach (var datas in list)
                {
                    var data = _entities.ProductionMaster.FirstOrDefault(a => a.Barcode == datas.Barcode);

                    if (data != null)
                    {

                        data.IsMoved = true;
                        data.MovedDate = DateTime.Now;
                        _entities.Entry(data).State = EntityState.Modified;
                       

                    }

                }

              
               
                _entities.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            
        }

        internal List<ProductionModel> GetPassedItemFromInventory(string fromdate, string todate)
        {
            DateTime startDate = Convert.ToDateTime(fromdate);
            DateTime endDate = Convert.ToDateTime(todate);
            DateTime toDate = endDate.AddDays(1).AddTicks(-1);


            var list = _entities.PassedInventory.Where(x => x.AddedDate >= startDate && x.AddedDate <= toDate && x.IsHold==false && x.IsMovedToFinalInventory==false).ToList();


            var query = list.AsEnumerable().GroupBy(row => row.AddedDate != null ? new {MasterBarcode=row.MasterBarcode, Min = row.AddedDate.Value.Minute } : null).Select(grp => new ProductionModel()
                               {

                                   MasterBarCode = grp.Key.MasterBarcode,
                                   Min = grp.Key.Min.ToString(),
                                   Total = grp.Count()
                               }).ToList();




            return query;
        }

        internal List<ProductionModel> GetPassedItemFromInventoryWD()
        {

            var list = _entities.PassedInventory.Where(x => x.IsHold == false && x.IsMovedToFinalInventory == false).ToList();
            var query = list.AsEnumerable().GroupBy(row => row.AddedDate != null ? new { MasterBarcode = row.MasterBarcode, Min = row.AddedDate.Value.Minute } : null).Select(grp => new ProductionModel()
            {

                MasterBarCode = grp.Key.MasterBarcode,
                Min = grp.Key.Min.ToString(),
                Total = grp.Count()
            }).ToList();




            return query;
        }

        internal bool HoldPassedInventoryItems(PassedInventoryHold passedInventoryHold, long userId)
        {
            try
            {
                int a = 0;
                using (
                    var transaction = new TransactionScope(TransactionScopeOption.Required,
                        ApplicationState.TransactionOptions))
                {
                    using (
                        var con =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString))
                    {
                        con.Open();
                        if (passedInventoryHold != null)
                        {

                            var nncom = new SqlCommand(
                             "  INSERT INTO PassedInventoryHold(Barcode,MasterBarcode,LotBarcode,Remarks,ProductionDate,IsPassed,AddedBy,AddedDate)" +
                             "SELECT Barcode,MasterBarcode,LotBarcode,'" + passedInventoryHold.Remarks + "',ProductionDate,0,'" + userId + "',GETDATE()FROM PassedInventory WHERE MasterBarcode='" + passedInventoryHold.MasterBarcode + "'", con);
                            a = nncom.ExecuteNonQuery();
                        // _entities.Database.Connection.Close();

                        if (a <= 0) return false;
                        var nqccom = new SqlCommand("Update PassedInventory SET IsHold=1 WHERE  MasterBarcode='" + passedInventoryHold.MasterBarcode + "' ", con);
                        var qc = nqccom.ExecuteNonQuery();
                        if (qc <= 0) return false;

                        con.Close();
                        transaction.Complete();
                        }
                        return true;
                      
                    }

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        internal List<PassedInventory> GetPassedItemFromInventoryItems()
        {
            var list = _entities.PassedInventory.Where(x => x.IsHold == false && x.IsMovedToFinalInventory == false).ToList();
            return list;
        }

        internal bool ProcessFinalInvetoryData(List<FinalInventory> list)
        {

            try
            {
                int a = 0;
              
                    using (
                        var con =new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString))
                    {
                        foreach (var finalInventory in list)
                        {
                            con.Open();
                            using (
                                var transaction = new TransactionScope(TransactionScopeOption.Required,
                                    ApplicationState.TransactionOptions))
                            {
                                var isExists =
                                    new SqlCommand(
                                        "SELECT COUNT(*) from  FinalInventory where Barcode = '" +
                                        finalInventory.Barcode.Trim() + "'", con);
                                var count = (int) isExists.ExecuteScalar();
                                if (count == 0)
                                {
                              //      var ncom = new SqlCommand(
                              //"insert into LotHold(LotNumber,LotTrackingSerial,ProductionDate,AddedBy,AddedDate)" +
                              //" values('" + lotNumebr + "','" + productionSetupData.SerialNumber + "','" + productionSetupData.ProductionDate + "'," +
                              //"'" + userId + "','" + DateTime.Now + "')", con);




                                    var nncom = new SqlCommand(
                                        "  INSERT INTO FinalInventory(Barcode,MasterBarcode,OracleTrncCode,AddedBy,AddedDate) " +
                                        "Values ('" + finalInventory.Barcode.Trim() + "','" +
                                        finalInventory.MasterBarcode.Trim() + "','" + finalInventory.OracleTrncCode.Trim() + "','" + finalInventory.AddedBy +
                                        "','" + finalInventory.AddedDate + "' )", con);
                                    a = nncom.ExecuteNonQuery();
                                    if (a <= 0) return false;
                                    var nqccom =
                                        new SqlCommand(
                                            "Update PassedInventory SET IsMovedToFinalInventory=1,FinalInventoryMovedDate='" + DateTime.Now + "' WHERE  Barcode='" +
                                            finalInventory.Barcode + "' ", con);
                                    var qc = nqccom.ExecuteNonQuery();
                                    if (qc <= 0) return false;


                                    transaction.Complete();
                                 
                                }

                            }
                            con.Close();

                        }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
    }
}