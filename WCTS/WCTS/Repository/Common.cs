﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using OracleDatabaseConnections;
using WCTS.Models;
using WCTS.Models.EntityModel;

namespace WCTS.Repository
{
    public class Common
    {
        readonly WCTSEntities _entities = new WCTSEntities();
        internal List<Shift> GetShifts()
        {
            List<Shift> list;
            try
            {
                list = _entities.Shift.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return list;
        }

        internal List<Line> GetLines()
        {
            List<Line> list;
            try
            {
                list = _entities.Line.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return list;
        }

        internal List<Category> GetCategories()
        {
            List<Category> list;
            try
            {
                list = _entities.Category.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return list;
        }

        internal List<Model> GetModels()
        {
            List<Model> list;
            try
            {
                list = _entities.Model.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return list;
        }

        internal List<InventoryType> GetInventoryTypes()
        {

            
            List<InventoryType> list;
            try
            {
                list = _entities.InventoryType.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return list;
        }

        internal Login GetLoginInfo(Login login)
        {
            Login data;
            try
            {
                data = _entities.Login.FirstOrDefault(a => a.UserId == login.UserId && a.Password == login.Password);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return data;
        }

        internal List<tblSubMenu> GetUserMenus(tblSubMenu menu)
        {

            List<tblSubMenu> data;
            try
            {
                data = _entities.tblSubMenu.Where(a => a.RoleId == menu.RoleId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return data;

        }

        internal ProductionSetup GetProductionSetup(ProductionSetup productionSetup)
        {
            ProductionSetup data;
            try
            {
                data = _entities.ProductionSetup.FirstOrDefault(a => a.LineId == productionSetup.LineId && a.IsActive == productionSetup.IsActive);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return data;
        }

        internal List<ProductionModel> GetAllLotNumbers()
        {
            var  list = new List<ProductionModel>();
            try
            {
                var lists = _entities.ProductionSetup.ToList();

                foreach (var items in lists)
                {
                    var model = new ProductionModel();

                    model.LotNo = items.LotNumber;
                    list.Add(model);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return list;
        }


        internal ProductionSetup GetProductionSerialByLot(ProductionSetup productionSetup)
        {
            ProductionSetup data;
            try
            {
                data = _entities.ProductionSetup.FirstOrDefault(a => a.LotNumber == productionSetup.LotNumber);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return data;
        }

        internal ProductionSetup GetProductionSetupDataByLotAndSerial(ProductionSetup productionSetup)
        {
            ProductionSetup data;
            try
            {
                data = _entities.ProductionSetup.FirstOrDefault(a => a.LotNumber == productionSetup.LotNumber && a.SerialNumber == productionSetup.SerialNumber);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return data;
        }

        internal ProductionSetup GetProductionSetupDataByLot(ProductionSetup productionSetup)
        {
            ProductionSetup data;
            try
            {
                data = _entities.ProductionSetup.FirstOrDefault(a => a.LotNumber == productionSetup.LotNumber);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return data;
        }

        internal List<ProductionMaster> GetProductionDataDataByLotAndSerial(ProductionSetup productionSetup)
        {
            ProductionSetup setUpData;
            List<ProductionMaster> masterData;

            try
            {
                setUpData = _entities.ProductionSetup.FirstOrDefault(a => a.LotNumber == productionSetup.LotNumber && a.SerialNumber == productionSetup.SerialNumber);

                 masterData = _entities.ProductionMaster.Where(a => a.LotBarcode == setUpData.LotNumber).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return masterData;
        }

        internal bool UpdatePassword(Login isExist)
        {
            try
            {
                _entities.Entry(isExist).State = EntityState.Modified;
                _entities.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }

           
        }

        public Login GetEmployeeInfoById(string p)
        {
            Login data;
            try
            {
                data = _entities.Login.FirstOrDefault(a => a.EmployeeId == p);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return data;
        }

        internal QcLotTracking GetQcTrackingLotInfo(string lotNumber)
        {
            QcLotTracking data;
            try
            {
                data = _entities.QcLotTracking.FirstOrDefault(a => a.LotNumber == lotNumber);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return data;
        }

        internal List<OracleModel> GeModelDataFromOracle()
        {
            var connection = OracleDatabaseConnection.GetLatestOracleConnection();
            OracleDataReader oracleDataReader = null;
            var list = new List<OracleModel>();
            try
            {
                // connection = OracleDatabaseConnection.GetAnotherOldConnection();
                connection = OracleDatabaseConnection.GetLatestOracleConnection();
                connection.Open();
            }
            catch (Exception)
            {

                connection.Open();
            }

            const string itemMasterQuery = @"SELECT   MSI.CREATION_DATE, MSI.ITEM_TYPE, MSI.ORGANIZATION_ID, MSI.INVENTORY_ITEM_ID ,    MSI.SEGMENT1  ITEM_CODE,   MSI.DESCRIPTION ITEM_NAME,
                                    MSI.PRIMARY_UOM_CODE UOM,  MSI.ATTRIBUTE14 ITEM_COLOR, MCB.CATEGORY_ID ,  MCB.SEGMENT1,    MCB.SEGMENT2,    MCB.SEGMENT3 PROD_CAT,     MCB.SEGMENT4,    MCB.SEGMENT5 ITEM_MODEL
                                    FROM (SELECT  MIC.ORGANIZATION_ID,  MIC.INVENTORY_ITEM_ID,  MIC.CATEGORY_ID ,  MCB.SEGMENT1,    MCB.SEGMENT2,    MCB.SEGMENT3,     MCB.SEGMENT4,    MCB.SEGMENT5
                                    FROM    APPS.MTL_CATEGORIES_B MCB, APPS.MTL_ITEM_CATEGORIES MIC    WHERE       MCB.CATEGORY_ID=MIC.CATEGORY_ID         AND         MIC.CATEGORY_SET_ID = 1100000041     AND MIC.ORGANIZATION_ID=125)  MCB, 
                                    APPS.MTL_SYSTEM_ITEMS MSI  WHERE MCB.INVENTORY_ITEM_ID(+)=MSI.INVENTORY_ITEM_ID
                                    AND   MSI.INVENTORY_ITEM_STATUS_CODE='Active'  AND MSI.ITEM_TYPE IN ('FG','SA') AND MSI.ORGANIZATION_ID=125
                                    ";


            var oracleCommand = new OracleCommand(itemMasterQuery, connection) { CommandType = CommandType.Text };
            oracleDataReader = oracleCommand.ExecuteReader();
            if (oracleDataReader.HasRows)
            {

                while (oracleDataReader.Read())
                {
                    var obj = new OracleModel();

                    obj.Model = oracleDataReader["ITEM_MODEL"].ToString();
                    obj.ItemCode = oracleDataReader["ITEM_Code"].ToString();

                    list.Add(obj);
                }
            }

            oracleDataReader.Dispose();
            oracleDataReader.Close();
            connection.Close();
            connection.Dispose();
            return list;

        }
    }
}