﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using WCTS.Models.EntityModel;

namespace WCTS.Repository
{
    public class MasterBarcodeLogRepository
    {
        readonly WCTSEntities _entities = new WCTSEntities();

        public bool AddMasterBarcode(MasterBarcodeLog masterBarcode)
        {
            masterBarcode.CreatedDate = DateTime.Now;
            masterBarcode.SerialNo = GetPreviousSerialNumber(masterBarcode.LotBarcode,masterBarcode.ItemCode, masterBarcode.Model)+1;
            masterBarcode.MasterBarcode=GenerateMasterBarcode(masterBarcode.LotBarcode,masterBarcode.SerialNo);
            masterBarcode.IsPrinted= false;
            masterBarcode.IsActive = true;    
            _entities.MasterBarcodeLog.Add(masterBarcode);
            return _entities.SaveChanges()>0;
        }

        public string GenerateMasterBarcode(string lotBarcode,long serialNumber)
        {
            string countValue = "0001";
            string lot = lotBarcode.Substring(0,9);            
            long serialNo = serialNumber;
            if(serialNo<10)
            {
                countValue= "000" + serialNo;
            }
            else if(serialNo<100)
            {
                countValue= "00" + serialNo;
            }
            else if (serialNo < 1000)
            {
                countValue= "0" + serialNo;
            }else
            {
                countValue= serialNo.ToString();
            }
            string finalMasterBarcode = lot + countValue;
            return finalMasterBarcode;
            
        }


        public MasterBarcodeLog GetMasterBarcode(string userId, string lotBarcode, string itemCode, string model)
        {
            var masterBarcode = _entities.MasterBarcodeLog.Where(c => c.IsActive
            && !c.IsPrinted
            && c.UserId == userId
            && c.LotBarcode.ToLower() == lotBarcode.ToLower()
            && c.ItemCode.ToLower() == itemCode.ToLower()
            && c.Model.ToLower() == model.ToLower()).OrderByDescending(c=>c.Id).FirstOrDefault();
            return masterBarcode;
        }

        public MasterBarcodeLog GetMasterBarcode(string masterBarcode)
        {
            var master = _entities.MasterBarcodeLog.FirstOrDefault(c => c.IsActive            
            && c.MasterBarcode.ToLower() == masterBarcode.ToLower());
            return master;
        }

        public bool UpdateMasterBarCode(MasterBarcodeLog masterBarcode)
        {
            _entities.MasterBarcodeLog.AddOrUpdate(masterBarcode);
            return _entities.SaveChanges() > 0;
        }


        public long GetPreviousSerialNumber(string lotBarcode, string itemCode, string model)
        {
            string lotPreCode= lotBarcode.Substring(0,6).ToLower();
            long previousSerialNumber = _entities.MasterBarcodeLog.Where(c =>  c.LotBarcode.ToLower().StartsWith(lotPreCode)
            //&& c.ItemCode.ToLower() == itemCode.ToLower()
            //&& c.Model.ToLower() == model.ToLower()
            ).OrderByDescending(c=>c.SerialNo).Select(c=>c.SerialNo).FirstOrDefault();
            return previousSerialNumber;
        }

        public long GetPreviousSerialNumber(string itemCode, string model)
        {
            long previousSerialNumber = _entities.MasterBarcodeLog.Where(c =>
            c.ItemCode.ToLower() == itemCode.ToLower()
            && c.Model.ToLower() == model.ToLower()).OrderByDescending(c => c.Id).Select(c => c.SerialNo).FirstOrDefault();
            return previousSerialNumber;
        }
    }
}