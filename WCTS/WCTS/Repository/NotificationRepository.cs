﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WCTS.Models;
using WCTS.Models.EntityModel;

namespace WCTS.Repository
{
    public class NotificationRepository
    {
        readonly WCTSEntities _entities = new WCTSEntities();

        internal List<NotificationModel> GetWctsNotification(Models.NotificationModel notificationModel)
        {
            List<NotificationModel> list;
            try
            {
                list = (from brandnotification in _entities.WctsNotification

                        //join login in _entities.Login on brandnotification.AddedBy equals login.Id

                        where brandnotification.Seen == false && brandnotification.UserId == notificationModel.UseId && brandnotification.RoleId == notificationModel.RoleId
                        orderby brandnotification.AddedDate descending
                        select new NotificationModel
                        {
                            Id = brandnotification.Id,
                            Title = brandnotification.Title,
                            Description = brandnotification.Description,
                            TargetController = brandnotification.TargetController,
                            TargetAction = brandnotification.TargetAction,
                            Type = brandnotification.Type,
                            Parameter = brandnotification.Parameter,
                            AddedName = (long) brandnotification.UserId,
                            AddedDate = brandnotification.AddedDate

                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return list;
        }


        internal bool UpdateNotification(WctsNotification wctsNotification)
        {

            WctsNotification notification = _entities.WctsNotification.FirstOrDefault(x => x.Id == wctsNotification.Id && x.UserId == wctsNotification.UserId && x.RoleId == wctsNotification.RoleId);
            if (notification != null)
            {
                notification.Seen = true;

                _entities.Entry(notification).State = EntityState.Modified;
                _entities.SaveChanges();

            }

            else
            {
                return false;
            }

            return true;
        }

        internal bool UpdateAllNotification(WctsNotification wctsNotification)
        {
            try
            {
                var notificationList = _entities.WctsNotification.Where(x => x.UserId == wctsNotification.UserId && x.RoleId == wctsNotification.RoleId).ToList();
                foreach (var itms in notificationList)
                {
                    itms.Seen = true;
                    _entities.Entry(itms).State = EntityState.Modified;
                }

                _entities.SaveChanges();
                //if (notification != null)
                //{
                //    notification.Seen = true;

                //    _entities.Entry(notification).State = EntityState.Modified;
                //    _entities.SaveChanges();

                //}

                //else
                //{
                //    return false;
                //}

                return true;
            }
            catch (Exception)
            {

                throw;
            }
            
        }
        public bool InsertNotificationInfo(WctsNotification noti)
        {
            try
            {

                var getLoginPersons = _entities.Login.Where(x => x.RoleId == 1 || x.RoleId == 2 || x.RoleId == 3 || x.RoleId == 10002).ToList();

                foreach (var loginPerson in getLoginPersons)
                {
                    noti.UserId = Convert.ToInt64(loginPerson.UserId);
                    noti.RoleId = loginPerson.RoleId;
                    _entities.WctsNotification.Add(noti);
                    _entities.SaveChanges();
                }

             




                return true;
            }
            catch (Exception exception)
            {

                throw exception;
            }
        }
    }
}