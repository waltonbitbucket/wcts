﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WCTS.Helper_Model;
using WCTS.Models;
using WCTS.Models.EntityModel;

namespace WCTS.Repository
{
    public class ReportRepository
    {
        readonly WCTSEntities _entities = new WCTSEntities();

        internal List<ProductionModel> GetProductionLineReport(string fromdate, string todate)
        {
            List<ProductionModel> list;
            
            try
            {

                var targetdatefrom = new SqlParameter
                {
                    ParameterName = "@fromDate",
                    Value = fromdate == "" ? (object)DBNull.Value : fromdate
                };
                var targetdateto = new SqlParameter
                {
                    ParameterName = "@toDate",
                    Value = todate == "" ? (object)DBNull.Value : todate
                };
                _entities.Database.CommandTimeout = 600;

                list =
                    _entities.Database.SqlQuery<ProductionModel>(
                        "exec [SPWCTSLineProduction] @fromDate,@toDate",
                        targetdatefrom, targetdateto).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            ;

            return list;
        }

        internal List<ProductionModel> GetBarcodeDetailsReport(string barCode)
        {

            var modelList = new List<ProductionModel>();
            var model= new ProductionModel();

            var list = _entities.ProductionMaster.FirstOrDefault(a => a.Barcode == barCode);

            if (list != null)
            {
                model.BarCode = list.Barcode;
                model.Model = list.Model;
                model.LotNo = list.LotBarcode;
                model.MasterBarCode = list.MasterBarcode;
                model.CategoryName = _entities.Category.Where(a=>a.CategoryCode==list.CategoryCode).Select(a=>a.CategoryName).FirstOrDefault();
                model.BarCode = list.Barcode;
                model.CategoryCode = list.CategoryCode;
                model.ShiftName=_entities.Shift.Where(a=>a.ShiftId==list.ShiftId).Select(a=>a.ShiftName).FirstOrDefault();
                model.Weight =8.20;
                model.Status = list.IsPassed == true ? "Passed" : "Pending";
                if (list.ProductionDate != null) model.Date = (DateTime)list.ProductionDate;
                model.DateTime = model.Date.ToString("dd-MM-yyyy");

                modelList.Add(model);

            }


            return modelList;
        }

        internal List<ProductionModel> GetLotWiseReport(string lotNumber)
        {
            var modelList = new List<ProductionModel>();
         

            var lists = _entities.ProductionSetup.Where(a => a.LotNumber == lotNumber).Take(1).ToList();
            var plist = _entities.ProductionMaster.Where(a => a.LotBarcode == lotNumber).ToList();
            var list = (from pmaster in plist
                        join setup in lists on pmaster.LotBarcode equals setup.LotNumber
                       
                    select new ProductionModel
                    {
                      BarCode = pmaster.Barcode,
                      Model = pmaster.Model,
                      LotNo = pmaster.LotBarcode,
                      CategoryName = setup.CategoryName,
                      ShiftName = setup.ShiftName,
                      Weight = 8.20,
                      Status = pmaster.IsPassed == true ? "Passed" : "Pending",
                      //if (pmaster.ProductionDate != null) Date = (DateTime)items.ProductionDate,
                      Date = pmaster.ProductionDate!=null? (DateTime) pmaster.ProductionDate:DateTime.Now
                    }).ToList();



                //foreach (var items in list)
                //{
                //    var model = new ProductionModel();
                //    model.BarCode = items.Barcode;
                //    model.Model = items.Model;
                //    model.LotNo = items.LotBarcode;
                //    model.CategoryName = _entities.Category.Where(a => a.CategoryCode == items.CategoryCode).Select(a => a.CategoryName).FirstOrDefault();
                //    model.ShiftName = _entities.Shift.Where(a => a.ShiftId == items.ShiftId).Select(a => a.ShiftName).FirstOrDefault();
                //    model.Weight = 8.20;
                //    model.Status = items.IsPassed == true ? "Passed" : "Pending";
                //    if (items.ProductionDate != null) model.Date = (DateTime)items.ProductionDate;
                //    model.DateTime = model.Date.ToString("dd-MM-yyyy");
                //    modelList.Add(model);
                //}
     
              

            return list;
        }

        internal List<ProductionModel> GetMasterBarcodeWiseReport(string mCodeNumber)
        {
            var modelList = new List<ProductionModel>();


            var list = _entities.ProductionMaster.Where(a => a.MasterBarcode == mCodeNumber).ToList();



            foreach (var items in list)
            {
                var model = new ProductionModel();
                model.MasterBarCode = items.MasterBarcode;
                model.BarCode = items.Barcode;
                model.Model = items.Model;
                model.LotNo = items.LotBarcode;
                model.CategoryName = _entities.Category.Where(a => a.CategoryCode == items.CategoryCode).Select(a => a.CategoryName).FirstOrDefault();
                model.ShiftName = _entities.Shift.Where(a => a.ShiftId == items.ShiftId).Select(a => a.ShiftName).FirstOrDefault();
                model.Weight = 8.20;
                model.Status = items.IsPassed == true ? "Passed" : "Pending";
                if (items.ProductionDate != null) model.Date = (DateTime)items.ProductionDate;
                model.DateTime = model.Date.ToString("dd-MM-yyyy");
                modelList.Add(model);
            }



            return modelList;
        }

        public List<ProductionMaster> GetBaroceForDataLoader(string date, string model, string shift)
        {
            DateTime startDate =Convert.ToDateTime( date);

            DateTime endDate = Convert.ToDateTime(date);
            DateTime toDate = endDate.AddDays(1).AddTicks(-1);
            //var list = _entities.ProductionMaster.Where(x => x.Model == model && (x.AddedDate >= startDate && x.AddedDate <= toDate) && x.ShiftId == shift).ToList();

            var list = _entities.ProductionMaster.Where(x => x.Model == model && (x.ProductionDate >= startDate && x.ProductionDate <= toDate) && x.ShiftId == shift && x.IsPassed==true).ToList();

            return list;
        
        }


       

       



        internal List<ProductionMaster> GetLastSevenDaysProduction()
        {
            var today = DateTime.Now.ToString("MM/dd/yyyy");

            DateTime startDate = Convert.ToDateTime(today);


            var endDate = Convert.ToDateTime(today).AddDays(-6).AddTicks(-1); ;

            //var modelList = new List<ProductionReportModel>();
            var list = _entities.ProductionMaster.Where(x => (x.ProductionDate >= endDate && x.ProductionDate <= startDate)).ToList();

            //foreach (var items in list)
            //{
            //    var model = new ProductionReportModel();

            //    model.Shift = _entities.Shift.Where(a => a.ShiftId == items.ShiftId).Select(a => a.ShiftName).FirstOrDefault();
            //    if (items.ProductionDate != null) model.AddedDate =(DateTime) items.ProductionDate;
            //    model.Date = model.AddedDate.ToString("dd-MM-yyyy");
            //    modelList.Add(model);
            //}

            return list;
        }

        internal List<ProductionModel> GetProductionLinetablePending(string fromdate, string todate)
        {
            List<ProductionModel> list;

            try
            {

                var targetdatefrom = new SqlParameter
                {
                    ParameterName = "@fromDate",
                    Value = fromdate == "" ? (object)DBNull.Value : fromdate
                };
                var targetdateto = new SqlParameter
                {
                    ParameterName = "@toDate",
                    Value = todate == "" ? (object)DBNull.Value : todate
                };
                _entities.Database.CommandTimeout = 600;

                list =
                    _entities.Database.SqlQuery<ProductionModel>(
                        "exec [SPWCTSLineProductionPending] @fromDate,@toDate",
                        targetdatefrom, targetdateto).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }


            return list;
        }


        internal List<ProductionModel> GetAllProductionReport(string fromdate, string todate)
        {
            List<ProductionModel> list;

            try
            {

                var targetdatefrom = new SqlParameter
                {
                    ParameterName = "@fromDate",
                    Value = fromdate == "" ? (object)DBNull.Value : fromdate
                };
                var targetdateto = new SqlParameter
                {
                    ParameterName = "@toDate",
                    Value = todate == "" ? (object)DBNull.Value : todate
                };
                _entities.Database.CommandTimeout = 600;

                list =
                    _entities.Database.SqlQuery<ProductionModel>(
                        "exec [SPWCTSLineProductionAll] @fromDate,@toDate",
                        targetdatefrom, targetdateto).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }


            return list;
        }

        internal List<ProductionSummaryReport> GetAllMasterBarcodeWiseProductionReport(string fromdate, string todate)
        {
            List<ProductionSummaryReport> list;

            try
            {
                string query = string.Format(@"SELECT p.MasterBarcode,
p.Model,
p.ShiftId,
s.ShiftName Shift,
p.CategoryCode,
CONVERT(VARCHAR, p.AddedDate, 23) AS AddedDate,
COUNT(*) Total
  FROM [dbo].[ProductionMaster] p
INNER JOIN [dbo].[Shift] s ON p.ShiftId=s.ShiftId

where Convert(date, p.AddedDate) BETWEEN '{0}' AND '{1}'

GROUP BY p.MasterBarcode,p.Model,p.ShiftId,s.ShiftName,p.CategoryCode,CONVERT(VARCHAR, p.AddedDate, 23)", fromdate,todate);

                list =
                    _entities.Database.SqlQuery<ProductionSummaryReport>(query).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }


            return list;
        }



        internal List<ProductionModel> GetQcPassedItems(string fromdate, string todate)
        {
            List<ProductionModel> list;

            try
            {

                var targetdatefrom = new SqlParameter
                {
                    ParameterName = "@fromDate",
                    Value = fromdate == "" ? (object)DBNull.Value : fromdate
                };
                var targetdateto = new SqlParameter
                {
                    ParameterName = "@toDate",
                    Value = todate == "" ? (object)DBNull.Value : todate
                };
                _entities.Database.CommandTimeout = 600;

                list =
                    _entities.Database.SqlQuery<ProductionModel>(
                        "exec [SPWCTSQcPassedItems] @fromDate,@toDate",
                        targetdatefrom, targetdateto).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }


            return list;
        }

        internal List<ProductionMaster> GetCurrentlyHoldReport()
        {
            var list = _entities.ProductionMaster.Where(x => x.IsHold == true).ToList();

            return list;
        }

        internal List<LotHold> GetAllHoldReport(string fromdate, string todate)
        {
            DateTime startDate = Convert.ToDateTime(fromdate);
            DateTime endDate = Convert.ToDateTime(todate);
            DateTime toDate = endDate.AddDays(1).AddTicks(-1);
            var list = _entities.LotHold.Where(x => x.AddedDate >= startDate && x.AddedDate <= toDate).ToList();

            return list;
        }

        internal List<RejectedItems> GetCurrentlyRejectedReport()
        {
            var list = _entities.RejectedItems.Where(x => x.Passed == false).ToList();

            return list;
        }

         internal List<RejectedItems> GetAllRejectedReport(string fromdate, string todate)
        {
            DateTime startDate = Convert.ToDateTime(fromdate);
            DateTime endDate = Convert.ToDateTime(todate);
            DateTime toDate = endDate.AddDays(1).AddTicks(-1);
            var list = _entities.RejectedItems.Where(x => x.AddedDate >= startDate && x.AddedDate <= toDate).ToList();

            return list;
        }


        public List<PassedInventoryHoldModel> GetPassedInventoryHoldData()
        {
            var list = _entities.PassedInventoryHold.Where(x => x.IsPassed == false).ToList();


            var query = list.AsEnumerable().GroupBy(row => row.AddedDate != null ? new {row.MasterBarcode } : null).Select(grp => new PassedInventoryHoldModel()
            {
                MasterBarcode = grp.Key.MasterBarcode,
                Total = grp.Count()
            }).ToList();




            return query;
        }

        public List<PassedInventoryHold> GetAllPassedInventoryData(string fromdate, string todate)
        {
            DateTime startDate = Convert.ToDateTime(fromdate);
            DateTime endDate = Convert.ToDateTime(todate);
            DateTime toDate = endDate.AddDays(1).AddTicks(-1);
            var list = _entities.PassedInventoryHold.Where(x => x.AddedDate >= startDate && x.AddedDate <= toDate).ToList();

            return list;
        }

        public List<FinalInventory> GetAllInventoryData(string fromdate, string todate)
        {
            DateTime startDate = Convert.ToDateTime(fromdate);
            DateTime endDate = Convert.ToDateTime(todate);
            DateTime toDate = endDate.AddDays(1).AddTicks(-1);
            var list = _entities.FinalInventory.Where(x => x.AddedDate >= startDate && x.AddedDate <= toDate).ToList();

            return list;
        }

        internal List<ProductionMaster> GetBaroceForAllProduction(string fromdate, string todate, string model, string shift, string category)
        {
            DateTime startDate = Convert.ToDateTime(fromdate);

            DateTime endDate = Convert.ToDateTime(todate);
            DateTime toDate = endDate.AddDays(1).AddTicks(-1);
            //var list = _entities.ProductionMaster.Where(x => x.Model == model && (x.AddedDate >= startDate && x.AddedDate <= toDate) && x.ShiftId == shift).ToList();

            var list = _entities.ProductionMaster.Where(x => x.Model == model && (x.AddedDate >= startDate && x.AddedDate <= toDate) && x.Model == model && x.ShiftId == shift && x.CategoryCode == category).ToList();

            return list;
        }

        internal List<QcLotTracking> GetQcTrackingLotReport(string lotNumber)
        {
            var lists = _entities.QcLotTracking.Where(a => a.LotNumber == lotNumber).ToList();
            return lists;
        }

        internal List<ManualPrintLog> GetManualPrintLogReport(string masterBarCode)
        {
            var lists = _entities.ManualPrintLog.Where(a => a.MasterBarCode == masterBarCode.Trim()).ToList();
            return lists;
        }

        internal List<QcLotTracking> GetQcTrackingDateWiseReport(string fromdate, string todate)
        {
            DateTime startDate = Convert.ToDateTime(fromdate);
            DateTime endDate = Convert.ToDateTime(todate);
            DateTime toDate = endDate.AddDays(1).AddTicks(-1);
            var list = _entities.QcLotTracking.Where(x => x.AddedDate >= startDate && x.AddedDate <= toDate).ToList();

            return list;
        }

        internal List<QcItems> GetQcPassedOracleUploadedData(DateTime fromdate, DateTime todate)
        {

            DateTime startDate = fromdate;
            DateTime endDate = todate;
            DateTime toDate = endDate.AddDays(1).AddTicks(-1);

            List < QcItems > data;
            using (var entity = new WCTSEntities())
            {

                data = entity.QcItems.Where(a => a.AddedDate >= startDate && a.AddedDate <= toDate).ToList();
            }

            return data;



            
        }
    }
}