﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WCTS.Models.EntityModel;

namespace WCTS.Repository
{

    public class UpdatePanelRepository
    {
        readonly WCTSEntities _entities = new WCTSEntities();
        public bool UpdateProductionMaster(ProductionMaster productionMaster)
        {
            _entities.Configuration.LazyLoadingEnabled = false;
            var master = _entities.ProductionMaster.FirstOrDefault(x => x.Barcode == productionMaster.PreviousBarcode);

            if (master != null)
            {

                master.Barcode = productionMaster.Barcode;
                master.PreviousBarcode = productionMaster.PreviousBarcode;
                master.MasterBarcode = productionMaster.MasterBarcode;
                master.LotBarcode = productionMaster.LotBarcode;
                master.Model = productionMaster.Model;
                master.CategoryCode = productionMaster.CategoryCode;
                master.ProductionDate = productionMaster.ProductionDate;
                master.UpdatedBy = productionMaster.UpdatedBy;
                master.UpdatedDate = DateTime.Now;
                _entities.Entry(master).State = EntityState.Modified;
                _entities.SaveChanges();
            }
            else
            {
                return false;
            }

            //DB.SalesDeliveryPlans.Add(item);



            return true;
        }



        public bool DeleteProductionMaster(ProductionMaster productionMaster)
        {
            _entities.Configuration.LazyLoadingEnabled = false;
            var master = _entities.ProductionMaster.FirstOrDefault(x => x.Barcode == productionMaster.PreviousBarcode);

            if (master != null)
            {
                _entities.ProductionMaster.Remove(master);
                _entities.SaveChanges();
            }
            else
            {
                return false;
            }

            return true;
        }




        internal AFG CheckSerialCode(string barCode)
        {
            var afg = _entities.AFG.FirstOrDefault(a => a.Serial.Trim() == barCode.Trim());
            return afg;
        }
    }
}