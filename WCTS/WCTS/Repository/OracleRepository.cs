﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using OracleDatabaseConnections;
using WCTS.Models;

namespace WCTS.Repository
{
    public class OracleRepository
    {

        public bool CheckAndUploadImeiToOracle(string model,List<OracleUploadModel> list)
        {

            int inventoryitemId = 0;
            bool returnValue = false;
           // var endDate = todate;
           // var toDate = endDate.AddDays(1).AddTicks(-1);
            var connection = OracleDatabaseConnection.GetLatestOracleConnection();
            var pushconnection = OracleDatabaseConnection.GetLatestOracleConnection();
            OracleDataReader oracleDataReader = null;
            //OracleDataReader oracleDataReader1 = null;
            //OracleDataReader oracleDataReader2 = null;

            try
            {
                // connection = OracleDatabaseConnection.GetAnotherOldConnection();
                connection = OracleDatabaseConnection.GetLatestOracleConnection();
                connection.Open();
            }
            catch (Exception)
            {

                connection.Open();
            }


           



            // Item master query  modified to new oracle 
            var itemMasterQuery = @" SELECT MSI.CREATION_DATE, MSI.ORGANIZATION_ID, MSI.INVENTORY_ITEM_ID ,    MSI.SEGMENT1  ITEM_CODE,   MSI.DESCRIPTION ITEM_NAME,
                                    MSI.PRIMARY_UOM_CODE UOM,   MCB.SEGMENT1,    MCB.SEGMENT2,    MCB.SEGMENT3,     MCB.SEGMENT4,    MCB.SEGMENT5, MSI.ATTRIBUTE14 ITEM_COLOR
                                    FROM  apps.MTL_ITEM_CATEGORIES MIC,   apps.MTL_CATEGORIES_B MCB, apps.MTL_SYSTEM_ITEMS MSI
                                    WHERE    MIC.CATEGORY_SET_ID = 1100000041 
                                    AND MSI.INVENTORY_ITEM_ID = MIC.INVENTORY_ITEM_ID
                                    AND MSI.ORGANIZATION_ID = MIC.ORGANIZATION_ID
                                    AND MIC.CATEGORY_ID = MCB.CATEGORY_ID
                                    AND MSI.INVENTORY_ITEM_STATUS_CODE='Active'
                                    AND MCB.SEGMENT1 = 'FINISHED GOODS'   AND MSI.ORGANIZATION_ID=223 
                                    AND LOWER(MCB.SEGMENT5)='" + model.ToLower() +"'";

            var oracleCommand = new OracleCommand(itemMasterQuery, connection) { CommandType = CommandType.Text };
            oracleDataReader = oracleCommand.ExecuteReader();
            if (oracleDataReader.HasRows)
            {

                while (oracleDataReader.Read())
                {
                    inventoryitemId = Convert.ToInt32(oracleDataReader["INVENTORY_ITEM_ID"]);
                }
            }
            oracleDataReader.Dispose();
            oracleDataReader.Close();
            connection.Close();
            connection.Dispose();
            //Insert Data To Oracle 
            //Insert Data To Oracle 
            if (inventoryitemId != 0)
            {
                var commoDALGetCommonSelecting = new DALGetCommonSelecting();
                var oracleUploadList = new List<OracleUploadEntity>();
                pushconnection.Open();
                foreach (var items in list)
                {


                    var checkDuplcate = @"SELECT COUNT(1) FROM APPS.XX_PRODUCTION_ENTRY WHERE SERIAL_NO='" + items.Imei1 + "'";
                    var oracldupCommand = new OracleCommand(checkDuplcate, pushconnection) { CommandType = CommandType.Text };
                    var res = oracldupCommand.ExecuteScalar();
                    if (Convert.ToInt32(res) == 0)
                    {
                        var oracleUploadItems = new OracleUploadEntity();
                        oracleUploadItems.SequenceId = DateTime.Now.Ticks;
                        oracleUploadItems.OrganizationId = 223;
                        oracleUploadItems.TranscationDate = DateString();
                        oracleUploadItems.EbsItemId = inventoryitemId;
                        oracleUploadItems.ItemModel = items.Model;
                        oracleUploadItems.ItemColor = items.Color;

                        if (string.IsNullOrEmpty(items.WO))
                        {
                            oracleUploadItems.ItemVersion = "SKD-LOT01";
                        }
                        else
                        {
                            oracleUploadItems.ItemVersion = GetOracleVersion(items.WO);
                        }


                        oracleUploadItems.JobId = null;
                        oracleUploadItems.SeriaNo = items.Imei1.Trim();
                        oracleUploadItems.SeriaNo2 = items.Imei2.Trim();
                        oracleUploadItems.ImpFlag = 0;
                        oracleUploadItems.ImpReference = null;
                        oracleUploadItems.CreatedBy = items.AddedBy;
                        oracleUploadItems.CreationDate = Convert.ToDateTime(String.Format("{0:G}", DateTime.Now)); // DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt");
                        oracleUploadItems.LastUpdatedBy = items.AddedBy;
                        oracleUploadItems.LastDateUpdatedDate = DateTime.Now; //DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt");
                        oracleUploadItems.Remarks = "Logistics-Uploaded";
                        oracleUploadItems.DataSource = "WCMS";
                        oracleUploadItems.Grade = items.Grade.Trim();
                        oracleUploadItems.GradeReason = items.GradeReason != null ? items.GradeReason.Trim() : items.GradeReason;

                        oracleUploadList.Add(oracleUploadItems);
                    }

                }
                pushconnection.Close();
                // pushconnection.Dispose();
                try
                {
                    pushconnection.Open();
                    //OracleTransaction transaction = pushconnection.BeginTransaction();
                    // Assign transaction object for a pending local transaction
                    string query =
                        @"insert into APPS.XX_PRODUCTION_ENTRY (SEQ_ID,ORG_ID, TRAN_DATE,EBS_ITEM_ID,ITEM_MODEL,ITEM_COLOR,ITEM_VERSION,JOB_ID,SERIAL_NO,SERIAL_NO2,IMP_FLAG,IMP_REF,CREATED_BY,CREATION_DATE,LAST_UPDATE_BY,LAST_UPDATE_DATE,REMARKS,DATA_SOURCE,GRADE,GRADE_REASON )  
                                  values (:seqId,:orgId, :tranDate, :ebsItemId,:itemModel,:itemColor,:itemVersion,:jobId,:serialNo,:serialNo2,:impFlag,:impRef,:createdBy,:createdDate,:lastUpdateBy,:lastUpdateDate,:remarks,:dataSource,:grade,:gradeReason)";  //GRADE :grade ,GRADE_REASON :gradeReason
                    using (var command = pushconnection.CreateCommand())
                    {
                        //command.Transaction = transaction;

                        command.CommandText = query;
                        command.CommandType = CommandType.Text;
                        command.BindByName = true;
                        // In order to use ArrayBinding, the ArrayBindCount property
                        // of OracleCommand object must be set to the number of records to be inserted
                        command.ArrayBindCount = oracleUploadList.Count;
                        command.Parameters.Add(":seqId", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.SequenceId).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":orgId", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.OrganizationId).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":tranDate", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.TranscationDate).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":ebsItemId", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.EbsItemId).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":itemModel", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.ItemModel).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":itemColor", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.ItemColor).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":itemVersion", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.ItemVersion).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":jobId", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.JobId).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":serialNo", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.SeriaNo).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":serialNo2", OracleDbType.Varchar2,
                           oracleUploadList.Select(c => c.SeriaNo2).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":impFlag", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.ImpFlag).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":impRef", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.ImpReference).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":createdBy", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.CreatedBy).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":createdDate", OracleDbType.Date,
                            oracleUploadList.Select(c => c.CreationDate).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":lastUpdateBy", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.LastUpdatedBy).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":lastUpdateDate", OracleDbType.Date,
                            oracleUploadList.Select(c => c.LastDateUpdatedDate).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":remarks", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.Remarks).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":dataSource", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.DataSource).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":grade", OracleDbType.Varchar2,
                           oracleUploadList.Select(c => c.Grade).ToArray(), ParameterDirection.Input);

                        command.Parameters.Add(":gradeReason", OracleDbType.Varchar2,
                          oracleUploadList.Select(c => c.GradeReason).ToArray(), ParameterDirection.Input);
                        if (oracleUploadList.Count > 0)
                        {
                            int result = command.ExecuteNonQuery();
                            if (result == oracleUploadList.Count)
                            {
                                // transaction.Commit();
                                returnValue = true;
                                pushconnection.Close();
                            }

                            else
                            {
                                // transaction.Rollback();
                                pushconnection.Close();
                            }
                        }
                        else
                        {
                            returnValue = true;
                            pushconnection.Close();
                        }


                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                    pushconnection.Close();
                    _entity.Database.Connection.Close();
                    //Log error thrown
                }
                finally
                {
                    pushconnection.Close();
                    _entity.Database.Connection.Close();
                }
            }




            return returnValue;

        }


    }
}