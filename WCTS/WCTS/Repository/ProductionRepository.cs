﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Transactions;
using System.Web;
using System.Web.Http.Results;
using BR_BLL;
using Oracle.ManagedDataAccess.Client;
using OracleDatabaseConnections;
using WCMS_MAIN.HelperClass;
using WCTS.Helper_Model;
using WCTS.Models;
using WCTS.Models.EntityModel;

namespace WCTS.Repository
{
    public class ProductionRepository
    {
        private readonly Dictionary<int, SessionData> _sessiondictionary = SessionData.GetSessionValues();
        private readonly WCTSEntities _entities = new WCTSEntities();

        internal bool InsertProductionSetup(ProductionSetup productionSetup)
        {
            try
            {
                _entities.ProductionSetup.Add(productionSetup);
                _entities.SaveChanges();
                return true;


            }

            catch (Exception exception)
            {

                throw exception;

            }

        }



        internal ProductionSetup GetActiveProjectDetailsProductionSetupById(long setupId)
        {
            ProductionSetup record;
            using (var dbEntities = new WCTSEntities())
            {
                record = dbEntities.ProductionSetup.FirstOrDefault(i => i.Id == setupId);
            }
            return record;
        }



        internal ProductionSetup GetActiveProjectDetails(ProductionSetup productionSetup)
        {
            ProductionSetup prodSetup;

            try
            {

                prodSetup = _entities.ProductionSetup
                    .WhereIf(productionSetup.LineId != null, x => x.LineId == productionSetup.LineId)
                    .WhereIf(productionSetup.IsActive != null, x => x.IsActive == productionSetup.IsActive)
                    .FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return prodSetup;
        }

        internal bool UpdateProductionSetup(ProductionSetup isProjectActive)
        {

            try
            {

                _entities.Entry(isProjectActive).State = EntityState.Modified;
                _entities.SaveChanges();

                return true;

            }

            catch (Exception exception)
            {

                throw exception;

            }



        }

        public bool InsertProductionMaster(ProductionMaster pmaster)
        {
            try
            {
                _entities.ProductionMaster.Add(pmaster);
                _entities.SaveChanges();
                return true;


            }

            catch (Exception exception)
            {

                throw exception;

            }
        }

        public List<ProductionMaster> BrcodeItemCount(long lineId, string model, long projectSetupId)
        {
            var masters =
                _entities.ProductionMaster.Where(
                    a => a.IsPrinted == null && a.Model == model && a.ProjectSetupId == projectSetupId).OrderBy(a=>a.AddedDate).ToList();




            return masters;
        }

        internal bool UpdatePrintedBarcodes(List<Result> printedList,string masterBarcode)
        {

            foreach (var result in printedList)
            {

                var id = Convert.ToInt64(result.Id);
                var codes = _entities.ProductionMaster.FirstOrDefault(a => a.Barcode == result.Barcode && a.Id == id);

                if (codes == null) continue;
                codes.IsPrinted = true;
                _entities.Entry(codes).State = EntityState.Modified;
            }
            long firstId = Convert.ToInt64(printedList.FirstOrDefault().Id);


            var masterLog = _entities.MasterBarcodeLog.FirstOrDefault(c => c.MasterBarcode == masterBarcode);
            if (masterLog != null)
            {
                masterLog.IsPrinted = true;
                _entities.Entry(masterLog).State = EntityState.Modified;
            }
            _entities.SaveChanges();

            return true;


        }

        internal ProductionMaster GetBarCodeInfo(string barcode)
        {
            var masters = _entities.ProductionMaster.FirstOrDefault(a => a.Barcode == barcode);

            return masters;
        }

        public int GetTotalBarcode(string masterBarcode)
        {
            var totalBarcode = _entities.ProductionMaster.Count(a => a.MasterBarcode == masterBarcode);

            return totalBarcode;
        }

        internal List<ProductionMaster> GetLotBarcodeInformation(string barcode)
        {
            var infos = _entities.ProductionMaster.Where(a => a.LotBarcode == barcode && (a.IsPassed == null || a.IsPassed == false) && (a.IsHold == null || a.IsHold == false) && (a.IsRejected == null || a.IsRejected == false)).ToList();

            return infos;
        }

        internal List<ProductionMaster> GetLotBarcodeInformationForDateSerial(string barcode)
        {
            var infos = _entities.ProductionMaster.Where(a => a.LotBarcode == barcode && (a.IsPassed == null || a.IsPassed == false) && (a.IsHold == null || a.IsHold == false) && (a.IsRejected == null || a.IsRejected == false)).ToList();

            return infos;
        }




        internal List<QcItems> GetLotBarcodeInformationForQcPass(string lotbarcode)
        {
            var userId = (long)_sessiondictionary[1].Id;
            List<QcItems> infos;
            var transCode = RandomString();
            using (var dbEntities = new WCTSEntities())
            {
                var data = dbEntities.ProductionMaster.Where(
                        a =>
                            a.LotBarcode == lotbarcode && (a.IsPassed == null || a.IsPassed == false) &&
                            (a.IsHold == null || a.IsHold == false) && (a.IsRejected == null || a.IsRejected == false))
                        .ToList();
                 infos = data.Select(item => item.ProjectSetupId != null ? new QcItems
                 {
                     QcCheckedTrackingId = item.Model,
                     Model = item.Model,
                     Barcode = item.Barcode,
                     LotBarcode = item.LotBarcode,
                     MasterBarcode = item.MasterBarcode,
                     ProjectSetupId = item.ProjectSetupId,
                     Line = GetActiveProjectDetailsProductionSetupById((long)item.ProjectSetupId).LineId.ToString(),
                     Shift = item.ShiftId,
                     Passed = true,
                     IsHold = false,
                     IsRejected = false,
                     AddedBy = userId,
                     AddedDate = DateTime.Now,
                     UpdatedBy = userId,
                     UpdatedDate = DateTime.Now,
                     Category = item.CategoryCode,
                     ItemCode = item.ItemCode,
                     OracleTransactionCode = transCode,
                     Serial = item.LotBarcode.Substring(item.LotBarcode.Length - 4)
                 } : null)

                .ToList();



            }



            
            return infos;
        }



        //internal List<ProductionMaster> BrcodeItemTotalCount(long projectSetupId)
        //{
        //    DateTime startDate = DateTime.Today;
        //    DateTime endDate = startDate.AddDays(1).AddTicks(-1);

        //    var list = _entities.ProductionMaster.Where(a => a.ProjectSetupId == projectSetupId).ToList();

        //    // var list = _entities.ProductionMaster.Where(a => a.AddedDate >= startDate && a.AddedDate <= endDate && a.ProjectSetupId == projectSetupId).ToList();
        //    return list;
        //}


        internal List<ProductionMaster> BrcodeItemTotalCount(string date,string shiftId)
        {
            DateTime startDate = Convert.ToDateTime(date);
            DateTime endDate = startDate.AddDays(1).AddTicks(-1);

            //var list = _entities.ProductionMaster.Where(a => a.ProjectSetupId == projectSetupId).ToList();

            var list = _entities.ProductionMaster.Where(a => a.ProductionDate >= startDate && a.ProductionDate <= endDate && a.ShiftId == shiftId).ToList();
            return list;
        }

        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        internal bool InsertQcPanelInfo(List<QcItems> qcItemses, DataTable data)
        {
            try
            {
                int a = 0;
                int b = 0;
                var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString);
                con.Open();

                using (var sqlTransaction = con.BeginTransaction())
                {

                    using (
                       var sqlBulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default,
                           sqlTransaction))
                    {
                        sqlBulkCopy.DestinationTableName = "dbo.QcItems"; //"dbo.tblIMEIRecord";
                        sqlBulkCopy.ColumnMappings.Add("Model", "QcCheckedTrackingId");
                        sqlBulkCopy.ColumnMappings.Add("Model", "Model");
                        sqlBulkCopy.ColumnMappings.Add("Barcode", "Barcode");
                        sqlBulkCopy.ColumnMappings.Add("LotBarcode", "LotBarcode");
                        sqlBulkCopy.ColumnMappings.Add("MasterBarcode", "MasterBarcode");
                        sqlBulkCopy.ColumnMappings.Add("Passed", "Passed");
                        sqlBulkCopy.ColumnMappings.Add("IsHold", "IsHold");
                        sqlBulkCopy.ColumnMappings.Add("IsRejected", "IsRejected");
                        sqlBulkCopy.ColumnMappings.Add("ProjectSetupId", "ProjectSetupId");
                        sqlBulkCopy.ColumnMappings.Add("AddedBy", "AddedBy");
                        sqlBulkCopy.ColumnMappings.Add("AddedDate", "AddedDate");
                        sqlBulkCopy.ColumnMappings.Add("UpdatedBy", "UpdatedBy");
                        sqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                        try
                        {
                            sqlBulkCopy.WriteToServer(data);
                            sqlTransaction.Commit();
                            return true;
                        }
                        catch (Exception exception)
                        {
                            sqlTransaction.Rollback();
                            con.Close();
                            //con.Close();
                            return false;
                        }
                    }


                }
                //var lotBarcode = qcItemses[0].LotBarcode;
                //var sql = string.Format(@"Update ProductionMaster SET IsPassed=1 WHERE  LotBarcode='" + lotBarcode + "' AND IsRejected!=1 AND IsHold!=1");
                //var nwcomm = new SqlCommand("Update ProductionMaster SET IsPassed=1 WHERE  LotBarcode='" + lotBarcode + "' AND (IsRejected<>1 OR IsRejected IS NULL )AND (IsHold<>1 OR IsHold IS NULL ) ", con);
                //try
                //{
                //    var kk = nwcomm.ExecuteNonQuery();
                //    if (kk <= 0)
                //    {
                //        var nwdltcomm = new SqlCommand("DELETE FROM  QcItems WHERE  LotBarcode='" + lotBarcode + "'", con);
                //        nwdltcomm.ExecuteNonQuery();
                //        con.Close();
                //        return false;
                //    }
                //    con.Close();
                //}
                //catch (Exception)
                //{

                //    var nwdltcomm = new SqlCommand("DELETE FROM  QcItems WHERE  LotBarcode='" + lotBarcode + "'", con);
                //    nwdltcomm.ExecuteNonQuery();
                //    con.Close();
                //    return false;
                //}
             

            }

            catch (Exception ex)
            {
                
                
                return false;
            }

        }




        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        internal bool InsertQcPassInfoByMasterBarcode(DataTable data, string mastercode)
        {
            try
            {
                int a = 0;
                int b = 0;
                var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString);
                con.Open();

                using (var sqlTransaction = con.BeginTransaction())
                {

                    using (
                       var sqlBulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default,
                           sqlTransaction))
                    {
                        sqlBulkCopy.DestinationTableName = "dbo.QcItems"; //"dbo.tblIMEIRecord";
                        sqlBulkCopy.ColumnMappings.Add("Model", "QcCheckedTrackingId");
                        sqlBulkCopy.ColumnMappings.Add("Model", "Model");
                        sqlBulkCopy.ColumnMappings.Add("Barcode", "Barcode");
                        sqlBulkCopy.ColumnMappings.Add("LotBarcode", "LotBarcode");
                        sqlBulkCopy.ColumnMappings.Add("MasterBarcode", "MasterBarcode");
                        sqlBulkCopy.ColumnMappings.Add("Passed", "Passed");
                        sqlBulkCopy.ColumnMappings.Add("IsHold", "IsHold");
                        sqlBulkCopy.ColumnMappings.Add("IsRejected", "IsRejected");
                        sqlBulkCopy.ColumnMappings.Add("ProjectSetupId", "ProjectSetupId");
                        sqlBulkCopy.ColumnMappings.Add("AddedBy", "AddedBy");
                        sqlBulkCopy.ColumnMappings.Add("AddedDate", "AddedDate");
                        sqlBulkCopy.ColumnMappings.Add("UpdatedBy", "UpdatedBy");
                        sqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                        try
                        {
                            sqlBulkCopy.WriteToServer(data);
                            sqlTransaction.Commit();
                            return true;

                        }
                        catch (Exception exception)
                        {
                            sqlTransaction.Rollback();
                            con.Close();
                            return false;
                        }
                    }


                }
               // var lotBarcode = qcItemses[0].LotBarcode;
                //var sql = string.Format(@"Update ProductionMaster SET IsPassed=1 WHERE  LotBarcode='" + lotBarcode + "' AND IsRejected!=1 AND IsHold!=1");
                //var nwcomm = new SqlCommand("Update ProductionMaster SET IsPassed=1 WHERE  MasterBarcode='" + mastercode + "' AND (IsRejected<>1 OR IsRejected IS NULL )AND (IsHold<>1 OR IsHold IS NULL ) ", con);
                //try
                //{
                //    var kk = nwcomm.ExecuteNonQuery();
                //    if (kk <= 0)
                //    {
                //        var nwdltcomm = new SqlCommand("DELETE FROM  QcItems WHERE  LotBarcode='" + lotBarcode + "'", con);
                //        nwdltcomm.ExecuteNonQuery();
                //        con.Close();
                //        return false;
                //    }
                //    con.Close();
                //}
                //catch (Exception)
                //{

                //    var nwdltcomm = new SqlCommand("DELETE FROM  QcItems WHERE  LotBarcode='" + lotBarcode + "'", con);
                //    nwdltcomm.ExecuteNonQuery();
                //    con.Close();
                //    return false;
                //}
               

            }

            catch (Exception ex)
            {
                throw ex;
            }

        }


        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        internal bool InsertPassDataByBarcode(DataTable data, string barcode)
        {
            try
            {
                int a = 0;
                int b = 0;
                var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString);
                con.Open();

                using (var sqlTransaction = con.BeginTransaction())
                {

                    using (
                       var sqlBulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default,
                           sqlTransaction))
                    {
                        sqlBulkCopy.DestinationTableName = "dbo.QcItems"; //"dbo.tblIMEIRecord";
                        sqlBulkCopy.ColumnMappings.Add("Model", "QcCheckedTrackingId");
                        sqlBulkCopy.ColumnMappings.Add("Model", "Model");
                        sqlBulkCopy.ColumnMappings.Add("Barcode", "Barcode");
                        sqlBulkCopy.ColumnMappings.Add("LotBarcode", "LotBarcode");
                        sqlBulkCopy.ColumnMappings.Add("MasterBarcode", "MasterBarcode");
                        sqlBulkCopy.ColumnMappings.Add("Passed", "Passed");
                        sqlBulkCopy.ColumnMappings.Add("IsHold", "IsHold");
                        sqlBulkCopy.ColumnMappings.Add("IsRejected", "IsRejected");
                        sqlBulkCopy.ColumnMappings.Add("ProjectSetupId", "ProjectSetupId");
                        sqlBulkCopy.ColumnMappings.Add("AddedBy", "AddedBy");
                        sqlBulkCopy.ColumnMappings.Add("AddedDate", "AddedDate");
                        sqlBulkCopy.ColumnMappings.Add("UpdatedBy", "UpdatedBy");
                        sqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                        try
                        {
                            sqlBulkCopy.WriteToServer(data);
                            sqlTransaction.Commit();
                            return true;

                        }
                        catch (Exception exception)
                        {
                            sqlTransaction.Rollback();
                            con.Close();
                            return false;
                        }
                    }


                }
                // var lotBarcode = qcItemses[0].LotBarcode;
                //var sql = string.Format(@"Update ProductionMaster SET IsPassed=1 WHERE  LotBarcode='" + lotBarcode + "' AND IsRejected!=1 AND IsHold!=1");
                //var nwcomm = new SqlCommand("Update ProductionMaster SET IsPassed=1 WHERE  MasterBarcode='" + mastercode + "' AND (IsRejected<>1 OR IsRejected IS NULL )AND (IsHold<>1 OR IsHold IS NULL ) ", con);
                //try
                //{
                //    var kk = nwcomm.ExecuteNonQuery();
                //    if (kk <= 0)
                //    {
                //        var nwdltcomm = new SqlCommand("DELETE FROM  QcItems WHERE  LotBarcode='" + lotBarcode + "'", con);
                //        nwdltcomm.ExecuteNonQuery();
                //        con.Close();
                //        return false;
                //    }
                //    con.Close();
                //}
                //catch (Exception)
                //{

                //    var nwdltcomm = new SqlCommand("DELETE FROM  QcItems WHERE  LotBarcode='" + lotBarcode + "'", con);
                //    nwdltcomm.ExecuteNonQuery();
                //    con.Close();
                //    return false;
                //}


            }

            catch (Exception ex)
            {
                throw ex; 
            }

        }

        public List<ProductionMaster> GetMasterBarcodeData(string masterBarcode)
        {
            var masters = _entities.ProductionMaster.Where(a => a.MasterBarcode == masterBarcode.Trim() && (a.IsPassed == null || a.IsPassed == false || a.IsPassed == true) && (a.IsHold == null || a.IsHold == false) && (a.IsRejected == null || a.IsRejected == false)).OrderBy(a=>a.AddedDate).ToList();
            return masters;
        }

        public List<ProductionMaster> GetMasterBarcodeDataForQc(string masterBarcode)
        {
            var masters = _entities.ProductionMaster.Where(a => a.MasterBarcode == masterBarcode.Trim() && (a.IsPassed == null || a.IsPassed == false )).ToList();
            return masters;
        }

        public List<QcItems> GetMasterBarcodeDataForQcPass(string masterBarcode)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var transCode =RandomString().ToUpper();
            List<QcItems> infos;

            using (var dbEntities = new WCTSEntities())
            {
                var data =
                dbEntities.ProductionMaster.Where(
                    a => a.MasterBarcode == masterBarcode && (a.IsPassed == null || a.IsPassed == false)).ToList();
                 infos =
             data.Select(item => item.ProjectSetupId != null ? new QcItems
             {
                 QcCheckedTrackingId = item.Model,
                 Model = item.Model,
                 Barcode = item.Barcode,
                 LotBarcode = item.LotBarcode,
                 MasterBarcode = item.MasterBarcode,
                 ProjectSetupId = item.ProjectSetupId,
                 Line = GetActiveProjectDetailsProductionSetupById((long)item.ProjectSetupId).LineId.ToString(),
                 Shift = item.ShiftId,
                 Passed = true,
                 IsHold = false,
                 IsRejected = false,
                 AddedBy = userId,
                 AddedDate = DateTime.Now,
                 UpdatedBy = userId,
                 UpdatedDate = DateTime.Now,
                 Category = item.CategoryCode,
                 ItemCode = item.ItemCode,
                 OracleTransactionCode = transCode,
                 Serial = item.LotBarcode.Substring(item.LotBarcode.Length - 4)
             } : null).ToList();
            }

            return infos;
        }



        public List<QcItems> CheckAndUploadToOracle(string model,string lot,string transcode)
        {
            List<QcItems> infos;
            using (var dbEntities = new WCTSEntities())
            {
                infos = dbEntities.QcItems.Where(
                    a => a.Model == model && a.LotBarcode==lot&& a.OracleTransactionCode==transcode && a.OracleUploaded==null).ToList();
            }
            return infos;

        }


        internal List<ProductionSetup> GetLotBarcodeByDateAndSerial(string date, string serial)
        {

            try
            {
                var startDate = Convert.ToDateTime(date);
                var endDate = Convert.ToDateTime(date);
                var toDate = endDate.AddDays(1).AddTicks(-1);
                var list = _entities.ProductionSetup.Where(x => x.SerialNumber == serial && (x.ProductionDate >= startDate && x.ProductionDate <= toDate)).ToList();

                return list;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        internal bool HoldLotItems(string lotNumebr, long userId, string remarks)
        {
            try
            {
                var productionSetupData = _entities.ProductionSetup.FirstOrDefault(x => x.LotNumber == lotNumebr);
                _entities.Database.Connection.Close();
                int a = 0;
                using (
                    var transaction = new TransactionScope(TransactionScopeOption.Required,
                        ApplicationState.TransactionOptions))
                {
                    using (
                        var con =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString))
                    {

                        con.Open();

                        var lotBarcode = lotNumebr;

                        var lotTracking = Guid.NewGuid();

                        if (productionSetupData != null)
                        {
                            var ncom = new SqlCommand(
                                "insert into LotHold(LotNumber,LotTrackingSerial,ProductionDate,AddedBy,AddedDate)" +
                                " values('" + lotNumebr + "','" + productionSetupData.SerialNumber + "','" + productionSetupData.ProductionDate + "'," +
                                "'" + userId + "','" + DateTime.Now + "')", con);



                            var nncom = new SqlCommand(
                               "  INSERT INTO LotHold(HoldTrackingId,LotNumber,BarCode,MasterBarcode,LotTrackingSerial,ProductionDate,ProductionAddedDate,Remarks,HoldType,Passed,Hold,AddedBy,AddedDate)" +
                               "SELECT '" + lotTracking + "',LotBarcode,Barcode,MasterBarcode, RIGHT(LotBarcode,4),ProductionDate,AddedDate,'" + remarks + "','Lot Wise',0,1,'" + userId + "',GETDATE()FROM ProductionMaster WHERE LotBarcode='" + lotNumebr + "' " +
                               "AND (IsPassed=0 OR IsPassed IS NULL) AND (IsHold=0 OR IsHold IS NULL)  AND (IsRejected=0 OR IsRejected IS NULL) ", con);


                            a = nncom.ExecuteNonQuery();
                        }


                        // _entities.Database.Connection.Close();

                        if (a <= 0) return false;

                        var qcTracking = Guid.NewGuid();

                        var nqccom = new SqlCommand(
                               "  INSERT INTO QcItems(QcCheckedTrackingId,Model,Barcode,LotBarcode,MasterBarcode,ProjectSetupId,Passed,IsHold,IsRejected,AddedBy,AddedDate)" +
                               "SELECT '" + qcTracking + "',Model,Barcode,LotBarcode,MasterBarcode,ProjectSetupId,0,1,0,'" + userId + "','" + DateTime.Now + "' FROM ProductionMaster WHERE LotBarcode='" + lotNumebr + "' AND (IsPassed=0 OR IsPassed IS NULL) AND (IsHold=0 OR IsHold IS NULL)  AND (IsRejected=0 OR IsRejected IS NULL) ", con);
                        var qc = nqccom.ExecuteNonQuery();
                        if (qc <= 0) return false;


                        //string sql = string.Format(@"Update ProductionMaster SET IsPassed=0 WHERE  LotBarcode=" + lotBarcode + "");
                        var nwcomm = new SqlCommand("Update ProductionMaster SET IsHold=1 WHERE  LotBarcode='" + lotBarcode + "'", con);

                        var kk = nwcomm.ExecuteNonQuery();

                        if (kk <= 0) return false;
                        con.Close();
                        transaction.Complete();
                        return true;

                    }

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ProductionMaster> BrcodeItemCountManual(long lineId, string model, long projectSetupId)
        {
            var masters = _entities.ProductionMaster.Where(a => a.IsPrinted == null).OrderBy(a=>a.AddedDate).ToList();
            return masters;
        }


        public List<LotHold> GetHoldItems()
        {
            var holdData = _entities.LotHold.Where(a => a.Passed == false && a.Hold == true).OrderBy(a => a.AddedDate).ToList();
            return holdData;
        }
        public List<RejectedItems> GetrejectedItems()
        {
            var holdData = _entities.RejectedItems.Where(a => a.Passed == false).OrderBy(a => a.AddedDate).ToList();
            return holdData;
        }




        public List<LotHold> GetHoldedItemDetailsData(string holdTracking, string lotNo, string serial)
        {
            var holdData = _entities.LotHold.Where(a => a.HoldTrackingId == holdTracking && a.LotNumber == lotNo && a.LotTrackingSerial == serial && a.Passed == false && a.Hold == true).OrderBy(a => a.AddedDate).ToList();
            return holdData;
        }

        internal bool HoldMasterBarcodeItems(string masterBarcodeNumebr, long userId, string remarks, string lotNumebr)
        {
            try
            {
                var productionSetupData = _entities.ProductionSetup.FirstOrDefault(x => x.LotNumber == lotNumebr);
                _entities.Database.Connection.Close();
                int a = 0;
                using (
                    var transaction = new TransactionScope(TransactionScopeOption.Required,
                        ApplicationState.TransactionOptions))
                {
                    using (
                        var con =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString))
                    {

                        con.Open();

                        var lotBarcode = lotNumebr;

                        var lotTracking = Guid.NewGuid();

                        if (productionSetupData != null)
                        {
                            var ncom = new SqlCommand(
                                "insert into LotHold(LotNumber,LotTrackingSerial,ProductionDate,AddedBy,AddedDate)" +
                                " values('" + lotNumebr + "','" + productionSetupData.SerialNumber + "','" + productionSetupData.ProductionDate + "'," +
                                "'" + userId + "','" + DateTime.Now + "')", con);



                            var nncom = new SqlCommand(
                               "  INSERT INTO LotHold(HoldTrackingId,LotNumber,BarCode,MasterBarcode,LotTrackingSerial,ProductionDate,ProductionAddedDate,Remarks,HoldType,Passed,Hold,AddedBy,AddedDate)" +
                               "SELECT '" + lotTracking + "',LotBarcode,Barcode,MasterBarcode, RIGHT(LotBarcode,4),ProductionDate,AddedDate,'" + remarks + "','Master Barcode',0,1,'" + userId + "',GETDATE()FROM ProductionMaster WHERE MasterBarcode='" + masterBarcodeNumebr + "' AND (IsPassed=0 OR IsPassed IS NULL) AND (IsHold=0 OR IsHold IS NULL)  AND (IsRejected=0 OR IsRejected IS NULL) ", con);


                            a = nncom.ExecuteNonQuery();
                        }


                        // _entities.Database.Connection.Close();

                        if (a <= 0) return false;

                        var qcTracking = Guid.NewGuid();

                        var nqccom = new SqlCommand(
                               "  INSERT INTO QcItems(QcCheckedTrackingId,Model,Barcode,LotBarcode,MasterBarcode,ProjectSetupId,Passed,IsHold,IsRejected,AddedBy,AddedDate)" +
                               "SELECT '" + qcTracking + "',Model,Barcode,LotBarcode,MasterBarcode,ProjectSetupId,0,1,0,'" + userId + "','" + DateTime.Now + "' FROM ProductionMaster WHERE MasterBarcode='" + masterBarcodeNumebr + "' AND (IsPassed=0 OR IsPassed IS NULL) AND (IsHold=0 OR IsHold IS NULL)  AND (IsRejected=0 OR IsRejected IS NULL) ", con);
                        var qc = nqccom.ExecuteNonQuery();
                        if (qc <= 0) return false;


                        //string sql = string.Format(@"Update ProductionMaster SET IsPassed=0 WHERE  LotBarcode=" + lotBarcode + "");
                        //var nwcomm = new SqlCommand("Update ProductionMaster SET IsHold=1 WHERE  MasterBarcode='" + masterBarcodeNumebr + "' AND  IsPassed IS NULL ", con);

                        //var kk = nwcomm.ExecuteNonQuery();

                        //if (kk <= 0) return false;
                        con.Close();
                        transaction.Complete();
                        return true;

                    }

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal bool HoldBarcodeItems(string barcode, long userId, string remarks, string lotNumebr)
        {
            try
            {
                var barcodeData = _entities.ProductionMaster.FirstOrDefault(x => x.Barcode == barcode);
                _entities.Database.Connection.Close();
                int a = 0;
                using (
                    var transaction = new TransactionScope(TransactionScopeOption.Required,
                        ApplicationState.TransactionOptions))
                {
                    using (
                        var con =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString))
                    {

                        con.Open();

                        var lotBarcode = lotNumebr;

                        var lotTracking = Guid.NewGuid();

                        if (barcodeData != null)
                        {

                            var nncom = new SqlCommand(
                               "  INSERT INTO LotHold(HoldTrackingId,LotNumber,BarCode,MasterBarcode,LotTrackingSerial,ProductionDate,ProductionAddedDate,Remarks,HoldType,Passed,Hold,IsRejected,AddedBy,AddedDate)" +
                               "SELECT '" + lotTracking + "',LotBarcode,Barcode,MasterBarcode, RIGHT(LotBarcode,4),ProductionDate,AddedDate,'" + remarks + "','Barcode',0,1,0,'" + userId + "',GETDATE()FROM ProductionMaster WHERE Barcode='" + barcode + "' AND (IsPassed=0 OR IsPassed IS NULL) AND (IsHold=0 OR IsHold IS NULL)  AND (IsRejected=0 OR IsRejected IS NULL) ", con);
                            a = nncom.ExecuteNonQuery();
                        }

                        if (a <= 0) return false;

                        var qcTracking = Guid.NewGuid();

                        var nqccom = new SqlCommand(
                               "  INSERT INTO QcItems(QcCheckedTrackingId,Model,Barcode,LotBarcode,MasterBarcode,ProjectSetupId,Passed,IsHold,IsRejected,AddedBy,AddedDate)" +
                               "SELECT '" + qcTracking + "',Model,Barcode,LotBarcode,MasterBarcode,ProjectSetupId,0,1,0,'" + userId + "','" + DateTime.Now + "' FROM ProductionMaster WHERE Barcode='" + barcode + "' AND (IsPassed=0 OR IsPassed IS NULL) AND (IsHold=0 OR IsHold IS NULL)  AND (IsRejected=0 OR IsRejected IS NULL) ", con);
                        var qc = nqccom.ExecuteNonQuery();
                        if (qc <= 0) return false;

                        //var nwcomm = new SqlCommand("Update ProductionMaster SET IsHold=1 WHERE  Barcode='" + barcode + "' AND  IsPassed IS NULL ", con);

                        //var kk = nwcomm.ExecuteNonQuery();

                        //if (kk <= 0) return false;
                        con.Close();
                        transaction.Complete();
                        return true;

                    }

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal Result PrepareHoldDataToUpdate(List<Models.ProductionModel> holdData, long userId)
        {

            var result = new Result();
            try
            {
                var acounter = 0;
                using (
                    var transaction = new TransactionScope(TransactionScopeOption.Required,
                        ApplicationState.TransactionOptions))
                {
                    using (
                        var con =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString))
                    {
                        foreach (var items in holdData)
                        {
                            con.Open();
                            if (items.Status.ToLower() == "y")
                            {

                                var nncom = new SqlCommand(
                                   "Update LotHold SET Passed=1,Hold=0,PassedBy='" + userId + "',PassedDate='" + DateTime.Now + "' WHERE  Barcode='" + items.BarCode + "' AND MasterBarcode='" + items.MasterBarCode + "' AND LotNumber='" + items.LotNo + "'   ", con);
                                var a = nncom.ExecuteNonQuery();
                                if (a <= 0)
                                {
                                    result.IsSuccess = false;
                                    return result;
                                }
                                var qccom = new SqlCommand(
                                   "Update QcItems SET Passed=1,IsHold=0,IsRejected=0,UpdatedBy='" + userId + "',UpdatedDate='" + DateTime.Now + "' WHERE  Barcode='" + items.BarCode + "' AND MasterBarcode='" + items.MasterBarCode + "' AND LotBarcode='" + items.LotNo + "'   ", con);
                                var qcqr = qccom.ExecuteNonQuery();
                                if (qcqr <= 0)
                                {
                                    result.IsSuccess = false;
                                    return result;
                                }
                                var pmcomm = new SqlCommand("Update ProductionMaster SET IsPassed=1,PassedDate=GETDATE(), IsHold=0 WHERE  Barcode='" + items.BarCode + "' AND MasterBarcode='" + items.MasterBarCode + "' AND LotBarcode='" + items.LotNo + "'  ", con);

                                var kk = pmcomm.ExecuteNonQuery();

                                if (kk <= 0)
                                {
                                    result.IsSuccess = false;
                                    return result;
                                }
                            }

                            if (items.Status.ToLower() == "n")
                            {
                                // var nwcomm = new SqlCommand("Update ProductionMaster SET IsPassed=0,IsHold=0,IsRejected=1 WHERE  Barcode='" + items.BarCode + "'", con);
                                //var a = nwcomm.ExecuteNonQuery();
                                // if (a <= 0)
                                // {
                                //     result.IsSuccess = false;
                                //     return result;
                                // }

                                var rejTracking = Guid.NewGuid();
                                var remarks = "Test";


                                string sql = string.Format(@"INSERT INTO RejectedItems(RejectTrackingId,LotNumber,BarCode,MasterBarcode,ProductionDate,Remarks,Passed,AddedBy,AddedDate)" +
                                       "SELECT '" + rejTracking + "',LotBarcode,Barcode,MasterBarcode,ProductionDate,'" + remarks + "',0,'" + userId + "','" + DateTime.Now + "' FROM ProductionMaster WHERE Barcode='" + items.BarCode + "'");
                                var nqccom = new SqlCommand(
                                       "INSERT INTO RejectedItems(RejectTrackingId,LotNumber,BarCode,MasterBarcode,ProductionDate,Remarks,Passed,AddedBy,AddedDate)" +
                                       "SELECT '" + rejTracking + "',LotBarcode,Barcode,MasterBarcode,ProductionDate,'" + remarks + "',0,'" + userId + "','" + DateTime.Now + "' FROM ProductionMaster WHERE Barcode='" + items.BarCode + "' ", con);
                                var qcreg = nqccom.ExecuteNonQuery();
                                if (qcreg <= 0)
                                {
                                    result.IsSuccess = false;
                                    return result;
                                }

                                var ltcom = new SqlCommand(
                                   "Update LotHold SET Hold=0,Passed=0,IsRejected=1,RejectedBy='" + userId + "',RejectedDate='" + DateTime.Now + "'  WHERE  Barcode='" + items.BarCode + "' AND MasterBarcode='" + items.MasterBarCode + "' AND LotNumber='" + items.LotNo + "'   ", con);
                                var kk = ltcom.ExecuteNonQuery();
                                if (kk <= 0)
                                {
                                    result.IsSuccess = false;
                                    return result;
                                }
                                var qccom = new SqlCommand(
                                   "Update QcItems SET Passed=0,IsHold=0,IsRejected=1,UpdatedBy='" + userId + "',UpdatedDate='" + DateTime.Now + "' WHERE  Barcode='" + items.BarCode + "' AND MasterBarcode='" + items.MasterBarCode + "' AND LotBarcode='" + items.LotNo + "'   ", con);
                                var qcqr = qccom.ExecuteNonQuery();
                                if (qcqr <= 0)
                                {
                                    result.IsSuccess = false;
                                    return result;
                                }


                            }

                            con.Close();
                            acounter += 1;


                        }


                        if (acounter == holdData.Count())
                        {

                            transaction.Complete();
                            result.IsSuccess = true;
                            result.Message = "Items Processed Successfully";
                        }
                        else
                        {
                            transaction.Dispose();
                            result.IsSuccess = false;
                        }

                    }

                }

            }
            catch (Exception exception)
            {

                result.Message = exception.ToString();
                result.IsSuccess = false;
            }
            return result;
        }

        internal Result PrepareRejectedDataToUpdate(List<Models.ProductionModel> rejected, long userId)
        {




            var result = new Result();
            try
            {
                var acounter = 0;
                using (
                    var transaction = new TransactionScope(TransactionScopeOption.Required,
                        ApplicationState.TransactionOptions))
                {
                    using (
                        var con =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString))
                    {
                        foreach (var items in rejected)
                        {
                            con.Open();
                            if (items.Status.ToLower() == "y")
                            {

                                var nncom = new SqlCommand(
                                   "Update LotHold SET Passed=1,Hold=0,IsRejected=0,PassedBy='" + userId + "',PassedDate='" + DateTime.Now + "' WHERE  Barcode='" + items.BarCode + "' AND MasterBarcode='" + items.MasterBarCode + "' AND LotNumber='" + items.LotNo + "'   ", con);
                                var a = nncom.ExecuteNonQuery();
                                if (a <= 0)
                                {
                                    result.IsSuccess = false;
                                    return result;
                                }
                                var qccom = new SqlCommand(
                                   "Update QcItems SET Passed=1,IsRejected=0,IsHold=0,UpdatedBy='" + userId + "',UpdatedDate='" + DateTime.Now + "' WHERE  Barcode='" + items.BarCode + "' AND MasterBarcode='" + items.MasterBarCode + "' AND LotBarcode='" + items.LotNo + "'   ", con);
                                var qcqr = qccom.ExecuteNonQuery();
                                if (qcqr <= 0)
                                {
                                    result.IsSuccess = false;
                                    return result;
                                }
                                //var pmcomm = new SqlCommand("Update ProductionMaster SET IsPassed=1, IsHold=0 WHERE  Barcode='" + items.BarCode + "' AND MasterBarcode='" + items.MasterBarCode + "' AND LotBarcode='" + items.LotNo + "'  ", con);

                                //var kk = pmcomm.ExecuteNonQuery();

                                //if (kk <= 0)
                                //{
                                //    result.IsSuccess = false;
                                //    return result;
                                //}

                                var rejcomm = new SqlCommand("Update RejectedItems SET Passed=1,UpdatedBy='" + userId + "',UpdatedDate='" + DateTime.Now + "' WHERE  Barcode='" + items.BarCode + "' AND MasterBarcode='" + items.MasterBarCode + "' AND LotNumber='" + items.LotNo + "'  ", con);

                                var rejkk = rejcomm.ExecuteNonQuery();

                                if (rejkk <= 0)
                                {
                                    result.IsSuccess = false;
                                    return result;
                                }

                            }

                            con.Close();
                            acounter += 1;


                        }


                        if (acounter == rejected.Count())
                        {

                            transaction.Complete();
                            result.IsSuccess = true;
                            result.Message = "Items Processed Successfully";
                        }
                        else
                        {
                            transaction.Dispose();
                            result.IsSuccess = false;
                        }

                    }

                }

            }
            catch (Exception exception)
            {

                result.Message = exception.ToString();
                result.IsSuccess = false;
            }
            return result;



        }

        internal List<QcItems> IsAlreadyInQC(QcItems qcItems)
        {
            List<QcItems> qc;

            try
            {

                qc = _entities.QcItems
                    .WhereIf(qcItems.LotBarcode != null, x => x.LotBarcode == qcItems.LotBarcode)
                    .WhereIf(qcItems.MasterBarcode != null, x => x.MasterBarcode == qcItems.MasterBarcode)
                    .WhereIf(qcItems.Barcode != null, x => x.Barcode == qcItems.Barcode)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return qc;
        }

        internal List<ProductionModel> GetProductionTimeWiseData(string fromdate, string todate)
        {


            try
            {
                var list = new List<ProductionModel>();
                String connectionString = ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString;


                var queryString = "SELECT * FROM ProductionMaster WHERE FORMAT(AddedDate, 'yyyy-MM-dd HH:mm') Between '" + fromdate + "' AND '" + todate + "' AND (IsPassed=0 OR IsPassed IS NULL) AND (IsHold=0 OR IsHold IS NULL)  AND (IsRejected=0 OR IsRejected IS NULL)";
                using (var connection =
                    new SqlConnection(connectionString))
                {

                    var command = new SqlCommand(queryString, connection);

                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var data = new ProductionModel();
                            var model = reader["Model"];
                            var barcode = reader["Barcode"];
                            var lot = reader["LotBarcode"];
                            var master = reader["MasterBarcode"];
                            var pdate = reader["ProductionDate"];
                            var adate = reader["AddedDate"];

                            data.Model = model == null ? "" : model.ToString();
                            data.BarCode = barcode == null ? "" : barcode.ToString();
                            data.LotNo = lot == null ? "" : lot.ToString();
                            data.MasterBarCode = master == null ? "" : master.ToString();
                            data.ProductionDate = Convert.ToDateTime(pdate);
                            data.ScannedDate = Convert.ToDateTime(adate);
                            list.Add(data);
                        }
                        reader.Close();
                        connection.Close();
                    }
                    catch (Exception ex) { throw ex; };
                }





                return list;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        //Prepare Time wise data

        internal Result PrepareScannedTimeWiseData(List<Models.ProductionModel> holdData, long userId)
        {

            var result = new Result();
            try
            {
                var acounter = 0;
                var misscounter = 0;
                var lotTracking = Guid.NewGuid();
                var qcTracking = Guid.NewGuid();
                using (
                    var transaction = new TransactionScope(TransactionScopeOption.Required,
                        ApplicationState.TransactionOptions))
                {
                    using (
                        var con =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString))
                    {
                        foreach (var items in holdData)
                        {
                            con.Open();
                            //var sqlduplicateCommand = new SqlCommand("SELECT COUNT(*) from [tblBarCodeInv] where [BarCode]= @im1 OR [BarCode2]= @im1",

                            if (items.Status.ToLower() == "y")
                            {

                                var nqccom = new SqlCommand(
                               "  INSERT INTO QcItems(QcCheckedTrackingId,Model,Barcode,LotBarcode,MasterBarcode,ProjectSetupId,Passed,IsHold,IsRejected,AddedBy,AddedDate,UpdatedBy,UpdatedDate)" +
                               "SELECT '" + qcTracking + "',Model,Barcode,LotBarcode,MasterBarcode,ProjectSetupId,1,0,0,'" + userId + "','" + DateTime.Now + "' ,'" + userId + "','" + DateTime.Now + "' FROM ProductionMaster WHERE Barcode='" + items.BarCode + "' ", con);
                                var qc = nqccom.ExecuteNonQuery();
                                if (qc <= 0)
                                {
                                    result.IsSuccess = false;
                                    return result;
                                }  
                            }

                            if (items.Status.ToLower() == "n")
                            {
                              
                               var nncom = new SqlCommand(
                               "  INSERT INTO LotHold(HoldTrackingId,LotNumber,BarCode,MasterBarcode,LotTrackingSerial,ProductionDate,ProductionAddedDate,Remarks,HoldType,Passed,Hold,IsRejected,AddedBy,AddedDate)" +
                               "SELECT '" + lotTracking + "',LotBarcode,Barcode,MasterBarcode, RIGHT(LotBarcode,4),ProductionDate,AddedDate,' Hold ','Barcode',0,1,0,'" + userId + "',GETDATE()FROM ProductionMaster WHERE Barcode='" + items.BarCode + "' ", con);
                           var a = nncom.ExecuteNonQuery();


                           if (a <= 0)
                           {
                               result.IsSuccess = false;
                               return result;
                           }

                        var nqccom = new SqlCommand(
                               "  INSERT INTO QcItems(QcCheckedTrackingId,Model,Barcode,LotBarcode,MasterBarcode,ProjectSetupId,Passed,IsHold,IsRejected,AddedBy,AddedDate)" +
                               "SELECT '" + qcTracking + "',Model,Barcode,LotBarcode,MasterBarcode,ProjectSetupId,0,1,0,'" + userId + "','" + DateTime.Now + "' FROM ProductionMaster WHERE Barcode='" + items.BarCode + "' ", con);
                        var qc = nqccom.ExecuteNonQuery();
                        if (qc <= 0)
                            if (qc <= 0)
                            {
                                result.IsSuccess = false;
                                return result;
                            }


                            }

                            con.Close();
                            acounter += 1;


                        }


                        if (acounter == holdData.Count())
                        {

                            transaction.Complete();
                            result.IsSuccess = true;
                            result.Message = "Items Processed Successfully";
                        }
                        else
                        {
                            transaction.Dispose();
                            result.IsSuccess = false;
                        }

                    }

                }

            }
            catch (Exception exception)
            {

                result.Message = exception.ToString();
                result.IsSuccess = false;
            }
            return result;
        }








        internal List<ProductionModel> GetProductionMultiBarcodeData(List<ProductionModel> modelData)
        {
            var list = new List<ProductionModel>();
            try
            {

                foreach (var items in modelData)
                {
                    var masterData = _entities.ProductionMaster.FirstOrDefault(a => a.Barcode == items.BarCode);
                    var data = new ProductionModel();

                    if (masterData == null) continue;
                    data.Model = masterData.Model ?? "";
                    data.BarCode = masterData.Barcode??"";
                    data.LotNo = masterData.LotBarcode??"";
                    data.MasterBarCode = masterData.MasterBarcode??"";
                    data.ProductionDate = Convert.ToDateTime(masterData.ProductionDate);
                    data.ScannedDate = Convert.ToDateTime(masterData.AddedDate);
                    list.Add(data);
                }

            }
            catch (Exception)
            {
                
                throw;
            }

            return list;
        }

        internal bool InsertQcLotTrackingInfo(QcLotTracking qcLotTracking)
        {
            var result = new Result();
            try
            {
                try
                {
                    _entities.QcLotTracking.Add(qcLotTracking);
                    _entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
        }




        internal bool InsertModelName(Model model)
        {
            var result = new Result();
            try
            {
                try
                {
                    _entities.Model.Add(model);
                    _entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }








        internal bool UpdateProductionMaterQcPassedItems(string p)
        {


          //  var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString);
          
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString))
            {
                con.Open();
                try
                {
                    var pmcomm =
                   new SqlCommand("Update ProductionMaster SET IsPassed=1,PassedDate=GETDATE(), IsHold=0 WHERE  LotBarcode='" + p + "'  ",
                       con);
                    var kk = pmcomm.ExecuteNonQuery();
                    con.Close();
                }
                catch (Exception)
                {

                    return false;
                }
               
               

            }

            return true;
        }

        internal bool UpdateQcLotTrackingInfo(QcLotTracking qcLotTracking)
        {
            try
            {
                _entities.Entry(qcLotTracking).State = EntityState.Modified;
                _entities.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        internal bool CheckDuplicateLotNumber(string lotNumber)
        {
            try
            {
                var isExists = _entities.ManualPrintLog.Any(a => a.LotBarCode == lotNumber.Trim());
                return isExists;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        internal bool CheckDuplicateMasterBarcode(string masterCode)
        {
            try
            {
                var isExists = _entities.ManualPrintLog.Any(a => a.MasterBarCode == masterCode.Trim());
                return isExists;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        internal Result InsertManualPrintDataLog(List<ManualPrintLog> manualPrintLog)
        {
            var result = new Result();
           
                try
                {
                    
                   _entities.ManualPrintLog.AddRange(manualPrintLog);
                    _entities.SaveChanges();
                    result.IsSuccess = true;
                   
                }
                catch (Exception ex)
                {
                    result.IsSuccess = false;
                    result.Message = ex.ToString();
                }
                return result;
            }


        internal bool UpdateProductionMasterByLotPass(string p)
        {
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString);
            con.Open();

            try
            {
                //var lotBarcode = qcItemses[0].LotBarcode;
                var sql = string.Format(@"Update ProductionMaster SET IsPassed=1 WHERE  LotBarcode='" + p + "' AND IsRejected!=1 AND IsHold!=1");
                var nwcomm = new SqlCommand("Update ProductionMaster SET IsPassed=1,IsHold=0,IsMoved=1,MovedDate=GETDATE(),PassedDate=GETDATE() WHERE  LotBarcode='" + p + "' And (IsPassed=0 or IsPassed IS NULL) ", con);
                 nwcomm.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                con.Close();
                return false;
            }
            return true;
        }

        internal bool UpdateProductionMasterByMasterBarcodePass(string p)
        {
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString);
            con.Open();

            try
            {
                //var lotBarcode = qcItemses[0].LotBarcode;
                var sql = string.Format(@"Update ProductionMaster SET IsPassed=1 WHERE  LotBarcode='" + p + "' AND IsRejected!=1 AND IsHold!=1");
                var nwcomm = new SqlCommand("Update ProductionMaster SET IsPassed=1,IsHold=0,IsMoved=1,MovedDate=GETDATE(),PassedDate=GETDATE() WHERE  MasterBarcode='" + p + "' And (IsPassed=0 or IsPassed IS NULL) ", con);
                nwcomm.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                con.Close();
                return false;
            }
            return true;
        }

        internal bool InsertQcPanelInfo(List<QcItems> lotItems)
        {
            try
            {
                using (var dbEntities = new WCTSEntities())
                {
                    dbEntities.QcItems.AddRange(lotItems);
                    //dbEntities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }



        internal Result PushQcPassToOracle(List<QcItems> lotItems)
        {
            var employeeId = _sessiondictionary[3].Name;
            int inventoryitemId = 0;
            bool returnValue = false;
            var connection = OracleDatabaseConnection.GetLatestOracleConnection();
            var pushconnection = OracleDatabaseConnection.GetLatestOracleConnection();
            OracleDataReader oracleDataReader = null;
            OracleDataReader oracleDataReader1 = null;
            OracleDataReader oracleDataReader2 = null;
            var result = new Result();
            try
            {
                // connection = OracleDatabaseConnection.GetAnotherOldConnection();
                connection = OracleDatabaseConnection.GetLatestOracleConnection();
                connection.Open();
            }
            catch (Exception)
            {

                connection.Open();
            }

            var projectSetupId = lotItems[0].ProjectSetupId;
            long? setupType = null;
            var itemCode = "";
            if (projectSetupId != null)
            {
                 setupType =GetActiveProjectDetailsProductionSetupById( (long) projectSetupId).InventoryTypeId;
            }
            if (setupType == 1)
            {
                 itemCode = GetModelDataByModelName(lotItems[0].Model).FGItemCode;
            }
            if (setupType == 2)
            {
                itemCode = GetModelDataByModelName(lotItems[0].Model).SFGItemCode;
            }
          

            var itemMasterQuery = @"SELECT   MSI.CREATION_DATE, MSI.ITEM_TYPE, MSI.ORGANIZATION_ID, MSI.INVENTORY_ITEM_ID ,    MSI.SEGMENT1  ITEM_CODE,   MSI.DESCRIPTION ITEM_NAME,
                                    MSI.PRIMARY_UOM_CODE UOM,  MSI.ATTRIBUTE14 ITEM_COLOR, MCB.CATEGORY_ID ,  MCB.SEGMENT1,    MCB.SEGMENT2,    MCB.SEGMENT3 PROD_CAT,     MCB.SEGMENT4,    MCB.SEGMENT5 ITEM_MODEL
                                    FROM (SELECT  MIC.ORGANIZATION_ID,  MIC.INVENTORY_ITEM_ID,  MIC.CATEGORY_ID ,  MCB.SEGMENT1,    MCB.SEGMENT2,    MCB.SEGMENT3,     MCB.SEGMENT4,    MCB.SEGMENT5
                                    FROM    APPS.MTL_CATEGORIES_B MCB, APPS.MTL_ITEM_CATEGORIES MIC    WHERE       MCB.CATEGORY_ID=MIC.CATEGORY_ID         AND         MIC.CATEGORY_SET_ID = 1100000041     AND MIC.ORGANIZATION_ID=125)  MCB, 
                                    APPS.MTL_SYSTEM_ITEMS MSI  WHERE MCB.INVENTORY_ITEM_ID(+)=MSI.INVENTORY_ITEM_ID
                                    AND   MSI.INVENTORY_ITEM_STATUS_CODE='Active'  AND MSI.ITEM_TYPE IN ('FG','SA') AND MSI.ORGANIZATION_ID=125
                                    AND  MSI.SEGMENT1='" + itemCode + "'";


            var oracleCommand = new OracleCommand(itemMasterQuery, connection) { CommandType = CommandType.Text };
            oracleDataReader = oracleCommand.ExecuteReader();
            if (oracleDataReader.HasRows)
            {

                while (oracleDataReader.Read())
                {
                    inventoryitemId = Convert.ToInt32(oracleDataReader["INVENTORY_ITEM_ID"]);
                }
            }
            oracleDataReader.Dispose();
            oracleDataReader.Close();
            connection.Close();
            connection.Dispose();

            if (inventoryitemId != 0)
            {
                //var oracleUploadList = new List<OracleUploadModel>();

                var oracleUploadList = lotItems
                .Select(items => new OracleUploadModel
                {
                      // SequenceId = DateTime.Now.Ticks;
                       OrganizationId = 125,
                       TranscationDate = DateString(),
                       EbsItemId = inventoryitemId,
                       ItemModel = items.Model,
                       ItemVersion = items.LotBarcode,
                       JobId = null,
                       SeriaNo = items.Barcode.Trim(),
                       SeriaNo2 =items.Serial, //items.MasterBarcode,
                       MasterSerialNo =items.MasterBarcode, //items.Serial,
                       ItemColor = "Black",
                       //ImpFlag = 0,
                       // ImpReference = null,
                       CreatedBy = employeeId,
                       Line = items.Line,
                       OracleTransCode = items.OracleTransactionCode,
                       CreationDate = Convert.ToDateTime(String.Format("{0:G}", DateTime.Now)), // DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt");
                       LastUpdatedBy = employeeId,
                       LastDateUpdatedDate = DateTime.Now, //DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt");
                       Remarks = "WCTS-Qc-Uploaded",
                       DataSource = "WCTS",
                       Grade = items.Category,
                      //GradeReason = items.GradeReason != null ? items.GradeReason.Trim() : items.GradeReason
                })

                .ToList();


                try
                {
                    pushconnection.Open();
                    //OracleTransaction transaction = pushconnection.BeginTransaction();
                    // Assign transaction object for a pending local transaction
                    string query =
                        @"insert into APPS.XX_PRODUCTION_ENTRY (ORG_ID, TRAN_DATE,EBS_ITEM_ID,ITEM_MODEL,ITEM_COLOR,ITEM_VERSION,SERIAL_NO,SERIAL_NO2,CREATED_BY,CREATION_DATE,LAST_UPDATE_BY,LAST_UPDATE_DATE,REMARKS,DATA_SOURCE,DATA_SOURCE_ID,ASEM_LINE,GRADE,MASTER_SERIAL_NO)  
                                  values (:orgId, :tranDate, :ebsItemId,:itemModel,:itemColor,:itemVersion,:serialNo,:serialNo2,:createdBy,:createdDate,:lastUpdateBy,:lastUpdateDate,:remarks,:dataSource,:dataSourceid,:line,:grade,:masterserial)";  //GRADE :grade ,GRADE_REASON :gradeReason
                    using (var command = pushconnection.CreateCommand())
                    {
                        //command.Transaction = transaction;

                        command.CommandText = query;
                        command.CommandType = CommandType.Text;
                        command.BindByName = true;
                        // In order to use ArrayBinding, the ArrayBindCount property
                        // of OracleCommand object must be set to the number of records to be inserted
                        command.ArrayBindCount = oracleUploadList.Count;
                        //command.Parameters.Add(":seqId", OracleDbType.Varchar2,
                        //    oracleUploadList.Select(c => c.SequenceId).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":orgId", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.OrganizationId).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":tranDate", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.TranscationDate).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":ebsItemId", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.EbsItemId).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":itemModel", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.ItemModel).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":itemColor", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.ItemColor).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":itemVersion", OracleDbType.Varchar2,
                        oracleUploadList.Select(c => c.ItemVersion).ToArray(), ParameterDirection.Input);
                        //command.Parameters.Add(":jobId", OracleDbType.Varchar2,
                            //oracleUploadList.Select(c => c.JobId).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":serialNo", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.SeriaNo).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":serialNo2", OracleDbType.Varchar2,
                           oracleUploadList.Select(c => c.SeriaNo2).ToArray(), ParameterDirection.Input);
                        //command.Parameters.Add(":impFlag", OracleDbType.Varchar2,
                        //    oracleUploadList.Select(c => c.ImpFlag).ToArray(), ParameterDirection.Input);
                        //command.Parameters.Add(":impRef", OracleDbType.Varchar2,
                        //    oracleUploadList.Select(c => c.ImpReference).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":createdBy", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.CreatedBy).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":createdDate", OracleDbType.Date,
                            oracleUploadList.Select(c => c.CreationDate).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":lastUpdateBy", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.LastUpdatedBy).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":lastUpdateDate", OracleDbType.Date,
                            oracleUploadList.Select(c => c.LastDateUpdatedDate).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":remarks", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.Remarks).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":dataSource", OracleDbType.Varchar2,
                            oracleUploadList.Select(c => c.DataSource).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":dataSourceid", OracleDbType.Varchar2,
                           oracleUploadList.Select(c => c.OracleTransCode).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":line", OracleDbType.Int32,
                           oracleUploadList.Select(c => c.Line).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":grade", OracleDbType.Varchar2,
                           oracleUploadList.Select(c => c.Grade).ToArray(), ParameterDirection.Input);
                        command.Parameters.Add(":masterserial", OracleDbType.Varchar2,
                           oracleUploadList.Select(c => c.MasterSerialNo).ToArray(), ParameterDirection.Input);

                        

                        //command.Parameters.Add(":gradeReason", OracleDbType.Varchar2,
                        //  oracleUploadList.Select(c => c.GradeReason).ToArray(), ParameterDirection.Input);
                        if (oracleUploadList.Count > 0)
                        {
                            int results = command.ExecuteNonQuery();
                            if (results == oracleUploadList.Count)
                            {
                                // transaction.Commit();
                                result.IsSuccess = true;
                                pushconnection.Close();
                            }

                            else
                            {
                                // transaction.Rollback();
                                pushconnection.Close();
                            }
                        }
                        else
                        {
                            result.IsSuccess = true;
                            pushconnection.Close();
                        }


                    }


                }
                catch (Exception ex)
                {
                    //throw ex;
                    pushconnection.Close();
                    result.IsSuccess = false;
                    result.Message = ex.ToString();
                    return result;
                }
                

                
            }
            else
            {
                pushconnection.Close();
                result.IsSuccess = false;
                result.Message = "Inventory Item Id Not Found";

            }

            return result;
        }
        public static string DateString()
        {
            var dateStr = "";
            var month = DateTime.Now.ToString("MMM");

            var day = DateTime.Now.ToString("dd");
            var year = DateTime.Now.ToString("yy");

            dateStr = day + "-" + month + "-" + year;
            return dateStr;
        }

        public Model GetModelDataByModelName(string name)
        {
            Model record;
            using (var dbEntities = new WCTSEntities())
            {
                record = dbEntities.Model.FirstOrDefault(i => i.ModelName == name);
            }
            return record;
            
        }
        private static readonly Random Random = new Random();
        public static string RandomString()
        {
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        internal bool UpdateQcOracleStatus(string p,string uploadedby)
        {
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString);
            con.Open();
            try
            {
                
                var nwcomm = new SqlCommand("Update QcItems SET OracleUploaded=1,OracleUplodedDate=GETDATE(),OracleUploadedBy='" + uploadedby + "' WHERE  OracleTransactionCode='" + p + "'", con);
                nwcomm.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                con.Close();
                return false;
            }
            return true;
        }



        internal bool UpdateQcOracleStatusReProcess(string p, string uploadedby,string model,string lot)
        {
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ADOWCTS"].ConnectionString);
            con.Open();
            try
            {
                var nwcomm = new SqlCommand("Update QcItems SET OracleUploaded=1,OracleUplodedDate=GETDATE(),OracleUploadedBy='" + uploadedby + "' WHERE  OracleTransactionCode='" + p + "' and Model='" + model + "' and LotBarcode='" + lot + "' ", con);
                nwcomm.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                con.Close();
                return false;
            }
            return true;
        }

        internal bool UpdateModelInformation(Model model)
        {
            try
            {
                _entities.Entry(model).State = EntityState.Modified;
                _entities.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }

        }
    }
    }
