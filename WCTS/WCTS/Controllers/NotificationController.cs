﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCMS_MAIN.HelperClass;
using WCTS.Helper_Model;
using WCTS.Models;
using WCTS.Models.EntityModel;
using WCTS.Repository;

namespace WCTS.Controllers
{
    public class NotificationController : Controller
    {
        private readonly Dictionary<int, SessionData> _sessiondictionary = SessionData.GetSessionValues();
        private readonly ProductionRepository _productionRepository = new ProductionRepository();
        private readonly NotificationRepository _notificationRepository = new NotificationRepository();
        readonly Repository.Common _commonRepo = new Repository.Common();
        //
        // GET: /Notification/
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult UpdateNotification(NotificationModel notification)
        {

            Result result = new Result();
            try
            {

                var userId = (long)_sessiondictionary[1].Id;
                var userName = _sessiondictionary[2].Id;
                var roleId = _sessiondictionary[15].Id;

                result.IsSuccess = _notificationRepository.UpdateNotification(new WctsNotification() { Id = notification.Id, UserId = userId, RoleId = roleId }); //_brandManager.UpdateBrandOutletNotification(list);


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateAllNotification()
        {

            Result result = new Result();
            try
            {

                var userId = (long)_sessiondictionary[1].Id;
                var userName = _sessiondictionary[2].Id;
                var roleId = _sessiondictionary[15].Id;

                result.IsSuccess = _notificationRepository.UpdateAllNotification(new WctsNotification() { UserId = userId, RoleId = roleId }); //_brandManager.UpdateBrandOutletNotification(list);


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

	}
}