﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCMS_MAIN.HelperClass;
using WCTS.Helper_Model;
using WCTS.Models;
using WCTS.Models.ApiModels;
using WCTS.Models.EntityModel;
using WCTS.Repository;

namespace WCTS.Controllers
{
    public class ProductionController : Controller
    {
        //
        // GET: /Production/
        readonly Dictionary<int, SessionData> _sessiondictionary = SessionData.GetSessionValues();
        readonly ProductionRepository _productionRepository = new ProductionRepository();
        readonly Repository.Common _common = new Repository.Common();
        private readonly NotificationRepository _notificationRepository = new NotificationRepository();
        readonly MasterBarcodeLogRepository _masterBarcodeRepository = new MasterBarcodeLogRepository();
        public ActionResult Index()
        {

            var userId = (long)_sessiondictionary[1].Id;
            var userName = _sessiondictionary[2].Name;



            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            var employeeId = _sessiondictionary[3].Name;
            var model = _sessiondictionary[4].Name;
            var shiftId = _sessiondictionary[5].Name;
            var shiftName = _sessiondictionary[6].Name;
            var categoryCode = _sessiondictionary[7].Name;
            var lineId = _sessiondictionary[8].Id;
            var inventoryType = _sessiondictionary[13].Name;
            var itemCode = _sessiondictionary[10].Name;
            var lotBarcode = _sessiondictionary[11].Name;
            var lineName = _sessiondictionary[9].Name;
            var productionDate = _sessiondictionary[14].Name;
            ViewBag.Model = model;
            ViewBag.ShiftName = shiftName;
            ViewBag.LineName = lineName;
            ViewBag.CategoryCode = categoryCode;
            ViewBag.CategoryCode = categoryCode;
            ViewBag.InventoryType = inventoryType;
            ViewBag.ProductionDate = Convert.ToDateTime(productionDate).ToString("dd-MM-yyyy");
            var masterBarcode = _masterBarcodeRepository.GetMasterBarcode(userId.ToString(), lotBarcode, itemCode, model);
            if (masterBarcode != null)
            {
                ViewBag.MasterBarcode = masterBarcode.MasterBarcode;
            }

            return View();
        }


        [HttpGet]
        public JsonResult GenrateMasterBarcode()
        {
            var userId = (long)_sessiondictionary[1].Id;
            var userName = _sessiondictionary[2].Name;

            if (userId == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var employeeId = _sessiondictionary[3].Name;
            var model = _sessiondictionary[4].Name;
            var shiftId = _sessiondictionary[5].Name;
            var shiftName = _sessiondictionary[6].Name;
            var categoryCode = _sessiondictionary[7].Name;
            var lineId = _sessiondictionary[8].Id;
            var inventoryType = _sessiondictionary[13].Name;
            var itemCode = _sessiondictionary[10].Name;
            var lotBarcode = _sessiondictionary[11].Name;
            var lineName = _sessiondictionary[9].Name;
            var productionDate = _sessiondictionary[14].Name;
            MasterBarcodeLog masterBarcode = new MasterBarcodeLog();
            masterBarcode.UserId = userId.ToString();
            masterBarcode.ItemCode = itemCode;
            masterBarcode.LotBarcode = lotBarcode;
            masterBarcode.Model = model;
            bool isGenerated = _masterBarcodeRepository.AddMasterBarcode(masterBarcode);
            return Json(isGenerated, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ModelSetup()
        {

            var userId = (long)_sessiondictionary[1].Id;
            var userName = _sessiondictionary[2].Name;
            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }


            return View();
        }

        public ActionResult QcReturnPrint()
        {

            var userId = (long)_sessiondictionary[1].Id;
            var userName = _sessiondictionary[2].Name;
            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }


            return View();
        }


        public ActionResult AdminPassword()
        {

            var userId = (long)_sessiondictionary[1].Id;
            var userName = _sessiondictionary[2].Name;
            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }


            return View();
        }




        [OutputCache(NoStore = true, Location = System.Web.UI.OutputCacheLocation.Client, Duration = 2)]
        public ActionResult GetBrandNotification(string type)
        {

            var userId = (long)_sessiondictionary[1].Id;
            var userName = _sessiondictionary[2].Id;
            var roleId = _sessiondictionary[5].Id;

            object recentnotifications = null;

            return PartialView("_PartialNotification");
        }

        public ActionResult ManualProduction()
        {

            var userId = (long)_sessiondictionary[1].Id;
            var userName = _sessiondictionary[2].Name;
            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }
            var employeeId = _sessiondictionary[3].Name;
            var model = _sessiondictionary[4].Name;
            var shiftId = _sessiondictionary[5].Name;
            var shiftName = _sessiondictionary[6].Name;
            var categoryCode = _sessiondictionary[7].Name;
            var lineId = _sessiondictionary[8].Id;
            var inventoryType = _sessiondictionary[13].Name;
            var itemCode = _sessiondictionary[10].Name;
            var lotBarcode = _sessiondictionary[11].Name;
            var lineName = _sessiondictionary[9].Name;
            var productionDate = _sessiondictionary[14].Name;
            ViewBag.Model = model;
            ViewBag.ShiftName = shiftName;
            ViewBag.LineName = lineName;
            ViewBag.CategoryCode = categoryCode;
            ViewBag.InventoryType = inventoryType;
            ViewBag.ProductionDate = Convert.ToDateTime(productionDate).ToString("dd-MM-yyyy");
            var masterBarcode = _masterBarcodeRepository.GetMasterBarcode(userId.ToString(), lotBarcode, itemCode, model);
            if (masterBarcode != null)
            {
                ViewBag.MasterBarcode = masterBarcode.MasterBarcode;
            }
            return View();
        }

        public ActionResult ProductionSetup()
        {

            var userId = (long)_sessiondictionary[1].Id;
            var userName = _sessiondictionary[2].Name;
            var employeeId = _sessiondictionary[3].Name;
            var model = _sessiondictionary[4].Name;
            var shiftId = _sessiondictionary[5].Name;
            var shiftName = _sessiondictionary[6].Name;
            var categoryCode = _sessiondictionary[7].Name;
            var lineId = _sessiondictionary[8].Id;
            var lineName = _sessiondictionary[9].Name;


            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewBag.Model = model;
                ViewBag.ShiftName = shiftName;
                ViewBag.LineName = lineName;
                ViewBag.CategoryCode = categoryCode;


                return View();
            }

        }


        public JsonResult InsertModelName(Model model)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var empId = _sessiondictionary[3].Name;
            var result = new Result();
            if (userId != 0)
            {
                try
                {


                    var isModelExists = _common.GetModels().FirstOrDefault(a => a.ModelName == model.ModelName);

                    if (isModelExists == null)
                    {
                        model.FGItemCode = model.FGItemCode;
                        model.SFGItemCode = model.SFGItemCode;
                        model.AddedBy = empId;
                        model.AddedDate = DateTime.Now;
                        result.IsSuccess = _productionRepository.InsertModelName(model);
                    }

                    else
                    {
                        result.IsSuccess = false;
                        result.Message = "Model Already Added ! Please Update Information If Nedded";

                    }


                    return Json(result);
                }
                catch (Exception ex)
                {
                    throw ex;

                }
            }
            else
            {
                return Json(new Result { Id = "0" });
            }
        }

        public JsonResult UpdateModelInformation(Model model)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var empId = _sessiondictionary[3].Name;
            var result = new Result();
            if (userId != 0)
            {
                try
                {

                    var isExist = _common.GetModels().FirstOrDefault(a => a.ModelName == model.ModelName);

                    if (isExist == null)
                    {
                        result.IsSuccess = false;
                        result.Message = "Model Not Found";
                    }

                    else
                    {
                        isExist.FGItemCode = model.FGItemCode;
                        isExist.SFGItemCode = model.SFGItemCode;
                        isExist.UpdatedBy = empId;
                        isExist.UpdatedDate = DateTime.Now;
                        result.IsSuccess = _productionRepository.UpdateModelInformation(isExist);

                        if (result.IsSuccess)
                        {
                            result.Message = "Model Information Updated Successfully";
                        }
                        else
                        {
                            result.Message = "ErrorOccured while Updating Model Information";
                        }
                    }




                    return Json(result);
                }
                catch (Exception ex)
                {
                    result.IsSuccess = false;
                    result.Message = ex.ToString();
                    return Json(result);

                }
            }
            else
            {
                return Json(new Result { Id = "0" });
            }
        }




        public JsonResult GetEmployeeInfoById(Login login)
        {

            var userId = (long)_sessiondictionary[1].Id;

            if (userId != 0)
            {
                var loginInfo = new LoginModel();
                if (!String.IsNullOrEmpty(login.EmployeeId))
                {



                    var loginData = _common.GetEmployeeInfoById(login.EmployeeId);


                    if (loginData != null)
                    {

                        loginInfo.EmployeeId = loginData.EmployeeId;
                        loginInfo.OldPassword = loginData.Password;

                    }



                }
                return Json(loginInfo, JsonRequestBehavior.AllowGet);
                // return Json(loginInfo);
            }

            else
            {
                return Json(new Result { Id = "0" });
            }

        }



        public JsonResult ChangePasswordDetails(LoginModel login)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var result = new Result();
            if (userId != 0)
            {
                try
                {

                    var isExist = _common.GetLoginInfo(new Login() { UserId = login.EmployeeId.ToLower(), Password = login.OldPassword });

                    if (isExist == null)
                    {
                        result.IsSuccess = false;
                        result.Message = "Employee Id Or Password Not Matched";
                    }
                    else
                    {
                        isExist.Password = login.NewPassword;
                        result.IsSuccess = _common.UpdatePassword(isExist);

                        if (result.IsSuccess)
                        {
                            result.Message = "Password Updated Successfully";
                        }
                        else
                        {
                            result.Message = "ErrorOccured while Updating Password";
                        }

                    }
                    return Json(result);
                }
                catch (Exception ex)
                {
                    throw ex;

                }
            }
            else
            {
                return Json(new Result { Id = "0" });
            }
        }


        public JsonResult InsertProductionSetup(ProductionSetup productionSetup)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var userName = _sessiondictionary[2].Name;
            var employeeId = _sessiondictionary[3].Name;
            var lineId = _sessiondictionary[8].Id;
            var result = new Result();
            var result1 = new Result();
            if (userId != 0)
            {

                try
                {
                    var isProjectActive = _productionRepository.GetActiveProjectDetails(new ProductionSetup() { LineId = productionSetup.LineId, IsActive = true });

                    if (isProjectActive != null)
                    {
                        isProjectActive.IsActive = false;
                        result.IsSuccess = _productionRepository.UpdateProductionSetup(isProjectActive);
                    }
                    productionSetup.IsActive = true;
                    productionSetup.ItemCode = GenerateItemCode(productionSetup.ModelName, productionSetup.CategoryCode, productionSetup.ShiftId);
                    productionSetup.LotNumber = GenerateLotNumber(productionSetup.ShiftId, productionSetup.CategoryCode, productionSetup.SerialNumber, productionSetup.ProductionDate);
                    productionSetup.AddedBy = userId;
                    productionSetup.ProductionDate = productionSetup.ProductionDate;
                    productionSetup.AddedDate = DateTime.Now;
                    result.IsSuccess = _productionRepository.InsertProductionSetup(productionSetup);
                    if (result.IsSuccess)
                    {
                        var activeProject = _common.GetProductionSetup(new ProductionSetup() { LineId = productionSetup.LineId, IsActive = true });
                        System.Web.HttpContext.Current.Session["WCTSModel"] = activeProject.ModelName;
                        System.Web.HttpContext.Current.Session["WCTSShiftId"] = activeProject.ShiftId;
                        System.Web.HttpContext.Current.Session["WCTSShiftName"] = activeProject.ShiftName;
                        System.Web.HttpContext.Current.Session["WCTSCategoryCode"] = activeProject.CategoryCode;
                        System.Web.HttpContext.Current.Session["WCTSLineId"] = activeProject.LineId;
                        System.Web.HttpContext.Current.Session["WCTSLineName"] = activeProject.LineName;
                        System.Web.HttpContext.Current.Session["WCTSItemCode"] = activeProject.ItemCode;
                        System.Web.HttpContext.Current.Session["WCTSLotNumber"] = activeProject.LotNumber;
                        System.Web.HttpContext.Current.Session["WCTSProjectSetupId"] = activeProject.Id;
                        System.Web.HttpContext.Current.Session["WCTSInventoryType"] = activeProject.InventoryType;

                        System.Web.HttpContext.Current.Session["WCTSProductionDate"] = productionSetup.ProductionDate;
                    }


                    if (result.IsSuccess)
                    {
                        var a = new WctsNotification()
                        {

                            Title = "New Project Was Setup",
                            Description = "New Project Setup Complete ANd Production Started Model= " + System.Web.HttpContext.Current.Session["WCTSModel"] + " Shift " + System.Web.HttpContext.Current.Session["WCTSShiftId"] + " Category Code=" + System.Web.HttpContext.Current.Session["WCTSCategoryCode"] + " Inverntoy Type = " + System.Web.HttpContext.Current.Session["WCTSInventoryType"] + " By user",
                            TargetController = "Production",
                            TargetAction = "Index",
                            Seen = false,
                            AddedBy = userId,
                            AddedDate = DateTime.Now
                        };
                        result1.IsSuccess = _notificationRepository.InsertNotificationInfo(a);

                    }

                    return Json(result);
                }

                catch (Exception ex)
                {
                    throw ex;

                }

            }

            else
            {
                return Json(new Result { Id = "0" });
            }
        }


        public JsonResult UpdatePrintedBarcodes(List<Result> printedList, string masterBarcode)
        {

            var result = new Result();
            result.IsSuccess = _productionRepository.UpdatePrintedBarcodes(printedList, masterBarcode);
            return Json(result);


        }

        [HttpGet]
        public JsonResult PrintedMasterBarcode(string masterBarcode)
        {
            var master = _masterBarcodeRepository.GetMasterBarcode(masterBarcode);
            if (master == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            master.IsPrinted = true;
            bool isPrinted = _masterBarcodeRepository.UpdateMasterBarCode(master);
            return Json(isPrinted, JsonRequestBehavior.AllowGet);

        }

        public JsonResult InsertProductionMasterManualProcess(string barcode, string masterBarcode = "")
        {
            var result = new Result();
            var userId = (long)_sessiondictionary[1].Id;
            var shiftId = _sessiondictionary[5].Name;
            var categoryCode = _sessiondictionary[7].Name;
            var model = _sessiondictionary[4].Name;
            var itemCode = _sessiondictionary[10].Name;
            var lotNumber = _sessiondictionary[11].Name;
            var projectSetupId = _sessiondictionary[12].Id;
            var lineId = _sessiondictionary[8].Id;
            var productionDate = _sessiondictionary[14].Name;
            if (userId != 0 || categoryCode != null || shiftId != null)
            {

                try
                {
                    var pmaster = new ProductionMaster
                    {
                        Barcode = barcode.Trim().ToUpper(),
                        ShiftId = shiftId,
                        MasterBarcode = masterBarcode,
                        CategoryCode = categoryCode,
                        Model = model,
                        ItemCode = itemCode,
                        LotBarcode = lotNumber,
                        IsPrinted = true,
                        ProjectSetupId = projectSetupId,
                        AddedBy = userId,

                        AddedDate = DateTime.Now,
                        ProductionDate = Convert.ToDateTime(productionDate),
                    };


                    var isExists = _productionRepository.GetBarCodeInfo(barcode);

                    //if(!string.IsNullOrEmpty(masterBarcode))
                    //{
                    //    int total = _productionRepository.GetTotalBarcode(masterBarcode);
                    //    if(total>=90)
                    //    {
                    //        result.Success = false;
                    //        result.Message = "The limit has been exceeded.";
                    //        return Json(result);
                    //    }
                    //}


                    if (isExists == null)
                    {

                        result.IsSuccess = _productionRepository.InsertProductionMaster(pmaster);
                        var itemCount = _productionRepository.BrcodeItemCount(lineId, model, projectSetupId);

                        result.Success = true;
                        result.BatchItems = itemCount;
                        result.LotNo = itemCount[0].LotBarcode;
                        result.MasterBatch = itemCount[0].MasterBarcode;

                    }

                    else
                    {
                        result.Success = false;
                        result.Message = "Duplicate Barcode";
                    }



                }
                catch (Exception ex)
                {

                    result.Message = ex.ToString();
                }

                return Json(result);

            }

            else
            {
                return Json(new Result { Id = "0" });
            }

        }

        public JsonResult InsertProductionMaster(string barcode, string masterBarcode = "")
        {
            var result = new Result();
            var userId = (long)_sessiondictionary[1].Id;
            var shiftId = _sessiondictionary[5].Name;
            var categoryCode = _sessiondictionary[7].Name;
            var model = _sessiondictionary[4].Name;
            var itemCode = _sessiondictionary[10].Name;
            var lotNumber = _sessiondictionary[11].Name;
            var projectSetupId = _sessiondictionary[12].Id;
            //var shiftName = _sessiondictionary[6].Name;
            var lineId = _sessiondictionary[8].Id;
            //var lineName = _sessiondictionary[9].Name;
            //var userName = _sessiondictionary[2].Name;
            //var employeeId = _sessiondictionary[3].Name;

            var productionDate = _sessiondictionary[14].Name;


            if (userId != 0 || categoryCode != null || shiftId != null)
            {


                try
                {
                    var pmaster = new ProductionMaster
                    {
                        Barcode = barcode.Trim(),
                        ShiftId = shiftId,
                        MasterBarcode = masterBarcode.Trim(),
                        CategoryCode = categoryCode,
                        Model = model,
                        ItemCode = itemCode,
                        LotBarcode = lotNumber,
                        ProjectSetupId = projectSetupId,
                        ProductionDate = Convert.ToDateTime(productionDate),
                        AddedBy = userId,
                        AddedDate = DateTime.Now
                    };



                    var isExists = _productionRepository.GetBarCodeInfo(barcode);

                    if (!string.IsNullOrEmpty(masterBarcode))
                    {
                        int total = _productionRepository.GetTotalBarcode(masterBarcode);
                        if (total >= 90)
                        {
                            WCTSEntities _entity = new WCTSEntities();
                            var masterLog = _entity.MasterBarcodeLog.FirstOrDefault(c => c.MasterBarcode == masterBarcode);
                            if (masterLog != null)
                            {
                                masterLog.IsPrinted= true;
                                _entity.SaveChanges();
                            }
                            result.Success = false;
                            result.Message = "The limit has been exceeded.";
                            return Json(result);
                        }
                    }

                    if (isExists == null)
                    {

                        result.IsSuccess = _productionRepository.InsertProductionMaster(pmaster);
                        var itemCount = _productionRepository.BrcodeItemCount(lineId, model, projectSetupId);

                        result.Success = true;
                        result.BatchItems = itemCount;
                        result.LotNo = itemCount[0].LotBarcode;
                        result.MasterBatch = itemCount[0].MasterBarcode;

                    }

                    else
                    {
                        result.Success = false;
                        result.Message = "Duplicate Barcode";
                    }



                }
                catch (Exception ex)
                {

                    result.Message = ex.ToString();
                }

                return Json(result);

            }

            else
            {
                return Json(new Result { Id = "0" });
            }

        }

        public JsonResult GetDataForManualPrint()
        {
            var result = new Result();
            var userId = (long)_sessiondictionary[1].Id;
            var shiftId = _sessiondictionary[5].Name;
            var categoryCode = _sessiondictionary[7].Name;
            var model = _sessiondictionary[4].Name;
            var itemCode = _sessiondictionary[10].Name;
            var lotNumber = _sessiondictionary[11].Name;
            var projectSetupId = _sessiondictionary[12].Id;
            var lineId = _sessiondictionary[8].Id;


            if (userId != 0 || projectSetupId != 0)
            {

                try
                {
                    result.IsSuccess = true;
                    // var itemCount = _productionRepository.BrcodeItemCount(lineId, model, projectSetupId);

                    var itemCount = _productionRepository.BrcodeItemCountManual(lineId, model, projectSetupId);
                    result.BatchItems = itemCount;

                    if (itemCount.Count > 0)
                    {
                        result.LotNo = itemCount[0].LotBarcode;
                        result.MasterBatch = itemCount[0].MasterBarcode;
                    }

                    return Json(result);




                }
                catch (Exception)
                {

                    throw;
                }


            }


            else
            {
                return Json(new Result { Id = "0" });
            }

        }

        public JsonResult GetMasterBarcodeData(string masterBarcode)
        {
            var result = new Result();
            var userId = (long)_sessiondictionary[1].Id;

            if (userId != 0)
            {

                try
                {
                    result.IsSuccess = true;
                    var itemCount = _productionRepository.GetMasterBarcodeData(masterBarcode);
                    result.BatchItems = itemCount;
                    if (itemCount.Count > 0)
                    {
                        result.LotNo = itemCount[0].LotBarcode;
                        result.MasterBatch = itemCount[0].MasterBarcode;
                    }
                    return Json(result);
                }
                catch (Exception)
                {

                    throw;
                }


            }


            else
            {
                return Json(new Result { Id = "0" });
            }

        }


        public JsonResult GetMasterBarcodeDataBybarcode(string masterBarcode)
        {
            var result = new Result();
            var userId = (long)_sessiondictionary[1].Id;

            if (userId != 0)
            {

                try
                {
                    result.IsSuccess = true;
                    var barcode = _productionRepository.GetBarCodeInfo(masterBarcode).MasterBarcode;
                    if (barcode != "")
                    {
                        var itemCount = _productionRepository.GetMasterBarcodeData(barcode);
                        result.BatchItems = itemCount;
                        if (itemCount.Count > 0)
                        {

                            result.LotNo = itemCount[0].LotBarcode;
                            result.MasterBatch = itemCount[0].MasterBarcode;
                        }

                    }

                    return Json(result);
                }
                catch (Exception exception)
                {

                    throw exception;
                }


            }


            else
            {
                return Json(new Result { Id = "0" });
            }

        }



        public JsonResult GetItemsCounts(string barcode)
        {
            var result = new Result();
            var userId = (long)_sessiondictionary[1].Id;
            var shiftId = _sessiondictionary[5].Name;
            var categoryCode = _sessiondictionary[7].Name;
            var model = _sessiondictionary[4].Name;
            var itemCode = _sessiondictionary[10].Name;
            var lotNumber = _sessiondictionary[11].Name;
            var projectSetupId = _sessiondictionary[12].Id;
            //var shiftName = _sessiondictionary[6].Name;
            var lineId = _sessiondictionary[8].Id;
            var productionDate = _sessiondictionary[14].Name;


            //var totalCount = _productionRepository.BrcodeItemTotalCount(projectSetupId);

            var totalCount = _productionRepository.BrcodeItemTotalCount(productionDate, shiftId);

            var itemCount = _productionRepository.BrcodeItemCount(lineId, model, projectSetupId);
            var topThree = totalCount.OrderByDescending(a => a.AddedDate).Take(3).ToList();

            var aa = new List<string>();

            var colors = new { TotalCount = totalCount.Count, readyPrint = itemCount.Count(), topThreeRecord = topThree }.ToString();

            aa.Add(colors);

            var objstate = totalCount.Select(x => new { TotalCount = totalCount.Count, readyPrint = itemCount.Count(), topThreeRecord = topThree }).ToList();
            return Json(objstate.Take(1), JsonRequestBehavior.AllowGet);
        }


        //        DateTime d1 = DateTime.Now;
        //d1 = d1.AddDays(-1);

        private static string GenerateLotNumber(string shiftId, string categoryCode, string serial, DateTime? date)
        {
            //var sDate = DateTime.Now.ToString();
            var sDate = date.ToString();
            //DateTime startDate = Convert.ToDateTime(date);


            var datevalue = (Convert.ToDateTime(sDate.ToString()));
            var dy = datevalue.Day.ToString();




            //var nDate= DateTime.Now;
            //var timeFormat = nDate.ToString("tt");
            //var hour = nDate.Hour;


            //if (timeFormat.ToLower() == "am" && (hour >= 1 && hour <8) )

            //{
            //     var d1 = DateTime.Now;
            //     d1 = d1.AddDays(-1);

            //     var sChangedDate = d1.ToString();
            //     var datevalueChanged = (Convert.ToDateTime(sChangedDate.ToString()));
            //     dy = datevalueChanged.Day.ToString();

            //}





            if (dy.Length == 1)
            {
                dy = "0" + dy;
            }
            var mn = datevalue.Month.ToString();

            if (mn.Length == 1)
            {
                mn = "0" + mn;
            }
            var yy = datevalue.Year.ToString().Substring(datevalue.Year.ToString().Length - 2); ;
            return dy + mn + yy + shiftId + categoryCode + serial;
        }


        private static string GenerateItemCode(string model, string category, string shift)
        {
            model = model.Substring(0, 4);
            return "WC-" + model + category + shift;
        }


    }
}