﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCMS_MAIN.HelperClass;
using WCTS.Models;
using WCTS.Models.EntityModel;
using WCTS.Repository;

namespace WCTS.Controllers
{
    public class CommonController : Controller
    {
        private readonly Dictionary<int, SessionData> _sessiondictionary = SessionData.GetSessionValues();
        private readonly ProductionRepository _productionRepository = new ProductionRepository();
        private readonly NotificationRepository _notificationRepository = new NotificationRepository();
        readonly Repository.Common _commonRepo = new Repository.Common();
        //
        // GET: /Common/
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult GetShifts()
        {
            var eList = new List<Shift>();
            try
            {
                eList = _commonRepo.GetShifts().ToList();

            }
            catch (Exception ex)
            {
                throw;
            }
            var objstate = eList.Select(x => new { value = x.ShiftName , Id = x.ShiftId }).ToList();
            return Json(objstate.ToArray(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetModelsFromOracle()
        {
            var eList = new List<OracleModel>();
            try
            {
                eList = _commonRepo.GeModelDataFromOracle().ToList();

            }
            catch (Exception ex)
            {
                var err = eList.Select(x => new { value = ex, Id = "" }).ToList();
                return Json(err.ToArray(), JsonRequestBehavior.AllowGet);
            }
            var objstate = eList.Select(x => new { value = x.Model, Id = x.Model }).ToList();
            return Json(objstate.ToArray(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetSfgcodeFromOracle()
        {
            var eList = new List<OracleModel>();
            try
            {
                eList = _commonRepo.GeModelDataFromOracle().ToList();

            }
            catch (Exception ex)
            {
                var err = eList.Select(x => new { value = ex, Id = "" }).ToList();
                return Json(err.ToArray(), JsonRequestBehavior.AllowGet);
            }
            var objstate = eList.Select(x => new { value = x.ItemCode, Id = x.ItemCode }).ToList();
            return Json(objstate.ToArray(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFgcodeFromOracle()
        {
            var eList = new List<OracleModel>();
            try
            {
                eList = _commonRepo.GeModelDataFromOracle().ToList();

            }
            catch (Exception ex)
            {
                var err = eList.Select(x => new { value = ex, Id = "" }).ToList();
                return Json(err.ToArray(), JsonRequestBehavior.AllowGet);
            }
            var objstate = eList.Select(x => new { value = x.ItemCode, Id = x.ItemCode }).ToList();
            return Json(objstate.ToArray(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetQcTrackingLotInfo(string lotNumber)
        {
            var data = new QcLotTracking();
            try
            {
              var  dataFetch = _commonRepo.GetQcTrackingLotInfo(lotNumber);
                if (dataFetch == null)
                {
                    data.LotNumber = "null";
                }
                else
                {
                    data = dataFetch;
                }

            }
            catch (Exception ex)
            {
                throw;
            }
           // var objstate = eList.Select(x => new { value = x.ShiftName , Id = x.ShiftId }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetShiftsDetails()
        {
            var eList = new List<Shift>();
            try
            {
                eList = _commonRepo.GetShifts().ToList();

            }
            catch (Exception ex)
            {
                throw;
            }

            return Json(eList.ToArray(), JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetLines()
        {
            List<Line> eList;
            try
            {
                eList = _commonRepo.GetLines().ToList();

            }
            catch (Exception ex)
            {
                throw;
            }
            var objstate = eList.Select(x => new { value = x.LineName, Id = x.LineId }).ToList();
            return Json(objstate.ToArray(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetCategories()
        {
            List<Category> eList;
            try
            {
                eList = _commonRepo.GetCategories().ToList();

            }
            catch (Exception ex)
            {
                throw;
            }
            var objstate = eList.Select(x => new { value = x.CategoryName, Id = x.CategoryCode }).ToList();
            return Json(objstate.ToArray(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetModels()
        {
            List<Model> eList;
            try
            {
                eList = _commonRepo.GetModels().ToList();

            }
            catch (Exception ex)
            {
                throw;
            }
            var objstate = eList.Select(x => new { value = x.ModelName, Id = x.Id }).ToList();
            return Json(objstate.ToArray(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetModelInformationByName(string model)
        {
            Model modelobj = null;
            try
            {
                modelobj = _commonRepo.GetModels().FirstOrDefault(a => a.ModelName == model);

            }
            catch (Exception ex)
            {
                return Json(modelobj, JsonRequestBehavior.AllowGet);
            }

            return Json(modelobj, JsonRequestBehavior.AllowGet);
        }





        public JsonResult GetInventoryTypes()
        {
            List<InventoryType> eList;
            try
            {
                eList = _commonRepo.GetInventoryTypes().ToList();

            }
            catch (Exception ex)
            {
                throw;
            }
            var objstate = eList.Select(x => new { value = x.InventoryType1, Id = x.Id }).ToList();
            return Json(objstate.ToArray(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllLotNumbers()
        {
            List<ProductionModel> eList;
            try
            {
                eList = _commonRepo.GetAllLotNumbers().GroupBy(a => new {a.LotNo}).Select(b => new ProductionModel()
                {
                    LotNo = b.Key.LotNo

                }).ToList();

            }
            catch (Exception ex)
            {
                throw;
            }
            var objstate = eList.Select(x => new { value = x.LotNo, Id = x.LotNo }).ToList();
            return Json(objstate.ToArray(), JsonRequestBehavior.AllowGet);
        }



      //  [OutputCache(NoStore = true, Location = System.Web.UI.OutputCacheLocation.Client, Duration = 2)]
        //public ActionResult GetWctsNotification(string type)
        //{
        //    var userId = (long)_sessiondictionary[1].Id;
            
        //    var roleId = _sessiondictionary[15].Id;

        //    object recentnotifications = null;
        //    // var recentNotifications = _brandManager.GetRecentActivites();
        //    var recentNotifications = _notificationRepository.GetWctsNotification(new NotificationModel() { UseId = userId, RoleId = roleId });

        //    if (type == "S")
        //    {
        //        recentnotifications = recentNotifications.Take(30).ToList();

        //    }
        //    if (type == "A")
        //    {
        //        recentnotifications = recentNotifications.Take(50).ToList();

        //    }
        //    ViewBag.recentNotifications = recentnotifications;
        //    ViewBag.TotalNotifications = recentNotifications.Count;


        //    return PartialView("_PartialNotification");
        //}



	}
}