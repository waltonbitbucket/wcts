﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCMS_MAIN.HelperClass;
using WCTS.Repository;

namespace WCTS.Controllers
{
    public class HomeController : Controller
    {
        
readonly Dictionary<int, SessionData> _sessiondictionary = SessionData.GetSessionValues();
        readonly ProductionRepository _productionRepository = new ProductionRepository();
        readonly Repository.Common _common = new Repository.Common();
        public ActionResult Index()
        {
            var userId = (long)_sessiondictionary[1].Id;
            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            else
            {
                var model = _sessiondictionary[4].Name;
                var shiftName = _sessiondictionary[6].Name;
                var categoryCode = _sessiondictionary[7].Name;
                var inventoryType = _sessiondictionary[13].Name;
                var lineName = _sessiondictionary[9].Name;
                ViewBag.Model = model;
                ViewBag.ShiftName = shiftName;
                ViewBag.LineName = lineName;
                ViewBag.CategoryCode = categoryCode;

                ViewBag.InventoryType = inventoryType;
                return View();
            }
         
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult SessionDummy()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [HttpPost]
        public JsonResult KeepSessionAlive()
        {
            return new JsonResult { Data = "Success" };
        }

    }
}