﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCMS_MAIN.HelperClass;
using WCTS.Helper_Model;
using WCTS.Models.EntityModel;
using WCTS.Repository;

namespace WCTS.Controllers
{
    public class UpdatePanelController : Controller
    {
        private readonly Dictionary<int, SessionData> _sessiondictionary = SessionData.GetSessionValues();
        private readonly ProductionRepository _productionRepository = new ProductionRepository();
        private readonly ReportRepository _reportRepository = new ReportRepository();
        private readonly UpdatePanelRepository _updatePanelRepository = new UpdatePanelRepository();
        private readonly Repository.Common _common = new Repository.Common();
        public ActionResult Update()
        {
            var userId = (long)_sessiondictionary[1].Id;
          
            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();
        }

        #region Gomu Gomu testo Codeeeeeeeee
        //Gomu Gomu testo Codeeeeeeeee//

        public ActionResult CheckSerial()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();
        }

        public JsonResult CheckSerialCode(string barCode)
        { 
            var result = new Result();

             var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {
                result.Id = "0";
                return Json(result);
            }

            else
            {    
            var codedata = _updatePanelRepository.CheckSerialCode(barCode);

                if (codedata != null)
                {
                    result.Id = "1";
                    result.Message = "Serial Found"; 
                }

                else
                {
                    result.Id = "2";
                    result.Message = "Serial Not Found Found"; 
                }

            return Json(result, JsonRequestBehavior.AllowGet);

                
            }

       
        }

        //Gomu Gomu testo Codeeeeeeeee//
        #endregion Gomu Gomu testo Codeeeeeeeee

        public JsonResult GetBarcodeInforForUpdate(string barCode)
        {
            var codedata = _reportRepository.GetBarcodeDetailsReport(barCode);
           
            return Json(codedata.Cast<object>().ToArray(), JsonRequestBehavior.AllowGet);

        }


        public JsonResult ModifyCompressorBarcodeinfo(ProductionMaster productionMaster, string clickType)
        {

            var userId = (long)_sessiondictionary[1].Id;
            var result = new Result();
            if (userId != 0)
            {
                try
                {
                    if (clickType == "Update")

                    {
                        productionMaster.UpdatedBy = userId;
                        result.IsSuccess = _updatePanelRepository.UpdateProductionMaster(productionMaster);

                    }
                    if (clickType == "Delete")
                    {
                        result.IsSuccess = _updatePanelRepository.DeleteProductionMaster(productionMaster);

                    }

                   
                    return Json(result, JsonRequestBehavior.AllowGet);

                }
                catch (Exception exception)
                {
                    result.IsSuccess = false;
                    result.Message = exception.ToString();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }
            return Json(new Result { Id = "0" });
        }




	}
}