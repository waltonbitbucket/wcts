﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using BR_BLL;
using WCMS_MAIN.HelperClass;
using WCTS.Helper_Model;
using WCTS.Models;
using WCTS.Models.EntityModel;
using WCTS.Repository;

namespace WCTS.Controllers
{
    public class InventoryController : Controller
    {
        //
        // GET: /Inventory/

        readonly Dictionary<int, SessionData> _sessiondictionary = SessionData.GetSessionValues();
        readonly ProductionRepository _productionRepository = new ProductionRepository();
        readonly InventoryRepository _inventoryRepository = new InventoryRepository();
        readonly Repository.Common _common = new Repository.Common();
        public ActionResult Inventory()
        {
            return View();
        }


        public ActionResult InventoryReady()
        {
            return View();
        }

        public JsonResult GetMasterBarcodeData(string masterBarCode)
        {
            var userId = (long) _sessiondictionary[1].Id;
            var userName = _sessiondictionary[2].Name;
            var employeeId = _sessiondictionary[3].Name;
            var lineId = _sessiondictionary[8].Id;

            var result = new Result();
            var isExists = _inventoryRepository.GetInventoryInfo(new Inventory { MasterBarCode = masterBarCode }).Any();


            if (isExists)
            {

                return Json(new { data = result, status = "0",message="Already Received" }, JsonRequestBehavior.AllowGet);

            }


            else
            {
                var masterBarcodeData = _inventoryRepository.GetMasterBarcodeData(masterBarCode);
                return Json(new { data = masterBarcodeData, status = "1", message = "Received Successfully" }, JsonRequestBehavior.AllowGet);
            }
        }



        public JsonResult GetPassedItemFromInventoryWD()
        {

            var list = _inventoryRepository.GetPassedItemFromInventoryWD().ToList();

            var overAlllist = new List<Models.ProductionModel>();

            foreach (var items in list)
            {
                var timeSpan = DateTime.Now.Minute;


                if (timeSpan < Convert.ToInt32(items.Min))
                {
                    timeSpan = timeSpan + Convert.ToInt32(items.Min);

                }
                var diff = timeSpan - Convert.ToInt32(items.Min);
                var overAlll = new Models.ProductionModel();
                overAlll.MasterBarCode = items.MasterBarCode;

                overAlll.Total = items.Total;

                if (diff == -1 || diff == 0)
                {

                    overAlll.RemainTime = 20 - 1;
                }

                if (diff < 0)
                {

                    overAlll.RemainTime = Math.Abs(diff);

                }

                else
                {
                    overAlll.RemainTime = 20 - diff;

                }


                if (overAlll.RemainTime < 0)
                {
                    overAlll.RemainTime = Math.Abs(overAlll.RemainTime);

                }

                // overAlll.Total = items.Total;

                overAlllist.Add(overAlll);
            }




            return Json(new { data = overAlllist }, JsonRequestBehavior.AllowGet);


        }

        public JsonResult GetPassedItemFromInventory(string fromdate, string todate)
        {

            var list = _inventoryRepository.GetPassedItemFromInventory(fromdate, todate).ToList();

            var overAlllist = new List<Models.ProductionModel>();

            foreach (var items in list)
            {
                var timeSpan = DateTime.Now.Minute;
                var diff = timeSpan - Convert.ToInt32(items.Min);
                var overAlll = new Models.ProductionModel();
                overAlll.MasterBarCode = items.MasterBarCode;

                overAlll.Total = items.Total;

                if (diff == -1 || diff == 0)
                {

                    overAlll.RemainTime = 20-1;
                }

                if (diff < 0)
                {

                    overAlll.RemainTime = Math.Abs(diff);

                }

                else
                {
                    overAlll.RemainTime = 20-diff;
             
                }


                if (overAlll.RemainTime < 0)
                {
                    overAlll.RemainTime = Math.Abs(overAlll.RemainTime);

                }
               
                // overAlll.Total = items.Total;

                overAlllist.Add(overAlll);
            }




            return Json(new { data = overAlllist }, JsonRequestBehavior.AllowGet);


        }

        public JsonResult InventoryReceived(List<ProductionModel> inventoryList)         
        {

            var userId = (long)_sessiondictionary[1].Id;
            var result = new Result();

            if (userId != 0)
            {
                try
                {
                    result.IsSuccess = _inventoryRepository.InventoryReceived(inventoryList,userId);

                }
                catch (Exception)
                {
                    result.Message = "error occured while saving the information";
                    return Json(result);
                }


                return Json(result);
            }

            else
            {
                return Json(new Result { Id = "0" });

            }




           // return Json(checkedResult);

        }

       [HttpPost]
       [AllowAnonymous]
        public JsonResult InventoryPassedItemsMove()
        {
            var passedInventoryInsertresult = new Result();
            var passedItemUpdateresult = new Result();
            var list = new List<PassedInventory>();
           
                try
                {
                    var passedData= _inventoryRepository.GetPassedItemToPrepareForInventory().ToList();
                    if (passedData.Count > 0)
                    {
                        foreach (var data in passedData)
                    {
                        var isExistsinPassedInventory = _inventoryRepository.IsExistsInPassedInventory(data.Barcode.Trim());
                        if (!isExistsinPassedInventory)
                        {
                            var passedInventoryModelData=new PassedInventory();
                            passedInventoryModelData.ProjectSetupId = data.ProjectSetupId;
                            passedInventoryModelData.Model = data.Model;
                            passedInventoryModelData.ShiftId = data.ShiftId;
                            passedInventoryModelData.CategoryCode = data.CategoryCode; 
                            passedInventoryModelData.ItemCode = data.ItemCode;
                            passedInventoryModelData.Barcode = data.Barcode;
                            passedInventoryModelData.MasterBarcode = data.MasterBarcode;
                            passedInventoryModelData.ProductionDate = data.ProductionDate;
                            passedInventoryModelData.IsHold = false;
                            passedInventoryModelData.IsMovedToFinalInventory = false;
                            passedInventoryModelData.PassedDate = data.PassedDate;
                            passedInventoryModelData.ScannedBy = data.AddedBy;
                            passedInventoryModelData.ScannedDate = data.AddedDate;
                            passedInventoryModelData.AddedBy = 9911;
                            passedInventoryModelData.AddedDate = DateTime.Now;
                            list.Add(passedInventoryModelData);
                        }
                    }


                    passedInventoryInsertresult.IsSuccess = _inventoryRepository.InventoryPassedItemsInsert(list);

                    if (passedInventoryInsertresult.IsSuccess)
                    {

                        passedItemUpdateresult.IsSuccess=_inventoryRepository.UpdateProductionMasterMovedItems(list);


                    }

                    else
                    {
                        passedInventoryInsertresult.Message = "error occured while saving the information";
                   

                    }

                    }


                }
                catch (Exception)
                {
                    passedItemUpdateresult.Message = "error occured while saving the information";
                    return Json(passedItemUpdateresult);
                }


                return Json(passedItemUpdateresult);
            }

            // return Json(checkedResult);



       public JsonResult HoldPassedInventoryItems(PassedInventoryHold passedInventoryHold)
       {

           var userId = (long)_sessiondictionary[1].Id;
           var result = new Result();

           if (userId != 0)
           {
               try
               {
                   result.IsSuccess = _inventoryRepository.HoldPassedInventoryItems(passedInventoryHold, userId);

               }
               catch (Exception)
               {
                   result.Message = "error occured while saving the information";
                   return Json(result);
               }


               return Json(result);
           }

           else
           {
               return Json(new Result { Id = "0" });

           }
           // return Json(checkedResult);

       }

        }

    }
