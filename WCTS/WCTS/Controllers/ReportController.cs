﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Antlr.Runtime;
using Newtonsoft.Json;
using WCMS_MAIN.HelperClass;
using WCTS.Models;
using WCTS.Models.ApiModels;
using WCTS.Models.EntityModel;
using WCTS.Repository;
using ProductionModel = WCTS.Models.ProductionModel;

namespace WCTS.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/
        private readonly Dictionary<int, SessionData> _sessiondictionary = SessionData.GetSessionValues();
        private readonly ProductionRepository _productionRepository = new ProductionRepository();
        private readonly ReportRepository _reportRepository = new ReportRepository();
        private readonly Repository.Common _common = new Repository.Common();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DailyProduction()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();
        }

        public ActionResult BarCodeTracking()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();
        }

        public ActionResult FinalInventory()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();
        }

        public ActionResult LotTracking()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();
        }


        public ActionResult MasterBarcodeTracking()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();
        }

        public ActionResult OracleUploadReport()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();
        }




        public ActionResult HoldReport()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();
        }


        public ActionResult RejectedReport()
        {

            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();

           
        }
        public ActionResult QcLotTrackingReport()
        {

            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();


        }



        public ActionResult AllProduction()
        {

            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();


        }

        public ActionResult ManualPrintLog()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();
        }


        public ActionResult MultiBarcodeReport()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();
        }

        [HttpPost]
        public JsonResult GetProductionMultiBarcodeData(string strdata)
        {
            var modelData = JsonConvert.DeserializeObject<List<ProductionModel>>(strdata);
            var data = _productionRepository.GetProductionMultiBarcodeData(modelData);
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }




        public JsonResult GetProductionLine(string fromdate, string todate)
        {

            var list = _reportRepository.GetProductionLineReport(fromdate, todate).ToList();

              var overAlllist = new List<Models.ProductionModel>();

            foreach (var items in list)
            {
                var overAlll = new Models.ProductionModel();

                overAlll.DateTime = items.Date.ToString("yyyy-MM-dd");
                overAlll.Model = items.Model;
                overAlll.ShiftName = items.ShiftName;
                overAlll.CategoryName = items.CategoryName;
                overAlll.Status = "Passed";
                overAlll.Total = items.Total;

                overAlllist.Add(overAlll);
            }

            return Json(new { data = overAlllist }, JsonRequestBehavior.AllowGet);


        }
        

        public JsonResult GetCurrentlyHoldReport()
        {

            var list = _reportRepository.GetCurrentlyHoldReport();
            // list.GroupBy(x => new {x.LotBarcode, x.Model, x.ProductionDate}).Select(n => new ProductionModel
            var overAlllist =
                list.Select(n => new ProductionModel
                {
                    BarCode = n.Barcode,
                    MasterBarCode = n.MasterBarcode,
                    LotNo = n.LotBarcode,
                    DateTime = Convert.ToDateTime(n.ProductionDate).ToString("dd-MM-yyyy"),
                    Model=n.Model,
                    Status = "Hold",
                    //Total = n()
                }).ToList();

            return Json(new { data = overAlllist }, JsonRequestBehavior.AllowGet);


        }


        public JsonResult GetQcPassedOracleUploadedData(DateTime fromdate, DateTime todate)
        {

            var list = _reportRepository.GetQcPassedOracleUploadedData(fromdate, todate).ToList();

            //var list = reportService.GetLogisticsReportByDate(fromdate, todate).Where(a => a.Uploaded == null).ToList();

            var oQcListByDate = list.GroupBy(x => new { x.Model, x.LotBarcode, x.OracleTransactionCode }).Select(n => new QcItemsModel()
            {
                Model = n.Key.Model,
                LotBarcode = n.Key.LotBarcode,
                OracleTransactionCode = n.Key.OracleTransactionCode,
                QcPassedCount = n.Count(),
                OracleUploadCount = n.Count(a => a.OracleUploaded == true)
            }).ToList();

            return Json(oQcListByDate, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllHoldReport(string fromdate, string todate)
        {

            var list = _reportRepository.GetAllHoldReport(fromdate, todate);
            var overAlllist =
                list.Select(n => new ProductionModel
                {
                    LotNo = n.LotNumber,
                    BarCode = n.BarCode,
                    MasterBarCode = n.MasterBarcode,
                    DateTime = Convert.ToDateTime(n.ProductionDate).ToString("dd-MM-yyyy"),
                    ScanDateTime = Convert.ToDateTime(n.AddedDate).ToString("dd-MM-yyyy"),
                    Model = _common.GetProductionSetupDataByLotAndSerial(new ProductionSetup(){LotNumber = n.LotNumber, SerialNumber =n.LotTrackingSerial }).ModelName,
                    Status = "Holded",
                    Remarks = n.Remarks
                    //Total = _common.GetProductionDataDataByLotAndSerial(new ProductionSetup() { LotNumber = n.LotNumber, SerialNumber = n.LotTrackingSerial }).Count(),
                }).ToList();

            return Json(new { data = overAlllist }, JsonRequestBehavior.AllowGet);


        }
        public JsonResult GetAllRejectedReport(string fromdate, string todate)
        {

            var list = _reportRepository.GetAllRejectedReport(fromdate, todate);
            var overAlllist =
                list.Select(n => new ProductionModel
                {
                    LotNo = n.LotNumber,
                    BarCode = n.BarCode,
                    MasterBarCode = n.MasterBarcode,
                    DateTime = Convert.ToDateTime(n.ProductionDate).ToString("dd-MM-yyyy"),
                    ScanDateTime = Convert.ToDateTime(n.AddedDate).ToString("dd-MM-yyyy"),
                    Model = _common.GetProductionSetupDataByLot(new ProductionSetup() { LotNumber = n.LotNumber }).ModelName ?? "NULL",
                    Status = "Rejected",
                    Remarks = n.Remarks
                    //Total = _common.GetProductionDataDataByLotAndSerial(new ProductionSetup() { LotNumber = n.LotNumber, SerialNumber = n.LotTrackingSerial }).Count(),
                }).ToList();

            return Json(new { data = overAlllist }, JsonRequestBehavior.AllowGet);


        }

        public JsonResult GetCurrentlyRejectedReport()
        {

            var list = _reportRepository.GetCurrentlyRejectedReport();
            // list.GroupBy(x => new {x.LotBarcode, x.Model, x.ProductionDate}).Select(n => new ProductionModel
            var overAlllist =
                list.Select(n => new ProductionModel
                {
                    BarCode = n.BarCode,
                    MasterBarCode = n.MasterBarcode,
                    LotNo = n.LotNumber,
                    DateTime = Convert.ToDateTime(n.ProductionDate).ToString("dd-MM-yyyy"),
                    Model = _common.GetProductionSetupDataByLot(new ProductionSetup() { LotNumber = n.LotNumber }).ModelName ?? "NULL",
                    Status = "Rejected",
                    Remarks = n.Remarks,
                    //Total = n()
                }).ToList();

            return Json(new { data = overAlllist }, JsonRequestBehavior.AllowGet);


        }

        public JsonResult GetProductionLinetablePending(string fromdate, string todate,string shifts)
        {

            var list = _reportRepository.GetProductionLinetablePending(fromdate, todate).ToList();

            var js = new JavaScriptSerializer();
            var shift = js.Deserialize<string[]>(shifts);



            //list = (List<ProductionModel>) (from person in list
            //    where shift.Contains(person.ShiftName)
            //    select person);


            list = list.Where(x => shift.Contains(x.ShiftName)).ToList();

           // var baol = shift;

           // var names = new string[] { "Alex", "Colin", "Danny", "Diego" };
           // var aa = names;
            var overAlllist = new List<Models.ProductionModel>();

            foreach (var items in list)
            {
                var overAlll = new Models.ProductionModel();
                overAlll.BarCode = items.BarCode;
                overAlll.DateTime = items.Date.ToString("yyyy-MM-dd");
                overAlll.Model = items.Model;
                overAlll.ShiftName = items.ShiftName;
                overAlll.CategoryName = items.CategoryName;
                overAlll.Status = "Pending";
               // overAlll.Total = items.Total;

                overAlllist.Add(overAlll);
            }



            var jsonResult = Json(new { data = overAlllist }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;

            //var jsonResult = Json(overAlllist, JsonRequestBehavior.AllowGet);
            //jsonResult.MaxJsonLength = int.MaxValue;
            //return Json(new { data = jsonResult }, JsonRequestBehavior.AllowGet);


        }


        public JsonResult GetAllProductionReport(string fromdate, string todate, string shifts, string category)
        {

            var list = _reportRepository.GetAllProductionReport(fromdate, todate).ToList();

            var js = new JavaScriptSerializer();
            var shift = js.Deserialize<string[]>(shifts);
            var ctegry = js.Deserialize<string[]>(category);
            var shiftCount = shift.Length;
            var categoryCount = ctegry.Length;

            if (shiftCount > 0)

            {

                list = list.Where(x => shift.Contains(x.ShiftName)).ToList();


            }

            if (categoryCount > 0)
            {

                list = list.Where(x => ctegry.Contains(x.CategoryCode)).ToList();


            }

            var overAlllist = new List<Models.ProductionModel>();

            foreach (var items in list)
            {
                var overAlll = new Models.ProductionModel();
                overAlll.BarCode = items.BarCode;
                overAlll.DateTime = items.Date.ToString("yyyy-MM-dd");
                overAlll.Model = items.Model;
                overAlll.ShiftName = items.ShiftName;
                overAlll.CategoryName = items.CategoryName;
                overAlll.CategoryCode = items.CategoryCode;
                overAlll.Status = "Pending";
                // overAlll.Total = items.Total;

                overAlllist.Add(overAlll);
            }

            var oQcListByDate = overAlllist.GroupBy(x => new { x.Model, x.ShiftName, x.CategoryCode }).Select(n => new ProductionReportModel
            {
                Model = n.Key.Model,
                Shift = n.Key.ShiftName,
                Category = n.Key.CategoryCode,
                Count = n.Count()
            }).ToList();




            var jsonResult = Json(new { data = oQcListByDate }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;

        


        }





        public JsonResult GetQcPassedItems(string fromdate, string todate)
        {

            var list = _reportRepository.GetQcPassedItems(fromdate, todate).ToList();

            var overAlllist = new List<Models.ProductionModel>();

            foreach (var items in list)
            {
                var overAlll = new Models.ProductionModel();

                overAlll.DateTime = items.Date.ToString("yyyy-MM-dd");
                overAlll.Model = items.Model;
                overAlll.ShiftName = items.ShiftName;
                overAlll.CategoryName = items.CategoryName;
                overAlll.Status = "Passed";
                overAlll.Total = items.Total;

                overAlllist.Add(overAlll);
            }




            return Json(new { data = overAlllist }, JsonRequestBehavior.AllowGet);


        }


        public JsonResult GetBarcodeDetailsReport(string barCode)
        {
            var codedata = _reportRepository.GetBarcodeDetailsReport(barCode);
            return Json(new { data = codedata.ToList() }, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetManualPrintLogReport(string masterBarCode)
        {
            var codedata = _reportRepository.GetManualPrintLogReport(masterBarCode);
            return Json(new { data = codedata.ToList() }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult CodeDataLoader(string date, string model, string shift)
        {

            ViewBag.date = date;
            ViewBag.model = model;
            ViewBag.shift = shift;
         

            return View();

        }

        public ActionResult GetDataLoaderProductionALL(string fromdate, string todate, string model, string shift, string category)
        {

            ViewBag.fromdate = fromdate;
            ViewBag.todate = todate;
            ViewBag.model = model;
            ViewBag.shift = shift;
            ViewBag.category = category;

            return View();

        }





        public JsonResult GetBaroceForAllProduction(string fromdate, string todate, string model, string shift, string category)
        {

            var firstOrDefault = _common.GetShifts().FirstOrDefault(a => a.ShiftName == shift.Trim());
            if (firstOrDefault != null)
                shift = firstOrDefault.ShiftId;


            var barcodes = _reportRepository.GetBaroceForAllProduction(fromdate, todate, model, shift, category);
            var jsonResult = Json(barcodes, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;

        }
        public JsonResult GetModelsReport()
        {

            var models = _common.GetModels().ToList();
            var jsonResult = Json(new { data = models }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;

        }


        public JsonResult GetBaroceForDataLoader(string date, string model, string shift)
        {

            var firstOrDefault = _common.GetShifts().FirstOrDefault(a => a.ShiftName == shift.Trim());
            if (firstOrDefault != null)
                shift = firstOrDefault.ShiftId;


            var barcodes = _reportRepository.GetBaroceForDataLoader(date, model, shift);
            var jsonResult = Json(barcodes, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
          
        }




        public JsonResult GetLotWiseReport(string lotNumber)
        {
            var codedata = _reportRepository.GetLotWiseReport(lotNumber);
            return Json(new { data = codedata.ToList() }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQcTrackingLotReport(string lotNumber)
        {
            var codedata = _reportRepository.GetQcTrackingLotReport(lotNumber);
            return Json(new { data = codedata.ToList() }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetQcTrackingDateWiseReport(string fromdate, string todate)
        {
            var codedata = _reportRepository.GetQcTrackingDateWiseReport(fromdate, todate);

            var jsonResult = Json(new { data = codedata }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
           // return Json(new { data = codedata.ToList() }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMasterBarcodeWiseReport(string mCodeNumber)
        {
            var codedata = _reportRepository.GetMasterBarcodeWiseReport(mCodeNumber);
            return Json(new { data = codedata.ToList() }, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetLastSevenDaysProduction()
        {

            var data = _reportRepository.GetLastSevenDaysProduction();

            var oQcListByDate = data.GroupBy(x => new { x.ProductionDate, x.ShiftId, x.Model }).OrderBy(x => x.Key.ProductionDate).Select(n =>
            {
                var firstOrDefault = _common.GetShifts().FirstOrDefault(a => a.ShiftId == n.Key.ShiftId);
                return firstOrDefault != null ? new ProductionReportModel
               {
                   Date =   Convert.ToDateTime( n.Key.ProductionDate).ToString("dd-MM-yyyy"),
                   Shift = firstOrDefault.ShiftName,
                   Model = n.Key.Model,
                   Count = n.Count()
               } : null;
            }).ToList();



            var list2 = new List<ProductionReportModel>();

            if (oQcListByDate.Any())

            {
                foreach (var items in oQcListByDate)
                {
                    var indx = list2.FindIndex(i => i.Date == items.Date);
                    if (indx == -1)
                    {
                        items.OverAllData = items.Shift + " : (" + items.Model + "-" + items.Count+")";
                        list2.Add(items);
                    }

                    else
                    {
                        list2[indx].OverAllData = list2[indx].OverAllData + "\n" + items.Shift + " : (" + items.Model + "-" +
                                                  items.Count + ")";
                        list2[indx].Count += items.Count;
                    }

                }

            }



            return Json(list2, JsonRequestBehavior.AllowGet);
            //return Json(oQcListByDate, JsonRequestBehavior.AllowGet);
           
            

        }

        public ActionResult PassInventoryHold()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            
            
            return View();
        }

        public JsonResult GetPassedInventoryHoldData()
        {
            var list = _reportRepository.GetPassedInventoryHoldData().ToList();
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllPassedInventoryData(string fromdate, string todate)
        {

            var list = _reportRepository.GetAllPassedInventoryData(fromdate, todate);

            return Json(new { data = list }, JsonRequestBehavior.AllowGet);


        }

        public JsonResult GetHoldedPassData(string masterBarcode)
        {
            return Json(null, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetAllInventoryData(string fromdate, string todate)
        {

            var list = _reportRepository.GetAllInventoryData(fromdate, todate).ToList();

            var overAlllist = new List<Models.ProductionModel>();

            foreach (var items in list)
            {
                var overAlll = new Models.ProductionModel();

                overAlll.DateTime =Convert.ToDateTime( items.AddedDate).ToString("yyyy-MM-dd");
                overAlll.BarCode = items.Barcode;
                overAlll.MasterBarCode = items.MasterBarcode;
                if (items.IsMovedToOracle != null) overAlll.Uploaded = (bool) items.IsMovedToOracle==true?"Yes":"No";
                overAlllist.Add(overAlll);
            }




            return Json(new { data = overAlllist }, JsonRequestBehavior.AllowGet);


        }

        [HttpGet]
        public ActionResult MasterBarcodeWiseProduction()
        {
            return View();
        }

       



        public JsonResult GetAllMasterBarcodeWiseProductionReport(string fromdate, string todate, string shifts, string category)
        {

            var list = _reportRepository.GetAllMasterBarcodeWiseProductionReport(fromdate, todate).ToList();

            var js = new JavaScriptSerializer();
            var shift = js.Deserialize<string[]>(shifts);
            var ctegry = js.Deserialize<string[]>(category);
            var shiftCount = shift.Length;
            var categoryCount = ctegry.Length;

            if (shiftCount > 0)

            {

                list = list.Where(x => shift.Contains(x.Shift)).ToList();


            }

            if (categoryCount > 0)
            {

                list = list.Where(x => ctegry.Contains(x.CategoryCode)).ToList();


            }

            var jsonResult = Json(new { data = list }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;




        }


    }
}