﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using WCMS_MAIN.HelperClass;
using WCTS.Helper_Model;
using WCTS.Models;
using WCTS.Models.EntityModel;
using WCTS.Repository;


namespace WCTS.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        readonly Dictionary<int, SessionData> _sessiondictionary = SessionData.GetSessionValues();
        readonly ProductionRepository _productionRepository = new ProductionRepository();
        readonly Repository.Common _common = new Repository.Common();

        [HttpGet]
        [AllowAnonymous]
        public ActionResult LogIn()
        {
           
            Session.Clear();
            Session.Abandon();
            var cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "")
            {
                Expires = DateTime.Now.AddYears(-1)
            };
            Response.Cookies.Add(cookie1);
            var cookie2 = new HttpCookie("ASP.NET_SessionId", "") { Expires = DateTime.Now.AddYears(-1) };
            Response.Cookies.Add(cookie2);
            return View();
        }




      [AllowAnonymous]
        public ActionResult ChangePassword()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId != 0)
            {
                return View();
            }
                return RedirectToAction("LogOut", "Account");
        }

     
        
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult  Login(LoginModel login)
        {
            if (ModelState.IsValid)
            {
                var isExist = _common.GetLoginInfo(new Login() { UserId = login.UserId.Trim().ToLower(), Password=login.Password });

                //return RedirectToAction("Index", "Production");
                //return RedirectToAction("Index", "Production");
                if (isExist != null)
                {
                    var menus = _common.GetUserMenus(new tblSubMenu { RoleId = isExist.RoleId }).Select(x => new MenuModel
                    {
                        MainMenuId = (int)x.tblMainMenu.Id,
                        MainMenuName = x.tblMainMenu.MainMenu,
                        SubMenuId = (int)x.Id,
                        SubMenuName = x.SubMenu,
                        ControllerName = x.Controller,
                        ActionName = x.Action,
                        RoleId = (int?)x.RoleId

                    }).ToList();
                    var activeProject = _common.GetProductionSetup(new ProductionSetup() { LineId = isExist.LineId, IsActive = true });



                    //Menus Users
                    Session["AllMenus"] = menus;
                    System.Web.HttpContext.Current.Session["WCTSUserId"] = isExist.UserId;
                    System.Web.HttpContext.Current.Session["WCTSRoleId"] = isExist.RoleId;
                    System.Web.HttpContext.Current.Session["WCTSUserName"] = isExist.UserName;
                    System.Web.HttpContext.Current.Session["WCTSEmployeeId"] = isExist.EmployeeId;
                    //ProjectInfo


                    if (activeProject != null)
                    {
                        System.Web.HttpContext.Current.Session["WCTSModel"] = activeProject.ModelName;
                        System.Web.HttpContext.Current.Session["WCTSShiftId"] = activeProject.ShiftId;
                        System.Web.HttpContext.Current.Session["WCTSShiftName"] = activeProject.ShiftName;
                        System.Web.HttpContext.Current.Session["WCTSCategoryCode"] = activeProject.CategoryCode;
                        System.Web.HttpContext.Current.Session["WCTSLineId"] = activeProject.LineId;
                        System.Web.HttpContext.Current.Session["WCTSLineName"] = activeProject.LineName;
                        System.Web.HttpContext.Current.Session["WCTSItemCode"] = activeProject.ItemCode;
                        System.Web.HttpContext.Current.Session["WCTSLotNumber"] = activeProject.LotNumber;
                        System.Web.HttpContext.Current.Session["WCTSProjectSetupId"] = activeProject.Id;
                        System.Web.HttpContext.Current.Session["WCTSInventoryType"] = activeProject.InventoryType;
                        System.Web.HttpContext.Current.Session["WCTSProductionDate"] = activeProject.ProductionDate;
                   
                    
                    
                    }
                  

                    if (isExist.RoleId == 1 || isExist.RoleId == 2 || isExist.RoleId == 3)
                    {
                        return RedirectToAction("Index", "Home");
                    }

                    if (isExist.RoleId == 4 && activeProject == null)
                    {
                        return RedirectToAction("ProductionSetup", "Production");


                    }
                    if (isExist.RoleId == 10003)
                    {
                        return RedirectToAction("CheckSerial", "UpdatePanel");


                    }

                    if (isExist.RoleId == 4 && activeProject.ModelName != null)
                    {
                        return RedirectToAction("Index", "Production");


                    }


                    else
                    {

                        return RedirectToAction("Index", "Home");


                    }


                }

                else
                {
                    ViewBag.ErrorMsg = "Please enter the valid credentials!";
                    return View();
                }
                //return RedirectToAction("Index", "Production");
                //var user = await UserManager.FindAsync(model.UserName, model.Password);
                //if (user != null)
                //{
                //    await SignInAsync(user, model.RememberMe);
                //    return RedirectToLocal(returnUrl);
                //}
                //else
                //{
                //    ModelState.AddModelError("", "Invalid username or password.");
                //}
            }

            // If we got this far, something failed, redisplay form
            return View(login);
        }

        public ActionResult LogOut()
        {

            //Dictionary<int, SessionData> IDictionary = SessionData.GetSessionValues();
            //long userId = (long)IDictionary[3].Id;
            //var _CommonService = new CommonService();
            //var result = _CommonService.UpdateLastEntry(userId);

            Session.Clear();
            Session.Abandon();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            FormsAuthentication.SignOut();
            var cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "") { Expires = DateTime.Now.AddYears(-1) };
            Response.Cookies.Add(cookie1);
            var cookie2 = new HttpCookie("ASP.NET_SessionId", "") { Expires = DateTime.Now.AddYears(-1) };
            Response.Cookies.Add(cookie2);
            return RedirectToAction("LogIn");
        }

        
    }
}