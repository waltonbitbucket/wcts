﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WCTS.Models.ApiModels;
using WCTS.Models.EntityModel;
using WCTS.Repository;

namespace WCTS.Controllers.API_Controllers
{
    public class DisplayController : ApiController
    {

        //readonly ProductionRepository _productionRepository = new ProductionRepository();
       // readonly Repository.Common _common = new Repository.Common();
        private readonly WCTSEntities _entities = new WCTSEntities();

        [HttpGet]
        public ProductionModel[] Production()
        {
            var finalData = new ProductionModel[] {};
            try
            {
                var activeProject = _entities.ProductionSetup.FirstOrDefault(a => a.IsActive == true);

              
                if (activeProject != null)
                {

                    var startDate = Convert.ToDateTime(activeProject.ProductionDate);
                    var endDate = startDate.AddDays(1).AddTicks(-1);
                    var prodcutionCount = _entities.ProductionMaster.Count(a => a.ProductionDate >= startDate && a.ProductionDate <= endDate && a.ShiftId == activeProject.ShiftId);
                    var allModels =
                        _entities.ProductionSetup.Where(
                            a =>
                                a.ProductionDate >= startDate && a.ProductionDate <= endDate).OrderByDescending(a=>a.AddedDate).ToList();

                    var models = allModels.GroupBy(x => new {x.ModelName,x.IsActive,x.ShiftId,x.ShiftName}).Select(items => new ProductionModel
                    {
                        Model = items.Key.ModelName,
                        Active = items.Key.IsActive==true?"1":"0",
                        Shift =  items.Key.ShiftName.Substring(0,  items.Key.ShiftName.IndexOf(' '))    
                    });



                    finalData = allModels.Select(items => new ProductionModel
                    {
                        ActiveProject = activeProject,
                        Achieved = prodcutionCount,
                        WorkingModels = models,
                        Status = "1"

                    }).Take(1).ToArray();

                }
            }
            catch (Exception ex)
            {

                throw new CultureNotFoundException("Error Occured While Getting Information");
            }

            return finalData;

        }

    }
}