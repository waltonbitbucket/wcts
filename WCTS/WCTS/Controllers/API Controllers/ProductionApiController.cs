﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using WCTS.Helper_Model;
using WCTS.Models;
using WCTS.Models.ApiModels;
using WCTS.Models.EntityModel;

namespace WCTS.Controllers.API_Controllers
{
    public class ProductionApiController : ApiController
    {

        [HttpGet]
        public Models.ApiModels.ProductionModel Production()
        {
            var reports = new Models.ApiModels.ProductionModel();
            //RBSYNERGYEntities _entities = new RBSYNERGYEntities();
            // credentials.Data = _entities.tblProductRegistrations.ToList();
            return reports;
        }

        [HttpGet]
        public Models.ApiModels.ProductionModel Production(string barcode)
        {
            var groupSummary = new Models.ApiModels.ProductionModel();

            var entities = new WCTSEntities();
            var pmaster = new ProductionMaster();
            pmaster.Barcode = barcode;
            entities.ProductionMaster.Add(pmaster);

            entities.SaveChanges();

            var lists = entities.ProductionMaster.Where(a => a.IsPrinted == null).ToList();
            // var cnt = entities.ProductionMaster.ToList();
            groupSummary.Success = true;
            groupSummary.BatchItems = lists;
            groupSummary.LotNo = lists[0].LotBarcode;
            groupSummary.MasterBatch = lists[0].MasterBarcode;
            return groupSummary;

        }

        [HttpGet]
        public IHttpActionResult GetPDMCompressorSumamry(DateTime date)
        {
            try
            {
                bool isExist = Request.Headers.Contains("Authorization");
                if (!isExist)
                {
                    return BadRequest("Unauthorized");
                }
                var headerValue = Request.Headers.GetValues("Authorization").FirstOrDefault();
                if (headerValue != "wbct@#!32")
                {
                    return BadRequest("Unauthorized");
                }

                string query = string.Format(@"SELECT  
p.Model,
ps.LineName Line,
ps.LineName Point,
CONVERT(VARCHAR(10),CONVERT(date, p.AddedDate), 120) as Date,
datepart(Hour,p.AddedDate) as Hour,
COUNT(*) Count
FROM ProductionMaster p INNER JOIN ProductionSetup ps ON p.ProjectSetupId=ps.Id
WHERE CONVERT(date,p.AddedDate)='{0}'
GROUP BY p.Model,ps.LineName, CONVERT(date,p.AddedDate),datepart(Hour,p.AddedDate)", date.ToString("yyyy-MM-dd"));
                var entities = new WCTSEntities();
                var masterProduction = entities.Database.SqlQuery<PDMDataVm>(query).ToList();
                return Ok(masterProduction);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }




    }
}
