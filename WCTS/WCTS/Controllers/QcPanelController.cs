﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BR_BLL;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using WCMS_MAIN.HelperClass;
using WCTS.Helper_Model;
using WCTS.Models.EntityModel;
using WCTS.Repository;
using ProductionModel = WCTS.Models.ProductionModel;

namespace WCTS.Controllers
{
    public class QcPanelController : Controller
    {

        private readonly Dictionary<int, SessionData> _sessiondictionary = SessionData.GetSessionValues();
        private readonly ProductionRepository _productionRepository = new ProductionRepository();
        private readonly Repository.Common _common = new Repository.Common();
        private readonly NotificationRepository _notificationRepository = new NotificationRepository();
        //
        // GET: /QcPanel/
        public ActionResult Qc()
        {
            var userId = (long) _sessiondictionary[1].Id;
            var userName = _sessiondictionary[2].Name;
            var employeeId = _sessiondictionary[3].Name;
            var model = _sessiondictionary[4].Name;
            var shiftId = _sessiondictionary[5].Name;
            var shiftName = _sessiondictionary[6].Name;
            var categoryCode = _sessiondictionary[7].Name;
            var lineId = _sessiondictionary[8].Id;
            var lineName = _sessiondictionary[9].Name;


            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewBag.Model = model;
                ViewBag.ShiftName = shiftName;
                ViewBag.LineName = lineName;
                ViewBag.CategoryCode = categoryCode;


                return View();
            }
        }



        public ActionResult ViewHoldItems()
        {
            var userId = (long)_sessiondictionary[1].Id;          

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }
           
                return View();
            
        }

        public ActionResult ViewQcPassedItems()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();

        }


        public ActionResult ViewRejectedItems()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();

        }
        public ActionResult QcLotTracking()
        {
            var userId = (long)_sessiondictionary[1].Id;

            if (userId == 0)
            {

                return RedirectToAction("LogOut", "Account");
            }

            return View();

        }


        public JsonResult GetBarcodeInformation(string barcode)
        {
            var barcodeInfo = _productionRepository.GetBarCodeInfo(barcode);




            return Json(barcodeInfo, JsonRequestBehavior.AllowGet);
        }






        public JsonResult UpdateQcLotTrackingInfo(QcLotTracking qcLotTracking)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var result = new Result();
            var userName = _sessiondictionary[2].Name;
            var employeeId = _sessiondictionary[3].Name;
            if (userId != 0)
            {
                try
                {
                    var isExist = _common.GetQcTrackingLotInfo(qcLotTracking.LotNumber);
                    if (isExist == null)
                    {
                        result.IsSuccess = false;
                        result.Message = "Error Occured ! Data Mismatch";
                    }
                    else
                    {
                        isExist.LotNumber = qcLotTracking.LotNumber;
                        isExist.MarkingLotBarcode = qcLotTracking.MarkingLotBarcode;
                        isExist.Rotor = qcLotTracking.Rotor;
                        isExist.CrankCase = qcLotTracking.CrankCase;
                        isExist.Piston = qcLotTracking.Piston;
                        isExist.Screw_FB = qcLotTracking.Screw_FB;
                        isExist.Gasket_Leaf = qcLotTracking.Gasket_Leaf;
                        isExist.SuctionValveLeaf = qcLotTracking.SuctionValveLeaf;
                        isExist.ValvePlate = qcLotTracking.ValvePlate;
                        isExist.UniversalGasket = qcLotTracking.UniversalGasket;
                        isExist.CylinderHead = qcLotTracking.CylinderHead;
                        isExist.SuctionMuffler = qcLotTracking.SuctionMuffler;
                        isExist.SMFSpring = qcLotTracking.SMFSpring;
                        isExist.DischargeGasket = qcLotTracking.DischargeGasket;
                        isExist.Screw_CH = qcLotTracking.Screw_CH;
                        isExist.StatorScrew = qcLotTracking.StatorScrew;
                        isExist.SuspensionSpring = qcLotTracking.SuspensionSpring;
                        isExist.Stator = qcLotTracking.Stator;
                        isExist.LowerShellAssembly = qcLotTracking.LowerShellAssembly;
                        isExist.InternalDischargeTube = qcLotTracking.InternalDischargeTube;
                        isExist.BrazingRing = qcLotTracking.BrazingRing;
                        isExist.UpperShell = qcLotTracking.UpperShell;
                        isExist.SuctionRubberPlug = qcLotTracking.SuctionRubberPlug;
                        isExist.ServiceRubberPlug = qcLotTracking.ServiceRubberPlug;
                        isExist.DischargeRubberPlug = qcLotTracking.DischargeRubberPlug;
                        isExist.TBA = qcLotTracking.TBA;
                        isExist.Remarks = qcLotTracking.Remarks;
                        isExist.UpdatedBy = employeeId;
                        isExist.UpdatedDate = DateTime.Now;
                        isExist.IaProductionDate   = qcLotTracking.IaProductionDate;
                        isExist.MajorIssue = qcLotTracking.MajorIssue;
                        isExist.Model = qcLotTracking.Model;
                        isExist.LotByQc = qcLotTracking.LotByQc;
                        isExist.ShrinkedRotor = qcLotTracking.ShrinkedRotor;
                        isExist.CrankShaft = qcLotTracking.CrankShaft;
                        isExist.Flang = qcLotTracking.Flang;
                        isExist.ConRod = qcLotTracking.ConRod;
                        isExist.PistonLot = qcLotTracking.PistonLot;
                        isExist.PistonPinLot = qcLotTracking.PistonPinLot;
                        isExist.ValvePlateLot = qcLotTracking.ValvePlateLot;





                        result.IsSuccess = _productionRepository.UpdateQcLotTrackingInfo(isExist);
                    }

                    

                    return Json(result);
                }
                catch (Exception ex)
                {
                    throw ex;

                }
            }
            else
            {
                return Json(new Result { Id = "0" });
            }
        }



        public JsonResult InsertQcLotTrackingInfo(QcLotTracking qcLotTracking)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var result = new Result();
            var userName = _sessiondictionary[2].Name;
            var employeeId = _sessiondictionary[3].Name;
            if (userId != 0)
            {
                try
                {
                    qcLotTracking.AddedBy =userName+"("+ employeeId+")";
                    qcLotTracking.AddedDate = DateTime.Now;
                    result.IsSuccess = _productionRepository.InsertQcLotTrackingInfo(qcLotTracking);

                    return Json(result);
                }
                catch (Exception ex)
                {
                    throw ex;

                }
            }
            else
            {
                return Json(new Result { Id = "0" });
            }
        }




        public JsonResult GetLotBarcodeInformationByDateAndSerial(string date, string serial)
        {
            var lotBarcode = _productionRepository.GetLotBarcodeByDateAndSerial(date, serial);

            var productionModelList = new List<ProductionModel>();
           
            if (lotBarcode.Count != 0)
            {

                foreach (var data in lotBarcode)
                {
                    var productionModel = new ProductionModel();
                    var getLotBarcode = data.LotNumber; //.Select(a => a.LotBarcode).FirstOrDefault()


                    {
                        var lotbarcodeInfo = _productionRepository.GetLotBarcodeInformation(getLotBarcode);

                        if (lotbarcodeInfo.Count > 0)
                        {
                            productionModel.LotNo = lotbarcodeInfo[0].LotBarcode;
                            productionModel.Model = lotbarcodeInfo[0].Model;
                            productionModel.CategoryCode = lotbarcodeInfo[0].CategoryCode;
                            var catName =
                                _common.GetCategories()
                                    .FirstOrDefault(a => a.CategoryCode == lotbarcodeInfo[0].CategoryCode);
                            if (catName != null)
                                productionModel.CategoryName = catName.CategoryName;
                            var firstOrDefault
                                = _common.GetShifts().FirstOrDefault(a => a.ShiftId == lotbarcodeInfo[0].ShiftId);
                            if (firstOrDefault != null)
                                productionModel.ShiftName = firstOrDefault.ShiftName;
                            productionModel.DateTime = Convert.ToDateTime(lotbarcodeInfo[0].ProductionDate)
                                .ToString("yyyy-MM-dd");
                            productionModel.Total = lotbarcodeInfo.Count;
                            productionModel.Passed = lotbarcodeInfo[0].IsPassed.ToString();
                            productionModel.ProjectSetupId = lotbarcodeInfo[0].ProjectSetupId;
                            productionModel.Serial = _common.GetProductionSerialByLot(new ProductionSetup() { LotNumber = productionModel.LotNo }).SerialNumber;

                            if (lotbarcodeInfo[0].IsPassed == true)
                            {
                                productionModel.Status = "Passed";

                            }
                            if (lotbarcodeInfo[0].IsHold == null && lotbarcodeInfo[0].IsPassed == null)
                            {
                                productionModel.Status = "Pending";

                            }

                            if (lotbarcodeInfo[0].IsHold == true)
                            {
                                productionModel.Status = "Hold";

                            }

                            productionModelList.Add(productionModel);
                        }
                    }

                
                    
                }



            }
            return Json(productionModelList, JsonRequestBehavior.AllowGet);
        }




        public JsonResult GetLotBarcodeInformationByMasterBarcode(string barcode)
        {
            var productionModel = new ProductionModel();
           // var getLotBarcode = _productionRepository.GetMasterBarcodeData(barcode).Select(a=>a.LotBarcode).FirstOrDefault();

            try
            {
               
                //var isAlreadyQcDone = _productionRepository.IsAlreadyInQC(new QcItems() { MasterBarcode = barcode });
               // if (isAlreadyQcDone.Count == 0)
                //{
                   // var getLotBarcode = _productionRepository.GetMasterBarcodeData(barcode);

                var getLotBarcode = _productionRepository.GetMasterBarcodeDataForQc(barcode);

                

                    if (getLotBarcode != null)
                    {
                        if (getLotBarcode.Count > 0)
                        {
                            productionModel.LotNo = getLotBarcode[0].LotBarcode;
                            productionModel.MasterBarCode = getLotBarcode[0].MasterBarcode;
                            productionModel.Model = getLotBarcode[0].Model;
                            productionModel.CategoryCode = getLotBarcode[0].CategoryCode;
                            var catName = _common.GetCategories().FirstOrDefault(a => a.CategoryCode == getLotBarcode[0].CategoryCode);
                            if (catName != null)
                                productionModel.CategoryName = catName.CategoryName;
                            var firstOrDefault
                                = _common.GetShifts().FirstOrDefault(a => a.ShiftId == getLotBarcode[0].ShiftId);
                            if (firstOrDefault != null)
                                productionModel.ShiftName = firstOrDefault.ShiftName;
                            productionModel.DateTime = Convert.ToDateTime(getLotBarcode[0].AddedDate).ToString("yyyy-MM-dd");
                            productionModel.Total = getLotBarcode.Count;
                            productionModel.Passed = getLotBarcode[0].IsPassed.ToString();
                            productionModel.ProjectSetupId = getLotBarcode[0].ProjectSetupId;
                            productionModel.Serial = _common.GetProductionSerialByLot(new ProductionSetup() { LotNumber = productionModel.LotNo }).SerialNumber;
                            if (getLotBarcode[0].IsPassed == true)
                            {
                                productionModel.Status = "Passed";

                            }
                            if (getLotBarcode[0].IsHold == null && getLotBarcode[0].IsPassed == null)
                            {
                                productionModel.Status = "Pending";

                            }

                            if (getLotBarcode[0].IsHold == true)
                            {
                                productionModel.Status = "Hold";

                            }
                        }
                        else
                        {
                            productionModel.Status = "0";
                            productionModel.Message = "No Data Found/Already Qc Complete";
                        }

                    }



                //}

                else
                {
                    productionModel.Status = "0";
                    productionModel.Message = "Already Qc Complete";
                }
            }
            catch (Exception exception)
            {
                
               productionModel.Status = "0";
               productionModel.Message = exception.ToString();
            }
            return Json(productionModel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLotBarcodeInformationByBarcode(string barcode)
        {
            // var getLotBarcode = _productionRepository.GetMasterBarcodeData(barcode).Select(a=>a.LotBarcode).FirstOrDefault();
            var productionModel = new ProductionModel();
            try
            {
                var isAlreadyQcDone = _productionRepository.IsAlreadyInQC(new QcItems() { Barcode = barcode });
                if (isAlreadyQcDone.Count == 0)
                {
                    var getLotBarcode = _productionRepository.GetBarCodeInfo(barcode);

                    if (getLotBarcode != null)
                    {
                        productionModel.LotNo = getLotBarcode.LotBarcode;
                        productionModel.MasterBarCode = getLotBarcode.MasterBarcode;
                        productionModel.Model = getLotBarcode.Model;
                        productionModel.CategoryCode = getLotBarcode.CategoryCode;
                        var catName =
                            _common.GetCategories().FirstOrDefault(a => a.CategoryCode == getLotBarcode.CategoryCode);
                        if (catName != null)
                            productionModel.CategoryName = catName.CategoryName;
                        var firstOrDefault
                            = _common.GetShifts().FirstOrDefault(a => a.ShiftId == getLotBarcode.ShiftId);
                        if (firstOrDefault != null)
                            productionModel.ShiftName = firstOrDefault.ShiftName;
                        productionModel.DateTime = Convert.ToDateTime(getLotBarcode.AddedDate).ToString("yyyy-MM-dd");
                        productionModel.Total = 1;
                        productionModel.Passed = getLotBarcode.IsPassed.ToString();
                        productionModel.ProjectSetupId = getLotBarcode.ProjectSetupId;
                        productionModel.Serial =
                            _common.GetProductionSerialByLot(new ProductionSetup() { LotNumber = productionModel.LotNo })
                                .SerialNumber;
                        if (getLotBarcode.IsPassed == true)
                        {
                            productionModel.Status = "Passed";

                        }
                        if (getLotBarcode.IsHold == null && getLotBarcode.IsPassed == null)
                        {
                            productionModel.Status = "Pending";

                        }

                        if (getLotBarcode.IsHold == true)
                        {
                            productionModel.Status = "Hold";

                        }
                    }

                    else
                    {
                        productionModel.Status = "0";
                        productionModel.Message = "No Data Found";
                    }
                }

                else
                {

                    productionModel.Status = "0";
                    productionModel.Message = "Already Qc Complete";

                }
            }
            catch (Exception exception)
            {

                productionModel.Status = "0";
                productionModel.Message = exception.ToString();
            }
           

            return Json(productionModel, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetLotBarcodeInformation(string barcode)
        {
            var productionModel = new ProductionModel();

            try
            {
                //var isAlreadyQcDone = _productionRepository.IsAlreadyInQC(new QcItems() { LotBarcode = barcode });
                //if (isAlreadyQcDone.Count == 0)
               // {
                    var lotbarcodeInfo = _productionRepository.GetLotBarcodeInformation(barcode);



                    if (lotbarcodeInfo.Count > 0)
                    {

                        productionModel.LotNo = lotbarcodeInfo[0].LotBarcode;
                        productionModel.Model = lotbarcodeInfo[0].Model;
                        productionModel.CategoryCode = lotbarcodeInfo[0].CategoryCode;


                        var catName = _common.GetCategories().FirstOrDefault(a => a.CategoryCode == lotbarcodeInfo[0].CategoryCode);
                        if (catName != null)
                            productionModel.CategoryName = catName.CategoryName;


                        var firstOrDefault
                            = _common.GetShifts().FirstOrDefault(a => a.ShiftId == lotbarcodeInfo[0].ShiftId);
                        if (firstOrDefault != null)
                            productionModel.ShiftName = firstOrDefault.ShiftName;
                        productionModel.DateTime = Convert.ToDateTime(lotbarcodeInfo[0].AddedDate).ToString("yyyy-MM-dd");
                        productionModel.Total = lotbarcodeInfo.Count;
                        productionModel.Passed = lotbarcodeInfo[0].IsPassed.ToString();
                        productionModel.ProjectSetupId = lotbarcodeInfo[0].ProjectSetupId;
                        productionModel.Serial = _common.GetProductionSerialByLot(new ProductionSetup() { LotNumber = productionModel.LotNo }).SerialNumber;
                        if (lotbarcodeInfo[0].IsPassed == true)
                        {
                            productionModel.Status = "Passed";

                        }
                        if (lotbarcodeInfo[0].IsHold == null && lotbarcodeInfo[0].IsPassed == null)
                        {
                            productionModel.Status = "Pending";

                        }

                        if (lotbarcodeInfo[0].IsHold == true)
                        {
                            productionModel.Status = "Hold";

                        }
                    }
                    else
                    {
                        productionModel.Status = "0";
                        productionModel.Message = "No Data Found/Already Qc Done";
                    }

                //}


                //else
                //{
                //    productionModel.Status = "0";
                //    productionModel.Message = "Already Qc Complete";
                //}
               

            }
            catch (Exception exception)
            {
                productionModel.Status = "0";
                productionModel.Message = exception.ToString();
                //throw;
            }
            return Json(productionModel, JsonRequestBehavior.AllowGet);
        }


        #region Date And Serial Wise Qc Hold Items
        public JsonResult HoldDateAndSerialWiseData(List<QcItems> qcItemses)
        {
            var userId = (long)_sessiondictionary[1].Id;

            var result = new Result();
            var result1 = new Result();
            if (userId != 0)
            {
                try
                {

                    foreach (var qcItemse in qcItemses)
                    {

                        result.IsSuccess = _productionRepository.HoldLotItems(qcItemse.LotBarcode, userId, qcItemse.Remarks);

                        if (result.IsSuccess)
                        {
                            var a = new WctsNotification()
                            {

                                Title = "Lot Wise Item Hold",
                                Description = "Lot No " + qcItemse.LotBarcode + " Was Hold by QC Remarks = " + qcItemse.Remarks + "",
                                TargetController = "QcPanel",
                                TargetAction = "ViewHoldItems",
                                Seen = false,
                                AddedBy = userId,
                                AddedDate = DateTime.Now
                            };
                            result1.IsSuccess = _notificationRepository.InsertNotificationInfo(a);

                        }

                    }


                    
                }
                catch (Exception exception)
                {
                    result.IsSuccess = false;
                    result.Message = exception.ToString();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            else
            {
                return Json(new Result { Id = "0" });
            }
        }


        #endregion




        #region Date And Serial Wise Qc Pass Items



        public JsonResult InsertProductionPassedItems(List<QcItems> qcItemses)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var employeeId = _sessiondictionary[3].Name;
            var result = new Result();
            var result1 = new Result();
            var oracleresult = new Result();
            var transresult = new Result();
            if (userId != 0)
            {
                try
                {
                    int countProcess = 0;
                    foreach (var data in qcItemses)
                    {
                        countProcess += 1;
                        var lotItems = _productionRepository.GetLotBarcodeInformationForQcPass(data.LotBarcode);
                        result.IsSuccess = _productionRepository.InsertQcPanelInfo(lotItems);
                        if (result.IsSuccess)
                        {
                            var sb = new StringBuilder();
                            sb.Append(" Item -" + countProcess + "Passed Successfully!");
                            result1.IsSuccess = _productionRepository.UpdateProductionMasterByLotPass(data.LotBarcode);
                            if (result1.IsSuccess)
                            {
                                sb.Append(", Item -" + countProcess + " Updated Successfully!");
                            }
                            else
                            {
                                sb.Append(",  Item -" + countProcess + "Updated failed ! ");
                            }
                            sb.Append(Environment.NewLine);
                            oracleresult = _productionRepository.PushQcPassToOracle(lotItems);
                            if (oracleresult.IsSuccess)
                            {
                                transresult.IsSuccess =
                                   _productionRepository.UpdateQcOracleStatus(lotItems[0].OracleTransactionCode, employeeId);
                                sb.Append(", Item -" + countProcess + " Uploaded To Oracle Successfully!");
                            }
                            else
                            {
                                sb.Append(", Item -" + countProcess + " Oracle Upload Failed !  Reason :" + oracleresult.Message + " ");
                            }

                            result.Message = sb.ToString();
                        }


                        #region modification

                    //    var lotItems = _productionRepository.GetLotBarcodeInformation(data.LotBarcode);
                    //    var lotList = new List<QcItems>();

                    //    var itemdata = new QcItems
                    //    {
                    //        LotBarcode = lotItems[0].LotBarcode
                    //    };


                    //    lotList.Add(itemdata);
                    //    var dto = new DataTable();


                    //    var qcTrackId = Guid.NewGuid();
                    //    dto.Columns.AddRange(new DataColumn[13]
                    //{
                    //new DataColumn("QcCheckedTrackingId", typeof (string)),
                    //new DataColumn("Model", typeof (string)),
                    //new DataColumn("Barcode", typeof (string)),
                    //new DataColumn("LotBarcode", typeof (string)),
                    //new DataColumn("MasterBarcode", typeof (string)),
                    //new DataColumn("ProjectSetupId", typeof (string)),
                    //new DataColumn("Passed", typeof (bool)),
                    //new DataColumn("IsHold", typeof (bool)),
                    //new DataColumn("IsRejected", typeof (bool)),
                    //new DataColumn("AddedBy", typeof (string)),
                    //new DataColumn("AddedDate", typeof (string)),
                    // new DataColumn("UpdatedBy", typeof (string)),
                    //new DataColumn("UpdatedDate", typeof (string))
                    
                    //});
                    //    foreach (var productionMaster in lotItems)
                    //    {

                    //        var row = dto.NewRow();
                    //        row["QcCheckedTrackingId"] = qcTrackId;
                    //        row["Model"] = productionMaster.Model;
                    //        row["Barcode"] = productionMaster.Barcode;
                    //        row["LotBarcode"] = productionMaster.LotBarcode;
                    //        row["MasterBarcode"] = productionMaster.MasterBarcode;
                    //        row["ProjectSetupId"] = productionMaster.ProjectSetupId;
                    //        row["Passed"] = true;
                    //        row["IsHold"] = false;
                    //        row["IsRejected"] = false;
                    //        row["AddedBy"] = userId;
                    //        row["AddedDate"] = DateTime.Now;
                    //        row["UpdatedBy"] = userId;
                    //        row["UpdatedDate"] = DateTime.Now;
                    //        dto.Rows.Add(row);

                    //        //var lotData = new QcItems();
                    //        //lotData.Model = productionMaster.Model;
                    //        //lotData.Barcode = productionMaster.Barcode;
                    //        //lotData.MasterBarcode = productionMaster.MasterBarcode;
                    //        //lotData.ProjectSetupId = productionMaster.ProjectSetupId;
                    //        //lotData.AddedBy = productionMaster.AddedBy;
                    //        //lotData.AddedDate = DateTime.Now;
                    //        //lotList.Add(lotData);
                    //    }

                    //    result.IsSuccess = _productionRepository.InsertQcPanelInfo(lotList, dto);

                    //    if (result.IsSuccess)
                    //    {

                    //        result1.IsSuccess = _productionRepository.UpdateProductionMaterQcPassedItems(data.LotBarcode);


                    //    }

                        #endregion

                    }

                    return Json(result, JsonRequestBehavior.AllowGet);

                }
                catch (Exception exception)
                {
                    result.IsSuccess = false;
                    result.Message = exception.ToString();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }


            else
            {
                return Json(new Result { Id = "0" });
            }




        }


        #endregion


        #region lot Wise Pass
        public JsonResult InsertQcPanelInfo(List<QcItems> qcItemses)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var employeeId = _sessiondictionary[3].Name;
            var result = new Result();
            var result1 = new Result();
            var oracleresult = new Result();
            var transresult = new Result();
            if (userId != 0)
            {
                try
                {
                    //var lotItems = _productionRepository.GetLotBarcodeInformation(qcItemses[0].LotBarcode);

                    var lotItems = _productionRepository.GetLotBarcodeInformationForQcPass(qcItemses[0].LotBarcode);

                    #region Modification

                    //var lotList = new List<QcItems>();

                    //var itemdata = new QcItems
                    //{
                    //    LotBarcode = lotItems[0].LotBarcode
                    //};


                    //lotList.Add(itemdata);
                    //var dto = new DataTable();


                    //var qcTrackId = Guid.NewGuid();
                    //dto.Columns.AddRange(new DataColumn[13]
                    //{
                    //new DataColumn("QcCheckedTrackingId", typeof (string)),
                    //new DataColumn("Model", typeof (string)),
                    //new DataColumn("Barcode", typeof (string)),
                    //new DataColumn("LotBarcode", typeof (string)),
                    //new DataColumn("MasterBarcode", typeof (string)),
                    //new DataColumn("ProjectSetupId", typeof (string)),
                    //new DataColumn("Passed", typeof (bool)),
                    //new DataColumn("IsHold", typeof (bool)),
                    //new DataColumn("IsRejected", typeof (bool)),
                    //new DataColumn("AddedBy", typeof (string)),
                    //new DataColumn("AddedDate", typeof (string)),
                    // new DataColumn("UpdatedBy", typeof (string)),
                    //new DataColumn("UpdatedDate", typeof (string))
                    
                    //});
                    //foreach (var productionMaster in lotItems)
                    //{

                    //    var row = dto.NewRow();
                    //    row["QcCheckedTrackingId"] = qcTrackId;
                    //    row["Model"] = productionMaster.Model;
                    //    row["Barcode"] = productionMaster.Barcode;
                    //    row["LotBarcode"] = productionMaster.LotBarcode;
                    //    row["MasterBarcode"] = productionMaster.MasterBarcode;
                    //    row["ProjectSetupId"] = productionMaster.ProjectSetupId;
                    //    row["Passed"] = true;
                    //    row["IsHold"] = false;
                    //    row["IsRejected"] = false;
                    //    row["AddedBy"] = userId;
                    //    row["AddedDate"] = DateTime.Now;
                    //    row["UpdatedBy"] = userId;
                    //    row["UpdatedDate"] = DateTime.Now;
                    //    dto.Rows.Add(row);

                    //}
                    //result.IsSuccess = _productionRepository.InsertQcPanelInfo(lotList, dto);
                    #endregion

                   // result.IsSuccess = _productionRepository.InsertQcPanelInfo(lotList, dto);

                    result.IsSuccess = _productionRepository.InsertQcPanelInfo(lotItems);
                    if (result.IsSuccess)
                    {
                        var sb = new StringBuilder();
                        sb.Append("Passed Successfully!");
                        result1.IsSuccess = _productionRepository.UpdateProductionMasterByLotPass(qcItemses[0].LotBarcode);

                        if (result1.IsSuccess)
                        {
                            sb.Append(", Updated Successfully!");
                        }
                        else
                        {
                            sb.Append(", Updated failed ! ");
                        }

                        oracleresult = _productionRepository.PushQcPassToOracle(lotItems);
                        if (oracleresult.IsSuccess)
                        {
                            transresult.IsSuccess =
                               _productionRepository.UpdateQcOracleStatus(lotItems[0].OracleTransactionCode, employeeId);
                            sb.Append(", Uploaded To Oracle Successfully!");
                        }
                        else
                        {
                            sb.Append(", Oracle Upload Failed !  Reason :" + oracleresult .Message+ " ");
                        }

                        result.Message = sb.ToString();
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);

                }
                catch (Exception exception)
                {
                    result.IsSuccess = false;
                    result.Message = exception.ToString();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }


            else
            {
                return Json(new Result { Id = "0" });
            }


        }

        #endregion lot Wise Pass

        #region Master Barcode Wise Pass
        public JsonResult InsertQcPassInfoByMasterBarcode(string mastercode)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var employeeId = _sessiondictionary[3].Name;
            var result = new Result();
            var result1 = new Result();
            var transresult = new Result();
            var oracleresult = new Result();

            if (userId != 0)
            {
                try
                {
                    //var masterbarcodedata = _productionRepository.GetMasterBarcodeData(mastercode);
                    var masterbarcodedata = _productionRepository.GetMasterBarcodeDataForQcPass(mastercode);
                    


                    #region Modification
                    //var dto = new DataTable();
                    //var qcTrackId = Guid.NewGuid();
                    //dto.Columns.AddRange(new DataColumn[13]
                    //{
                    //new DataColumn("QcCheckedTrackingId", typeof (string)),
                    //new DataColumn("Model", typeof (string)),
                    //new DataColumn("Barcode", typeof (string)),
                    //new DataColumn("LotBarcode", typeof (string)),
                    //new DataColumn("MasterBarcode", typeof (string)),
                    //new DataColumn("ProjectSetupId", typeof (string)),
                    //new DataColumn("Passed", typeof (bool)),
                    //new DataColumn("IsHold", typeof (bool)),
                    //new DataColumn("IsRejected", typeof (bool)),
                    //new DataColumn("AddedBy", typeof (string)),
                    //new DataColumn("AddedDate", typeof (string)),
                    // new DataColumn("UpdatedBy", typeof (string)),
                    //new DataColumn("UpdatedDate", typeof (string))
                    
                    //});
                    //foreach (var productionMaster in masterbarcodedata)
                    //{

                    //    var row = dto.NewRow();
                    //    row["QcCheckedTrackingId"] = qcTrackId;
                    //    row["Model"] = productionMaster.Model;
                    //    row["Barcode"] = productionMaster.Barcode;
                    //    row["LotBarcode"] = productionMaster.LotBarcode;
                    //    row["MasterBarcode"] = productionMaster.MasterBarcode;
                    //    row["ProjectSetupId"] = productionMaster.ProjectSetupId;
                    //    row["Passed"] = true;
                    //    row["IsHold"] = false;
                    //    row["IsRejected"] = false;
                    //    row["AddedBy"] = userId;
                    //    row["AddedDate"] = DateTime.Now;
                    //    row["UpdatedBy"] = userId;
                    //    row["UpdatedDate"] = DateTime.Now;
                    //    dto.Rows.Add(row);
                    //}
                    //result.IsSuccess = _productionRepository.InsertQcPassInfoByMasterBarcode(dto, mastercode);
                    #endregion


                    result.IsSuccess = _productionRepository.InsertQcPanelInfo(masterbarcodedata);
                    if (result.IsSuccess)
                    {
                        var sb = new StringBuilder();
                        sb.Append("Passed Successfully!");
                        result1.IsSuccess = _productionRepository.UpdateProductionMasterByMasterBarcodePass(mastercode);

                        if (result1.IsSuccess)
                        {
                            sb.Append(", Updated Successfully!");
                        }
                        else
                        {
                            sb.Append(", Updated failed ! ");
                        }

                        oracleresult = _productionRepository.PushQcPassToOracle(masterbarcodedata);
                        if (oracleresult.IsSuccess)
                        {
                            transresult.IsSuccess =
                                _productionRepository.UpdateQcOracleStatus(masterbarcodedata[0].OracleTransactionCode, employeeId);
                            sb.Append(", Uploaded To Oracle Successfully!");
                        }
                        else
                        {
                            sb.Append(", Oracle Upload Failed !  Reason :" + oracleresult.Message + " ");
                        }

                        result.Message = sb.ToString();

                    }


                    return Json(result, JsonRequestBehavior.AllowGet);

                }
                catch (Exception exception)
                {
                    result.IsSuccess = false;
                    result.Message = exception.ToString();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }


            else
            {
                return Json(new Result { Id = "0" });
            }




        }

        #endregion Master Barcode Wise Pass

        #region  Barcode Wise Pass
        public JsonResult InsertPassDataByBarcode(string barcode)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var userName = _sessiondictionary[2].Name;
            var result = new Result();
            var result1 = new Result();
            if (userId != 0)
            {
                try
                {
                    var masterbarcodedata = _productionRepository.GetBarCodeInfo(barcode);
                    //var lotList = new List<QcItems>();

                    //var itemdata = new QcItems
                    //{
                    //    LotBarcode = masterbarcodedata[0].LotBarcode
                    //};


                    //lotList.Add(itemdata);
                    var dto = new DataTable();
                    var qcTrackId = Guid.NewGuid();
                    dto.Columns.AddRange(new DataColumn[13]
                    {
                         new DataColumn("QcCheckedTrackingId", typeof (string)),
                    new DataColumn("Model", typeof (string)),
                    new DataColumn("Barcode", typeof (string)),
                    new DataColumn("LotBarcode", typeof (string)),
                    new DataColumn("MasterBarcode", typeof (string)),
                    new DataColumn("ProjectSetupId", typeof (string)),
                    new DataColumn("Passed", typeof (bool)),
                    new DataColumn("IsHold", typeof (bool)),
                    new DataColumn("IsRejected", typeof (bool)),
                    new DataColumn("AddedBy", typeof (string)),
                    new DataColumn("AddedDate", typeof (string)),
                      new DataColumn("UpdatedBy", typeof (string)),
                    new DataColumn("UpdatedDate", typeof (string))
                    
                    });
                  
                    

                        var row = dto.NewRow();
                        row["QcCheckedTrackingId"] = qcTrackId;
                        row["Model"] = masterbarcodedata.Model;
                        row["Barcode"] = masterbarcodedata.Barcode;
                        row["LotBarcode"] = masterbarcodedata.LotBarcode;
                        row["MasterBarcode"] = masterbarcodedata.MasterBarcode;
                        row["ProjectSetupId"] = masterbarcodedata.ProjectSetupId;
                        row["Passed"] = true;
                        row["IsHold"] = false;
                        row["IsRejected"] = false;
                        row["AddedBy"] = userId;
                        row["AddedDate"] = DateTime.Now;
                        row["UpdatedBy"] = userId;
                        row["UpdatedDate"] = DateTime.Now;

                        dto.Rows.Add(row);

                        //var lotData = new QcItems();
                        //lotData.Model = productionMaster.Model;
                        //lotData.Barcode = productionMaster.Barcode;
                        //lotData.MasterBarcode = productionMaster.MasterBarcode;
                        //lotData.ProjectSetupId = productionMaster.ProjectSetupId;
                        //lotData.AddedBy = productionMaster.AddedBy;
                        //lotData.AddedDate = DateTime.Now;
                        //lotList.Add(lotData);


                        result.IsSuccess = _productionRepository.InsertPassDataByBarcode(dto, barcode);



                        if (result.IsSuccess)
                        {
                            var a = new WctsNotification()
                            {

                                Title = "Barcode Wise Item Passed",
                                Description = "Barcode No " + barcode + " Was Passed by QC",
                                TargetController = "QcPanel",
                                TargetAction = "ViewQcPassedItems",
                                Seen = false,
                                AddedBy = userId,
                                AddedDate = DateTime.Now
                            };
                            result1.IsSuccess = _notificationRepository.InsertNotificationInfo(a);

                        }
                    return Json(result, JsonRequestBehavior.AllowGet);

                }
                catch (Exception exception)
                {
                    result.IsSuccess = false;
                    result.Message = exception.ToString();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }


            else
            {
                return Json(new Result { Id = "0" });
            }




        }

        #endregion  Barcode Wise Pass










        public JsonResult HoldLotItems(string lotNumebr, string remarks)
        {
            var userId = (long)_sessiondictionary[1].Id;
          
            var result = new Result();
            var result1 = new Result();
            if (userId != 0)
            {
                try
                {
                    result.IsSuccess = _productionRepository.HoldLotItems(lotNumebr, userId, remarks);

                    if (result.IsSuccess)
                    {
                        var a = new WctsNotification()
                        {

                            Title = "Lot Wise Item Hold",
                            Description = "Lot No " + lotNumebr + " Was Hold by QC Remarks = " + remarks + "",
                            TargetController = "QcPanel",
                            TargetAction = "ViewHoldItems",
                            Seen = false,
                            AddedBy = userId,
                            AddedDate = DateTime.Now
                        };
                        result1.IsSuccess = _notificationRepository.InsertNotificationInfo(a);

                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                catch (Exception exception)
                {
                    result.IsSuccess = false;
                    result.Message = exception.ToString();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }


            else
            {
                return Json(new Result { Id = "0" });
            }
        }

        public JsonResult HoldMasterBarcodeItems(string masterBarcodeNumebr, string remarks, string lotNumebr)
        {
            var userId = (long)_sessiondictionary[1].Id;
          
            var result = new Result();
            var result1 = new Result();
            if (userId != 0)
            {
                try
                {
                    result.IsSuccess = _productionRepository.HoldMasterBarcodeItems(masterBarcodeNumebr, userId, remarks, lotNumebr);

                    if (result.IsSuccess)
                    {
                        var a = new WctsNotification()
                        {

                            Title = "Master Barcode Wise Item Hold",
                            Description = "Master barcode No " + masterBarcodeNumebr + " Was Hold by QC Remarks = " + remarks + "",
                            TargetController = "QcPanel",
                            TargetAction = "ViewHoldItems",
                            Seen = false,
                            AddedBy = userId,
                            AddedDate = DateTime.Now
                        };
                        result1.IsSuccess = _notificationRepository.InsertNotificationInfo(a);

                    }
                    
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                catch (Exception exception)
                {
                    result.IsSuccess = false;
                    result.Message = exception.ToString();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }


            else
            {
                return Json(new Result { Id = "0" });
            }
        }

        public JsonResult InsertManualPrintDataLog(List<ManualPrintLog> manualPrintLog)
        {
            var userId = (long)_sessiondictionary[1].Id;
           var employeeId = _sessiondictionary[3].Name;
            var roleId = (long)_sessiondictionary[15].Id;
            var result = new Result();
            var result1 = new Result();
            if (userId != 0)
            {
                try
                {
                    if (manualPrintLog.Any())
                    {

                        //var ifduplicateLot = _productionRepository.CheckDuplicateLotNumber(manualPrintLog[0].LotBarCode);

                        //if (!ifduplicateLot)
                        //{
                            var ifduplicateMasterCode = _productionRepository.CheckDuplicateMasterBarcode(manualPrintLog[0].MasterBarCode);

                            if (!ifduplicateMasterCode)
                            {
                                manualPrintLog.ForEach(c => c.AddedBy = employeeId);
                                manualPrintLog.ForEach(c => c.AddeDate = DateTime.Now);
                                result = _productionRepository.InsertManualPrintDataLog(manualPrintLog);

                            }
                            else
                            {
                                result.IsSuccess = false;
                                result.Message = "Duplicat Master Barcode Number !!!";
                                return Json(result, JsonRequestBehavior.AllowGet);
                            }

                        }
                        //else
                        //{
                        //    result.IsSuccess = false;
                        //    result.Message = "Duplicat Lot Number !!!";
                        //    return Json(result, JsonRequestBehavior.AllowGet);
                        //}

                    //}
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                catch (Exception exception)
                {
                    result.IsSuccess = false;
                    result.Message = exception.ToString();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }


            else
            {
                return Json(new Result { Id = "0" });
            }
        }

        public JsonResult HoldBarcodeItems(string barcode, string remarks, string lotNumebr)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var roleId = (long)_sessiondictionary[15].Id;
            var result = new Result();
            var result1 = new Result();
            if (userId != 0)
            {
                try
                {
                    result.IsSuccess = _productionRepository.HoldBarcodeItems(barcode, userId, remarks, lotNumebr);
                    if (result.IsSuccess)
                    {
                        var a = new WctsNotification()
                        {

                            Title = "Barcode Wise Item Hold",
                            Description = "Barcode No " + barcode + " Was Hold by QC Remarks = " + remarks + "",
                            TargetController = "QcPanel",
                            TargetAction = "ViewHoldItems",
                            Seen = false,
                            AddedBy = userId,
                            //RoleId = roleId,
                            AddedDate = DateTime.Now
                        };
                        result1.IsSuccess = _notificationRepository.InsertNotificationInfo(a);

                    }
                    
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                catch (Exception exception)
                {
                    result.IsSuccess = false;
                    result.Message = exception.ToString();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }


            else
            {
                return Json(new Result { Id = "0" });
            }
        }

        public JsonResult GetHoldItems()
        {
            var holdDataList = _productionRepository.GetHoldItems();
            var overAlllist =
                holdDataList.GroupBy(x => new { x.HoldTrackingId, x.LotNumber, x.LotTrackingSerial,x.HoldType }).Select(n => new ProductionModel
                {
                    HoldTrackingId = n.Key.HoldTrackingId,
                    LotNo = n.Key.LotNumber,
                    Serial = n.Key.LotTrackingSerial,
                    Status = "Hold",
                    HoldType = n.Key.HoldType,
                   // DateTime = Convert.ToDateTime(n.Key.AddedDate).ToString("dd-MM-yyyy"),
                  
                    Total = n.Count()
                }).ToList();

            return Json(new { data = overAlllist }, JsonRequestBehavior.AllowGet);

            //return Json(barcodeInfo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetrejectedItems()
       {
           var data = _productionRepository.GetrejectedItems();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);

            //return Json(barcodeInfo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProductionTimeWiseData(string fromdate, string todate)
        {
            var data = _productionRepository.GetProductionTimeWiseData(fromdate, todate);



            var jsonResult = Json(new { data = data }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;

            //return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }


       [HttpPost]
        public JsonResult GetProductionMultiBarcodeData(string strdata )
        {
           
            var modelData = JsonConvert.DeserializeObject<List<ProductionModel>>(strdata);
            var data = _productionRepository.GetProductionMultiBarcodeData(modelData);
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetHoldedItemDetailsData(string holdTracking, string lotNo, string serial)
        {
            var holdDataListDetails = _productionRepository.GetHoldedItemDetailsData(holdTracking, lotNo, serial);
            return Json(new { data = holdDataListDetails }, JsonRequestBehavior.AllowGet);
        }


        #region Prepare Hold Data
        public JsonResult PrepareHoldDataToUpdate(List<ProductionModel> holdData)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var roleId = (long)_sessiondictionary[15].Id;
            var result1 = new Result();
            var passeditems = holdData.Where(a => a.Status.ToLower() == "y").ToList();
            var rejected = holdData.Where(a => a.Status.ToLower() == "n").ToList();
            Result result = _productionRepository.PrepareHoldDataToUpdate(holdData, userId);

            if (result.IsSuccess)
            {

                if (passeditems.Any())
                {

                    var b = new WctsNotification()
                    {

                        Title = "Holt Items Passed",
                        Description = "Total  " + holdData.Count() + " Items Was Processed by QC Items Passed = " + passeditems.Count() + "",
                        TargetController = "QcPanel",
                        TargetAction = "ViewQcPassedItems",
                        Seen = false,
                        AddedBy = userId,
                        //RoleId = roleId,
                        AddedDate = DateTime.Now
                    };
                    result1.IsSuccess = _notificationRepository.InsertNotificationInfo(b);


                }
                if (rejected.Any())
                {
                    var a = new WctsNotification()
                    {

                        Title = "Holt Items Rejected",
                        Description = "Total  " + holdData.Count() + " Items Was Rejected by QC Items   Rejected = " + rejected.Count() + "",
                        TargetController = "QcPanel",
                        TargetAction = "ViewRejectedItems",
                        Seen = false,
                        AddedBy = userId,
                        //RoleId = roleId,
                        AddedDate = DateTime.Now
                    };
                    result1.IsSuccess = _notificationRepository.InsertNotificationInfo(a);
                }
               

            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Prepare Rejected Data
        public JsonResult PrepareRejectedDataToUpdate(List<ProductionModel> rejected)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var roleId = (long)_sessiondictionary[15].Id;
            Result result = _productionRepository.PrepareRejectedDataToUpdate(rejected, userId);
            var result1 = new Result();
            var passeditems = rejected.Where(a => a.Status.ToLower() == "y");
            if (result.IsSuccess)
            {
                var a = new WctsNotification()
                {

                    Title = "Rejected Items Processed",
                    Description = "Total  " + rejected.Count() + " Items Was Processed by QC, Items Passed = " + passeditems.Count() + "",
                    TargetController = "QcPanel",
                    TargetAction = "ViewQcPassedItems",
                    Seen = false,
                    AddedBy = userId,
                    //RoleId = roleId,
                    AddedDate = DateTime.Now
                };
                result1.IsSuccess = _notificationRepository.InsertNotificationInfo(a);

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion


        public JsonResult PrepareScannedTimeWiseData(List<ProductionModel> data)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var roleId = (long)_sessiondictionary[15].Id;
            var result1 = new Result();
            var passeditems = data.Where(a => a.Status.ToLower() == "y").ToList();
            var rejected = data.Where(a => a.Status.ToLower() == "n").ToList();
            Result result = _productionRepository.PrepareScannedTimeWiseData(data, userId);

            if (result.IsSuccess)
            {

                if (passeditems.Any())
                {

                    var b = new WctsNotification()
                    {

                        Title = "Production Time Wise Items Passed ",
                        Description = "Total  " + data.Count() + " Items Was Processed by QC Items Passed = " + passeditems.Count() + "",
                        TargetController = "QcPanel",
                        TargetAction = "ViewQcPassedItems",
                        Seen = false,
                        AddedBy = userId,
                        //RoleId = roleId,
                        AddedDate = DateTime.Now
                    };
                    result1.IsSuccess = _notificationRepository.InsertNotificationInfo(b);


                }
                if (rejected.Any())
                {
                    var a = new WctsNotification()
                    {

                        Title = "Production Time Wise Items Hold",
                        Description = "Total  " + data.Count() + " Items Was Processed  by QC    Hold = " + rejected.Count() + "",
                        TargetController = "QcPanel",
                        TargetAction = "ViewHoldItems",
                        Seen = false,
                        AddedBy = userId,
                        //RoleId = roleId,
                        AddedDate = DateTime.Now
                    };
                    result1.IsSuccess = _notificationRepository.InsertNotificationInfo(a);
                }


            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }




        public JsonResult CheckAndUploadToOracle(string model,string lot,string transcode)
        {
            var userId = (long)_sessiondictionary[1].Id;
            var employeeId = _sessiondictionary[3].Name;
            var result = new Result();
            var transresult = new Result();
            var oracleresult = new Result();

            if (userId != 0)
            {
                try
                {
                    var data = _productionRepository.CheckAndUploadToOracle(model, lot, transcode);
                    if (data.Count>0)
                    {
                        var sb = new StringBuilder();
                        oracleresult = _productionRepository.PushQcPassToOracle(data);
                        if (oracleresult.IsSuccess)
                        {
                            transresult.IsSuccess = _productionRepository.UpdateQcOracleStatusReProcess(transcode, employeeId,model, lot);
                            sb.Append(", Uploaded To Oracle Successfully!");
                        }
                        else
                        {
                            sb.Append(", Oracle Upload Failed !  Reason :" + oracleresult.Message + " ");
                        }
                        result.IsSuccess = true;
                        result.Message = sb.ToString();
                    }

                    else
                    {
                        result.IsSuccess = false;
                        result.Message = "No Data Found";

                    }
                    return Json(result, JsonRequestBehavior.AllowGet);

                }
                catch (Exception exception)
                {
                    result.IsSuccess = false;
                    result.Message = exception.ToString();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }

            else
            {
                return Json(new Result { Id = "0" });
            }

        }



        
    }
}