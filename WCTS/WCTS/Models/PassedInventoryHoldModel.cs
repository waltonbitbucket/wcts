﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCTS.Models
{
    public class PassedInventoryHoldModel
    {
        public string MasterBarcode { get; set; }
        public int Total { get; set; }
        public string Remarks { get; set; }

        public string Barcode { get; set; }
        public string LotBarcode { get; set; }
        public DateTime? ProductionDateTime { get; set; }
        public DateTime? PassedDateTime { get; set; }
        public DateTime? AddedDateTime { get; set; }
        public long? AddedBy { get; set; }
        public bool IsPassed { get; set; }

    }
}