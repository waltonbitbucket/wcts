﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using Quartz;
using WCTS.Helper_Model;
using WCTS.Models.EntityModel;
using WCTS.Repository;

namespace WCTS.Models
{
    public class InventoryPriInsertJob:IJob
    {
         readonly InventoryRepository _inventoryRepository = new InventoryRepository();
         private readonly NotificationRepository _notificationRepository = new NotificationRepository();
        public void Execute(IJobExecutionContext context)
        {
            var passedInventoryInsertresult = new Result();
            var passedItemUpdateresult = new Result();
            var notificationResult = new Result();
            var list = new List<PassedInventory>();
           
                try
                {
                    var passedData= _inventoryRepository.GetPassedItemToPrepareForInventory().ToList();
                    if (passedData.Count > 0)
                    {
                        foreach (var data in passedData)
                    {
                        var isExistsinPassedInventory = _inventoryRepository.IsExistsInPassedInventory(data.Barcode.Trim());
                        if (!isExistsinPassedInventory)
                        {
                            var passedInventoryModelData=new PassedInventory();
                            passedInventoryModelData.ProjectSetupId = data.ProjectSetupId;
                            passedInventoryModelData.Model = data.Model;
                            passedInventoryModelData.ShiftId = data.ShiftId;
                            passedInventoryModelData.CategoryCode = data.CategoryCode; 
                            passedInventoryModelData.ItemCode = data.ItemCode;
                            passedInventoryModelData.Barcode = data.Barcode;
                            passedInventoryModelData.MasterBarcode = data.MasterBarcode;
                            passedInventoryModelData.ProductionDate = data.ProductionDate;
                            passedInventoryModelData.IsHold = false;
                            passedInventoryModelData.IsMovedToFinalInventory = false;
                            passedInventoryModelData.PassedDate = data.PassedDate;
                            passedInventoryModelData.ScannedBy = data.AddedBy;
                            passedInventoryModelData.ScannedDate = data.AddedDate;
                            passedInventoryModelData.AddedBy = 9911;
                            passedInventoryModelData.AddedDate = DateTime.Now;
                            list.Add(passedInventoryModelData);
                        }
                    }


                    passedInventoryInsertresult.IsSuccess = _inventoryRepository.InventoryPassedItemsInsert(list);

                    if (passedInventoryInsertresult.IsSuccess)
                    {

                        passedItemUpdateresult.IsSuccess=_inventoryRepository.UpdateProductionMasterMovedItems(list);


                        if (passedItemUpdateresult.IsSuccess)
                        {


                            var a = new WctsNotification()
                            {

                                Title = "Passed Inventory",
                                Description = "Items Are Ready to upload AT EBS the whole process will start after 15 Miniutes, Model='" + passedData[0].Model + "' Total Items Moved='" + passedData.Count + "'",
                                TargetController = "Production",
                                TargetAction = "Index",
                                Seen = false,
                                AddedBy = 9911,
                                AddedDate = DateTime.Now
                            };
                            notificationResult.IsSuccess = _notificationRepository.InsertNotificationInfo(a);
                        }


                    }

                    else
                    {
                        passedInventoryInsertresult.Message = "error occured while saving the information";
                   

                    }

                    }


                }
                catch (Exception)
                {
                    
                }


              
            }

        }
    }
