﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using System;


namespace WCTS.Models
{
    public class JobScheduler
    {
        public static void Start()
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            var job = JobBuilder.Create<InventoryPriInsertJob>().Build();

            var trigger = TriggerBuilder.Create()
                   .WithIdentity("trigger1", "group1")
                   .StartNow()
                   .WithSimpleSchedule(x => x
                       .WithIntervalInSeconds(50)
                       .RepeatForever())
                   .Build();

            scheduler.ScheduleJob(job, trigger);
        }
       
    }
}