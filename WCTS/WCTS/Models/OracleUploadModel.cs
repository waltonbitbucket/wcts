﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCTS.Models
{
    public class OracleUploadModel
    {
        public long SequenceId { get; set; }
        public int OrganizationId { get; set; }
        public String TranscationDate { get; set; }
        public int EbsItemId { get; set; }
        public string ItemModel { get; set; }
        public string DataSource { get; set; }
        public string ItemColor { get; set; }
        public string ItemVersion { get; set; }
        public int? JobId { get; set; }
        public string SeriaNo { get; set; }
        public string SeriaNo2 { get; set; }
        public string MasterSerialNo { get; set; }
        public string Line { get; set; }
        public string OracleTransCode { get; set; }
       // public int? ImpFlag { get; set; }
       // public int? ImpReference { get; set; }
        public string CreatedBy { get; set; }
        public string Grade { get; set; }
        public string GradeReason { get; set; }
        public DateTime CreationDate { get; set; }
        public string LastUpdatedBy { get; set; }
        public DateTime LastDateUpdatedDate { get; set; }
        public string Remarks { get; set; }
    }
}