﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCTS.Models
{
    public class LoginModel
    {

        public string UserId { get; set; }
        public string Password { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public long? UserRoleId { get; set; }
        public int? UserLineId { get; set; }
        public int? PackLineId { get; set; }
        public string RoleName { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeId { get; set; }
        public Boolean Remember { get; set; }

    }
}