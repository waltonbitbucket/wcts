﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCTS.Models
{
    public class ProductionReportModel
    {
        public string MasterBarCode { get; set; }
        public string Model { get; set; }
        public string Date { get; set; }
        public DateTime AddedDate { get; set; }
        public string Shift { get; set; }
        public string Category { get; set; }
        public int? Count { get; set; }

        public string OverAllData { get; set; }
    }
}