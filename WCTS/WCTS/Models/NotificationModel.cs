﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCTS.Models
{
    public class NotificationModel
    {

        public long Id { get; set; }
        public long? UseId { get; set; }
        public long? RoleId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TargetAction { get; set; }
        public string TargetController { get; set; }
        public long AddedName { get; set; }
        public string Type { get; set; }
        public string Parameter { get; set; }

        public DateTime? AddedDate { get; set; }

        public long AddedBy { get; set; }

        public bool Seen { get; set; }

    }
}