﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz;
using WCMS_MAIN.HelperClass;
using WCTS.Helper_Model;
using WCTS.Models.EntityModel;
using WCTS.Repository;
namespace WCTS.Models
{
    public class FinalInventoryProcessor : IJob
    {
        readonly InventoryRepository _inventoryRepository = new InventoryRepository();
        private readonly NotificationRepository _notificationRepository = new NotificationRepository();
      
        public void Execute(IJobExecutionContext context)
        {
            var passedFinalInventoryInsertresult = new Result();
            var passedInventoryUpdateresult = new Result();
            var notificationResult = new Result();
            var list = new List<FinalInventory>();
            try
            {
              
                var getPassedInventoryItems = _inventoryRepository.GetPassedItemFromInventoryItems().ToList();

                var guid = Guid.NewGuid();

                if (getPassedInventoryItems.Count > 0)
                {

                    foreach (var items in getPassedInventoryItems)
                    {
                        var data = new FinalInventory();


                     
                        if (items.AddedDate != null)
                        {

                            var curent= DateTime.Now.Minute;
                            var itemTime = Convert.ToInt32(items.AddedDate.Value.Minute);

                            if (curent < itemTime)
                            {

                                curent = curent + 60;
                            }
                            var diff = curent - itemTime;

                            if (diff > 1)
                            {

                                data.Barcode = items.Barcode;
                                data.MasterBarcode = items.MasterBarcode;
                                data.LotBarcode = items.LotBarcode;
                                data.AddedBy = 999;
                                data.AddedDate = DateTime.Now;
                                data.OracleTrncCode = guid.ToString();
                                data.IsMovedToOracle = true;
                                list.Add(data);


                            }
                        }
                    }

                    if (list.Count > 0)
                    {

                        passedFinalInventoryInsertresult.IsSuccess = _inventoryRepository.ProcessFinalInvetoryData(list);

                    }

                }


            }
            catch (Exception)
            {
                
                throw;
            }


        }



    }
}