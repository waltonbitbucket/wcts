﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCTS.Models.ApiModels
{
    public class ProductionModel
    {

        public string Barcode { get; set; }
        public string Shift { get; set; }
        public string Line { get; set; }
        public bool Success { get; set; }
        public int Count { get; set; }
        public int Target { get; set; }
        public int Achieved { get; set; }
        public object BatchItems { get; set; }
        public string LotNo { get; set; }
        public string MasterBatch { get; set; }
        public string Model { get; set; }
        public object ActiveProject { get; set; }
        public object WorkingModels { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }

        public string Active { get; set; }

    }
}