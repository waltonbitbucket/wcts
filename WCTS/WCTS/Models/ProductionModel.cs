﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCTS.Models
{
    public class ProductionModel
    {
        public long? ProjectSetupId { get; set; }
        public long? InventoryTypeId { get; set; }
        public string InventoryName { get; set; }
        public string Model { get; set; }
        public string Serial { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string BarCode { get; set; }
        public string ItemCode { get; set; }
        public string MasterBarCode { get; set; }
        public string LotNo { get; set; }
        public string ShiftId { get; set; }
        public string ShiftName { get; set; }
        public DateTime Date { get; set; }
        public int? Total { get; set; }
        public double? Weight { get; set; }
        public string Status { get; set; }
        public string DateTime { get; set; }
        public string ScanDateTime { get; set; }
        public string Passed { get; set; }
        public string Rejected { get; set; }
        public string HoldTrackingId { get; set; }
        public string RejectTrackingId { get; set; }
         
        public string HoldedDate { get; set; }
        public string HoldType{ get; set; }
        public string RejectedDate { get; set; }
        public string Remarks { get; set; }
        public string Message { get; set; }
        public DateTime ProductionDate { get; set; }
        public DateTime ScannedDate { get; set; }
        public string Min { get; set; }
        public int RemainTime { get; set; }
        public string Hour { get; set; }
        public string Sec { get; set; }
        public string Uploaded { get; set; }
    }
}