﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCTS.Models
{
    public class QcItemsModel
    {

        public long Id { get; set; }
        public string QcCheckedTrackingId { get; set; }
        public string Model { get; set; }
        public string Barcode { get; set; }
        public string LotBarcode { get; set; }
        public string MasterBarcode { get; set; }
        public long ProjectSetupId { get; set; }
        public bool Passed { get; set; }
        public long AddedBy { get; set; }
        public DateTime AddedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool IsHold { get; set; }
        public bool IsRejected { get; set; }
        public string Remarks { get; set; }
        public string Category { get; set; }
        public string Line { get; set; }
        public string Shift { get; set; }
        public string ItemCode { get; set; }
        public string Serial { get; set; }
        public bool OracleUploaded { get; set; }
        public DateTime OracleUplodedDate { get; set; }
        public string OracleUploadedBy { get; set; }
        public string OracleTransactionCode { get; set; }

        public int QcPassedCount { get; set; }

        public int OracleUploadCount { get; set; }
    }
}