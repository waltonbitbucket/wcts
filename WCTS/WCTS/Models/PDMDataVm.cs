﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCTS.Models
{
    public class PDMDataVm
    {   
        public string Date { get; set; }
        public string Model { get; set; }
        public string Line { get; set; }
        public string Point { get; set; }        
        public int Hour { get; set; }
        public int Count { get; set; }
    }
}