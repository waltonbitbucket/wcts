﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;

namespace WCTS.Models
{
    public class FinalInventoryJob
    {
        public static void Start()
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            var job = JobBuilder.Create<FinalInventoryProcessor>().Build();

            var trigger = TriggerBuilder.Create()
                   .WithIdentity("trigger2", "group2")
                   .StartNow()
                   .WithSimpleSchedule(x => x
                       .WithIntervalInSeconds(100)
                       .RepeatForever())
                   .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}