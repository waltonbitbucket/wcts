//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WCTS.Models.EntityModel
{
    using System;
    
    public partial class SPWCTSLineProduction_Result
    {
        public string Model { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string ShiftName { get; set; }
        public Nullable<int> Total { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
    }
}
