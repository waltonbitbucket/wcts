//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WCTS.Models.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class LotHold
    {
        public long Id { get; set; }
        public string HoldTrackingId { get; set; }
        public string LotNumber { get; set; }
        public string BarCode { get; set; }
        public string MasterBarcode { get; set; }
        public string LotTrackingSerial { get; set; }
        public Nullable<System.DateTime> ProductionDate { get; set; }
        public Nullable<System.DateTime> ProductionAddedDate { get; set; }
        public string Remarks { get; set; }
        public string HoldType { get; set; }
        public Nullable<bool> Passed { get; set; }
        public Nullable<bool> Hold { get; set; }
        public Nullable<bool> IsRejected { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> PassedBy { get; set; }
        public Nullable<System.DateTime> PassedDate { get; set; }
        public Nullable<long> RejectedBy { get; set; }
        public Nullable<System.DateTime> RejectedDate { get; set; }
    }
}
