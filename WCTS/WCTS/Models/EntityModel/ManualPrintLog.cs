//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WCTS.Models.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ManualPrintLog
    {
        public long Id { get; set; }
        public string LotBarCode { get; set; }
        public string Model { get; set; }
        public string MasterBarCode { get; set; }
        public string BarCode { get; set; }
        public string AddedBy { get; set; }
        public Nullable<System.DateTime> AddeDate { get; set; }
    }
}
