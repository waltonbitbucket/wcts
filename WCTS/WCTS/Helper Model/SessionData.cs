﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCMS_MAIN.HelperClass
{
    public class SessionData
    {
        public  SessionData(long id,string userName)
        {
            Id = id;
            Name = userName;

        }
            public long Id { get; set; }
            public string Name { get; set; }
    
             public static Dictionary<int, SessionData> GetSessionValues()
            {
                var sessionData = new Dictionary<int, SessionData>();

                if (HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "UserId");
                    sessionData.Add(1, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), "UserId");
                    sessionData.Add(1, aSessionData);
                }





                if (HttpContext.Current.Session["WCTSUserName"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "UserName");
                    sessionData.Add(2, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), HttpContext.Current.Session["WCTSUserName"].ToString());
                    sessionData.Add(2, aSessionData);
                }


                if (HttpContext.Current.Session["WCTSEmployeeId"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "EmployeeId");
                    sessionData.Add(3, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), HttpContext.Current.Session["WCTSEmployeeId"].ToString());
                    sessionData.Add(3, aSessionData);
                }


                if (HttpContext.Current.Session["WCTSModel"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "Model");
                    sessionData.Add(4, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), HttpContext.Current.Session["WCTSModel"].ToString());
                    sessionData.Add(4, aSessionData);
                }


                if (HttpContext.Current.Session["WCTSShiftId"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "ShiftId");
                    sessionData.Add(5, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), HttpContext.Current.Session["WCTSShiftId"].ToString());
                    sessionData.Add(5, aSessionData);
                }



                if (HttpContext.Current.Session["WCTSShiftName"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "ShiftName");
                    sessionData.Add(6, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), HttpContext.Current.Session["WCTSShiftName"].ToString());
                    sessionData.Add(6, aSessionData);
                }



                if (HttpContext.Current.Session["WCTSCategoryCode"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "CategoryCode");
                    sessionData.Add(7, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), HttpContext.Current.Session["WCTSCategoryCode"].ToString());
                    sessionData.Add(7, aSessionData);
                }


                if (HttpContext.Current.Session["WCTSLineId"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "LineId");
                    sessionData.Add(8, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), HttpContext.Current.Session["WCTSLineId"].ToString());
                    sessionData.Add(8, aSessionData);
                }

                if (HttpContext.Current.Session["WCTSLineName"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "LineName");
                    sessionData.Add(9, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), HttpContext.Current.Session["WCTSLineName"].ToString());
                    sessionData.Add(9, aSessionData);
                }


                if (HttpContext.Current.Session["WCTSItemCode"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "itemCode");
                    sessionData.Add(10, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), HttpContext.Current.Session["WCTSItemCode"].ToString());
                    sessionData.Add(10, aSessionData);
                }

                if (HttpContext.Current.Session["WCTSLotNumber"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "lotNumber");
                    sessionData.Add(11, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), HttpContext.Current.Session["WCTSLotNumber"].ToString());
                    sessionData.Add(11, aSessionData);
                }



                if (HttpContext.Current.Session["WCTSProjectSetupId"] == null)
                {
                    var akSessionData = new SessionData(0, "setupId");
                    sessionData.Add(12, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSProjectSetupId"].ToString()), "setupId");
                    sessionData.Add(12, aSessionData);
                }


                if (HttpContext.Current.Session["WCTSInventoryType"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "InventoryType");
                    sessionData.Add(13, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), HttpContext.Current.Session["WCTSInventoryType"].ToString());
                    sessionData.Add(13, aSessionData);
                }

                if (HttpContext.Current.Session["WCTSProductionDate"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "ProductionDate");
                    sessionData.Add(14, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSUserId"].ToString()), HttpContext.Current.Session["WCTSProductionDate"].ToString());
                    sessionData.Add(14, aSessionData);
                }




                if (HttpContext.Current.Session["WCTSRoleId"] == null || HttpContext.Current.Session["WCTSUserId"] == null)
                {
                    var akSessionData = new SessionData(0, "WCTSRoleId");
                    sessionData.Add(15, akSessionData);
                }
                else
                {
                    var aSessionData = new SessionData(long.Parse(HttpContext.Current.Session["WCTSRoleId"].ToString()), "WCTSRoleId");
                    sessionData.Add(15, aSessionData);
                }





                return sessionData;
            }


             
        }


    }
