﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCTS.Helper_Model
{
    public class ProductionSummaryReport
    {
        public string MasterBarcode { get; set; }
        public string Model { get; set; }
        public string ShiftId { get; set; }
        public string Shift { get; set; }
        public string CategoryCode { get; set; }
        public string AddedDate { get; set; }
        public int Total { get; set; }
    }
}