﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCTS.Helper_Model
{
    public class Result
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string Id { get; set; }
        public int Count { get; set; }

        /////
        /// 
        /// 
        /// 
        /// public string Barcode { get; set; }
        public string Barcode { get; set; }
        public string Shift { get; set; }
        public string Line { get; set; }
        public bool Success { get; set; }
        //public int Count { get; set; }
        public object BatchItems { get; set; }
        public string LotNo { get; set; }
        public string MasterBatch { get; set; }
    }
}