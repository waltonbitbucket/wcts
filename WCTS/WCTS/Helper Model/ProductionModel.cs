﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCTS.Helper_Model
{
    public class ProductionModel
    {

        public string Barcode { get; set; }
        public string Shift { get; set; }
        public string Line { get; set; }
        public object BatchItems { get; set; }
        public string LotNo { get; set; }
        public string MasterBatch { get; set; }
        public string ShiftName { get; set; }
        public string CategoryName { get; set; }
        public int CategoTotalryName { get; set; }
        public string Date { get; set; }

    }
}