﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WCTS.Startup))]
namespace WCTS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
